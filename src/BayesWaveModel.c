#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <lal/LALSimSGWB.h>

#include "BayesWave.h"
#include "BayesLine.h"
#include "BayesWaveIO.h"
#include "BayesWaveModel.h"
#include "BayesWavePrior.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveLikelihood.h"

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif


/* ********************************************************************************** */
/*                                                                                    */
/*                             Instrument noise routines                              */
/*                                                                                    */
/* ********************************************************************************** */


void Shf_Geocenter(struct Data *data, struct Model *model, double *SnGeo, double *params)
{
    int ifo,i,n;
    int NI = data->NI;

    struct Network *projection = model->projection;
    double **Snf = model->Snf;

    double AntennaPattern;
    double ecc = params[3];

    int nmax = model->signal[0]->size;
    int index[model->signal[0]->size];

    for(n=0; n<nmax; n++)
    {
        i = (int)(model->signal[0]->intParams[n+1][1]*data->Tobs);
        index[n] = i;
        SnGeo[i] = 0.0;
    }

    for(ifo=0; ifo<NI; ifo++)
    {
      //AntennaPattern = 1.0;
    AntennaPattern = 0.1; /* averaged over prior of all extrinsic parameters */
        if(data->geocenterPSDFlag) AntennaPattern = projection->Fplus[ifo]*projection->Fplus[ifo] + ecc*ecc*projection->Fcross[ifo]*projection->Fcross[ifo];
        for(n=0; n<nmax; n++)
        {
            i = index[n];
            SnGeo[i] += AntennaPattern/(Snf[ifo][i]);
        }
    }

    for(n=0; n<nmax; n++)
    {
        i = index[n];
        SnGeo[i] = 1./SnGeo[i];
    }
    
}

void Shf_Geocenter_full(struct Data *data, struct Network *projection, double **Snf, double *SnGeo, double *params)
{
  int ifo,i, halfN=data->N/2;
  int NI = data->NI;

  int imin = data->imin;
  int imax = data->imax;

  double AntennaPattern;
  double ecc = 1.0;
  if(data->polarizationFlag)ecc=params[3];

  for(i=0; i<halfN; i++) SnGeo[i] = 0.0;

  for(ifo=0; ifo<NI; ifo++)
  {
      //AntennaPattern = 1.0;
    AntennaPattern = 0.1; /* averaged over prior of all extrinsic parameters */
      if(data->geocenterPSDFlag) AntennaPattern = projection->Fplus[ifo]*projection->Fplus[ifo] + ecc*ecc*projection->Fcross[ifo]*projection->Fcross[ifo];
    for(i=imin; i<imax; i++) SnGeo[i] += AntennaPattern/(Snf[ifo][i]);
  }

  for(i=0; i<imin; i++)SnGeo[i]=1.0;
  for(i=imax; i<halfN; i++)SnGeo[i]=1.0;
  for(i=imin; i<imax; i++)  SnGeo[i] = 1./SnGeo[i];
}

void OverlapReductionFunction(struct Data *data)
{

  int i;
  double f;
  double invTobs = 1.0/data->Tobs;

  if(data->NI==2)
  {
    fprintf(stdout,"Calculating overlap reduction function, NI=%i\n\n",data->NI);
    for(i=0; i<data->N/2; i++)
    {

      f = (double)(i)*invTobs;
      data->ORF[i] = XLALSimSGWBOverlapReductionFunction(f, data->detector[0], data->detector[1]);
    }
  }
  else
  {
    fprintf(stdout,"Skipping overlap reduction function, NI=%i\n\n",data->NI);
  }
}

void ComputeNoiseCorrelationMatrix(struct Data *data, double **Snf, double *Sn, struct Background *background)
{
  int i;

  double C11, C12, C21, C22;
  double x;
  double invC;
  double invTobs = 1.0/data->Tobs;
  double invTf   = invTobs*background->invfref;
  double amp     = exp(background->logamp);

  for(i=data->imin; i<data->imax; i++)
  {

    /*
     Calculate network response to stochastic background parameters
     */
    x = (double)i*invTf;

    background->spectrum[i] = amp * powf(x,background->index);

    /*
     Calculate noise correlation matrix
     Combining instrument noise and stochastic backgrouns signal
     */
    C11 = background->spectrum[i] + Snf[0][i]*Sn[0];
    C22 = background->spectrum[i] + Snf[1][i]*Sn[1];
    C12 = background->spectrum[i] * data->ORF[i];
    C21 = C12;

    background->detCij[i] = C11*C22 - C12*C21;
    invC = 1./background->detCij[i];

    background->Cij[0][0][i] =  C22*invC;
    background->Cij[0][1][i] = -C12*invC;
    background->Cij[1][0][i] = -C21*invC;
    background->Cij[1][1][i] =  C11*invC;

//    printf("%lg %lg\n%lg %lg\n",background->Cij[0][0][i],background->Cij[0][1][i],background->Cij[1][0][i],background->Cij[1][1][i]);
//    printf("================\n");
//    printf("%lg %lg\n",Snf[0][i]*Sn[0],data->Snf[1][i]*Sn[1]);
  }
}

double symmetric_snr_ratio(struct Data *data, struct Network *projection, double *params)
{
   int ifo;
   int NI = data->NI;

   double AntennaPattern;
   double ecc = params[3];

   double SNR[NI];

   for(ifo=0; ifo<NI; ifo++)
   {
      AntennaPattern = projection->Fplus[ifo]*projection->Fplus[ifo] + ecc*ecc*projection->Fcross[ifo]*projection->Fcross[ifo];
      SNR[ifo] = AntennaPattern;
   }

   return SNR[0]*SNR[1]/(SNR[0]+SNR[1])/(SNR[0]+SNR[1]);
   
}

/* ********************************************************************************** */
/*                                                                                    */
/*                           Memory (de)allocation routines                           */
/*                                                                                    */
/* ********************************************************************************** */


void initialize_fisher(struct FisherMatrix *fisher, int N)
{
  int i;
  fisher->N = N;

  fisher->aamps	    = double_vector(fisher->N-1);
  fisher->sigma	    = double_vector(fisher->N-1);
  fisher->evalue  	= double_vector(fisher->N-1);

  fisher->count     = double_matrix(fisher->N-1,fisher->N-1);
  fisher->matrix	  = double_matrix(fisher->N-1,fisher->N-1);
  fisher->evector	  = double_matrix(fisher->N-1,fisher->N-1);

  for(i=0;i<fisher->N;i++)
  {
    fisher->aamps[i] = 1.0;
    fisher->count[0][i] = 1;
    fisher->count[1][i] = 1;
  }
}

void free_fisher(struct FisherMatrix *fisher)
{

  free_double_vector(fisher->aamps);
  free_double_vector(fisher->sigma);
  free_double_vector(fisher->evalue);

  free_double_matrix(fisher->count,fisher->N-1);
  free_double_matrix(fisher->matrix,fisher->N-1);
  free_double_matrix(fisher->evector,fisher->N-1);
}


void initialize_chain(struct Chain *chain, int flag)
{
  chain->mod0     = 0;     //Counter keeping track of noise model
  chain->mod1     = 0;     //Counter keeping track of signal model
  chain->mod2     = 0;     //Counter keeping track of noise model
  chain->mod3     = 0;     //Counter keeping track of signal model
  chain->mcount   = 1;     //# of MCMC proposals
  chain->scount   = 1;     //# of RJMCMC proposals
  chain->xcount   = 1;     //# of extrinsic proposals
  chain->fcount   = 1;     //# of intrinsic fisher proposals
  chain->pcount   = 1;     //# of intrinsic phase proposals
  chain->ccount   = 1;     //# of cluster proposals
  chain->dcount   = 1;     //# of density proposals
  chain->ucount   = 1;     //# of uniform proposals
  chain->macc     = 0;     //# of MCMC successes
  chain->sacc     = 0;     //# of RJMCMC successes
  chain->xacc     = 0;     //# of extrinsic successes
  chain->facc     = 0;     //# of intrinsic fisher successes
  chain->pacc     = 0;     //# of intrinsic phase successes
  chain->cacc     = 0;     //# of cluster proposal successes
  chain->dacc     = 0;     //# of density proposal successes
  chain->uacc     = 0;     //# of uniform proposal successes
  chain->burnFlag = flag;  //Flag for burn-in (1 = yes)
  }

void free_chain(struct Chain *chain, int NI, int NP)
{
   free(chain->ptacc);
   free(chain->ptprop);
   free(chain->index);
   free(chain->A);

   free(chain->dT);
   free(chain->temperature);
   free(chain->avgLogLikelihood);
   free(chain->varLogLikelihood);

   free(chain->intChainFile);

   int n;
   for(n=0; n<NP; n++) free(chain->intWaveChainFile[n]);
   free(chain->intWaveChainFile);

   for(n=0; n<NI; n++) free(chain->intGlitchChainFile[n]);
   free(chain->intGlitchChainFile);

  //logL chain for thermodynamic integration
  for(n=0; n<chain->NC; n++) free(chain->logLchain[n]);
  free(chain->logLchain);

}

void allocate_chain(struct Chain *chain, int NI, int NP)
{
   chain->ptacc        = malloc(chain->NC*sizeof(int));
   chain->ptprop       = malloc(chain->NC*sizeof(int));
   chain->index        = malloc(chain->NC*sizeof(int));
   chain->A            = malloc(chain->NC*sizeof(double));

   chain->dT               = malloc(chain->NC*sizeof(double));
   chain->temperature      = malloc(chain->NC*sizeof(double));
   chain->avgLogLikelihood = malloc(chain->NC*sizeof(double));
   chain->varLogLikelihood = malloc(chain->NC*sizeof(double));

   chain->intChainFile = malloc(chain->NC*sizeof(FILE*));

  int n;
  
   chain->intWaveChainFile = malloc(NP*sizeof(FILE**));
  for(n=0; n<NP; n++) chain->intWaveChainFile[n] = malloc(chain->NC*sizeof(FILE*));

   int ifo;
   chain->intGlitchChainFile = malloc(NI*sizeof(FILE**));
   for(ifo=0; ifo<NI; ifo++) chain->intGlitchChainFile[ifo] = malloc(chain->NC*sizeof(FILE*));

  int ic;
  chain->lineChainFile   = malloc(chain->NC*sizeof(FILE**));
  chain->splineChainFile = malloc(chain->NC*sizeof(FILE**));
  for(ic=0; ic<chain->NC; ic++)
  {
    chain->lineChainFile[ic]   = malloc(NI*sizeof(FILE*));
    chain->splineChainFile[ic] = malloc(NI*sizeof(FILE*));
  }

  //logL chain for thermodynamic integration
  int i;
  chain->nPoints = 3*chain->count/4/chain->cycle;
  chain->logLchain = malloc(chain->NC*sizeof(double *));
  for(i=0; i<chain->NC; i++)
  {
      chain->logLchain[i] = malloc(chain->nPoints*sizeof(double));
      for(n=0; n<chain->nPoints; n++) chain->logLchain[i][n]=0.0;
  }
}

void resize_model(struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model, struct BayesLineParams ***bayesline, double **psd, int NC)
{
  int ic,ifo;

  fprintf(stdout, " Resizing model for number of chains from %i to %i\n",chain->NC,NC);

  struct BayesLineParams **bptr;
  bptr=malloc(data->NI*sizeof(struct BayesLineParams *));
  initialize_bayesline(bptr,data,psd,chain->blcount);
  for(ifo=0; ifo<data->NI; ifo++) copy_bayesline_params(bayesline[0][ifo], bptr[ifo]);


  //free model structure
  for(ic=0; ic<chain->NC; ic++)
  {
    free_bayesline(bayesline[ic], data);
    free(bayesline[ic]);

    free_model(model[ic], data, prior);
    free(model[ic]);
  }

  //choose new number of chains
  free_chain(chain, data->NI, data->Npol);
  chain->NC = NC;
  allocate_chain(chain, data->NI, data->Npol);
  for(ic=0; ic<chain->NC; ic++) chain->index[ic]=ic;

  *model      = realloc(*model,      chain->NC * sizeof(struct Model*)            );
  **bayesline = realloc(**bayesline, chain->NC * sizeof(struct BayesLineParams**) );

  for(ic=0; ic<chain->NC; ic++)
  {
    model[ic] = malloc(sizeof(struct Model));
    initialize_model(model[ic], data, prior, psd, chain->seed);

    bayesline[ic] = malloc(data->NI*sizeof(struct BayesLineParams *));
    initialize_bayesline(bayesline[ic], data, psd, chain->blcount);
    for(ifo=0; ifo<data->NI; ifo++) copy_bayesline_params(bptr[ifo], bayesline[ic][ifo]);
  }

  free_bayesline(bptr, data);
  free(bptr);
}


void initialize_priors(struct Data *data, struct Prior *prior, int omax)
{
   int i,n;
   int NW = data->NW;
  
   /* Model dimension parameters */
   // number of sineGaussians we print to file
   prior->omax = omax;

   // maximum number of sineGaussians
   prior->gmin = int_vector(data->NI-1);
   prior->gmax = int_vector(data->NI-1);

    // wavelet density prior
    prior->Nwavelet = malloc(200*sizeof(double));
    double b = 2.9;
    //TODO Nwavelet unstable at i=0?
    for(i=1; i<200; i++) prior->Nwavelet[i] = log(((double)i*4.0*sqrt(3.)/(LAL_PI*pow(b,2.)))/(3.0+pow((double)i/b,4.)));
    prior->Nwavelet[0]=prior->Nwavelet[1];

   prior->smin = 0;
   prior->smax = data->Dmax;
   for(i=0; i<data->NI; i++)
   {
      prior->gmin[i] = 0;
      prior->gmax[i] = data->Dmax;
   }

   if(data->signalFlag || data->glitchFlag) prior->smin = 1;


   /* PSD paramters */
   prior->Snmin = 0.5;
   prior->Snmax = 2.0;


   /* Wavelet parameters */
   prior->range = double_matrix(NW-1,1);

   double Amin=0.0, Amax=1.0;
   //Set Amplitude priors based on the data channel being used
   //S6 strain
   if(!strcmp(data->channels[0],"LDAS-STRAIN"))
   {
      Amin = 5.e-25;
      Amax = 1.e-18;
   }

   //VSR2 strain
   else if (!strcmp(data->channels[0],"h_16384Hz"))
   {
      Amin = 5.e-24;
      Amax = 1.e-15;
   }

   //S6 LIGO DARM Error
   else if (!strcmp(data->channels[0],"LSC-DARM_ERR"))
   {
      Amin = 5.0e-9;
      Amax = 1.0e-3;
   }

   //Use S6 strain as default
   else
   {
     Amin = 4.78089288388547e-25;
     Amax = 7.7811322411338e-20;
   }

   //double dt   = data->Tobs/(double)data->tsize;
   double Qmin = data->Qmin;
   double Qmax = data->Qmax;
   double tmin = 0;//dt*(int)((data->Tobs/16.)/dt);
   double tmax = data->Tobs;//-tmin;
   double fmin = data->fmin;
   double fmax = data->fmax;
   double betamin = -1.0;
   double betamax = 1.0;

   /*
    if(data->amplitudePriorFlag)
    {
    Amin=0.0;
    Amax=1.0;
    }
    */

   prior->range[0][0] = tmin;
   prior->range[0][1] = tmax;
   prior->range[1][0] = fmin;
   prior->range[1][1] = fmax;
   prior->range[2][0] = Qmin;
   prior->range[2][1] = Qmax;
   prior->range[3][0] = Amin;
   prior->range[3][1] = Amax;
   prior->range[4][0] = 0.0;
   prior->range[4][1] = LAL_TWOPI;
    
    if(data->chirpletFlag)
    {
        prior->range[5][0] = betamin;
        prior->range[5][1] = betamax;
    }

   prior->TFV    = (tmax-tmin)*(fmax-fmin)*(Qmax-Qmin);
   prior->logTFV = log(prior->TFV);


   /**********************************/
   /* cluster prior                  */
   /**********************************/
   prior->bias = double_vector(prior->smax);

  if(data->clusterPriorFlag)
  {
    
    FILE *normFile;
    char filename[1000];
    
    sprintf(filename,"%s/cluster_norm_flow%i_twin%.2f_srate%i_Qmax%i.dat",prior->path,(int)data->fmin,data->Twin,data->srate,(int)data->Qmax);
    printf("opening file %s\n",filename);
    if(!checkfile(filename))
    {
      fprintf(stdout,"\n");
      fprintf(stdout,"ERROR: Missing file to correct bias in clustering prior:\n");
      fprintf(stdout,"  path:   %s\n",filename);
      fprintf(stdout,"  flow:   %i\n",(int)data->fmin);
      fprintf(stdout,"  window: %i\n",(int)data->Twin);
      fprintf(stdout,"  srate:  %i\n",data->srate);
      fprintf(stdout,"  Qmax:   %i\n",(int)data->Qmax);
      fprintf(stdout,"TRY:\n");
      fprintf(stdout,"  Creating file to correct for bias\n");
      fprintf(stdout,"  Checking your --clusterPath\n");
      fprintf(stdout,"  Removing --clusterPrior\n\n");
      exit(1);
    }
    else
    {
      sprintf(filename,"%s/cluster_norm_flow%i_twin%.2f_srate%i_Qmax%i.dat",prior->path,(int)fmin,data->Twin,data->srate,(int)data->Qmax);
      normFile = fopen(filename,"r");
      
      for(i=0; i<prior->smax; i++)
      {
        fscanf(normFile,"%i %lg",&n,&prior->bias[i]);
        prior->bias[i] = log(prior->bias[i]);
      }
      
      fclose(normFile);
    }
  }
}

void reset_priors(struct Data *data, struct Prior *prior)
{
   int i;

   prior->smin = 0;
   prior->smax = data->Dmax;
   for(i=0; i<data->NI; i++)
   {
      prior->gmin[i] = 0;
      prior->gmax[i] = data->Dmax;
   }

   if(data->signalFlag || data->glitchFlag) prior->smin = data->Dmin;

   /* PSD paramters */
   prior->Snmin = 0.5;
   prior->Snmax = 2.0;

   double Amin=0.0, Amax=1.0;
   //Set Amplitude priors based on the data channel being used
   //S6 strain
   if(!strcmp(data->channels[0],"LDAS-STRAIN"))
   {
      Amin = 5.e-25;
      Amax = 1.e-18;
   }

   //VSR2 strain
   else if (!strcmp(data->channels[0],"h_16384Hz"))
   {
      Amin = 5.e-24;
      Amax = 1.e-15;
   }

   //S6 LIGO DARM Error
   else if (!strcmp(data->channels[0],"LSC-DARM_ERR"))
   {
      Amin = 5.0e-9;
      Amax = 1.0e-3;
   }

   //Use S6 strain as default
   else
   {
    Amin = 4.78089288388547e-25;
    Amax = 7.7811322411338e-20;
   }

   double Qmin = data->Qmin;
   double Qmax = data->Qmax;
   double tmin = 0;//dt*(int)((data->Tobs/16.)/dt);
   double tmax = data->Tobs;//-tmin;
   double fmin = data->fmin;
   double fmax = data->fmax;
   double betamin = -1.0;
   double betamax = 1.0;
    
    
   //restrict prior range for wavelet time to 1 second around trigger
   if(data->runPhase==1)
   {
       tmin = data->Tobs/2.0-data->Twin/2.0;
       tmax = data->Tobs/2.0+data->Twin/2.0;
   }
   /*
    if(data->amplitudePriorFlag)
    {
    Amin=0.0;
    Amax=1.0;
    }
    */
   prior->range[0][0] = tmin;
   prior->range[0][1] = tmax;
   prior->range[1][0] = fmin;
   prior->range[1][1] = fmax;
   prior->range[2][0] = Qmin;
   prior->range[2][1] = Qmax;
  prior->range[3][0] =  Amin;
  prior->range[3][1] =  Amax;
   prior->range[4][0] = 0.0;
   prior->range[4][1] = LAL_TWOPI;
  if(data->chirpletFlag)
  {
    prior->range[5][0] = betamin;
    prior->range[5][1] = betamax;
  }

   prior->TFV    = (tmax-tmin)*(fmax-fmin)*(Qmax-Qmin);
   prior->logTFV = log(prior->TFV);

}

void initialize_data(struct Data *data, double **s, int N, int tsize, double Tobs, int NI, double fmin, double fmax)
{
   int i,ifo, halfN=N/2;

   data->N     = N;
   data->NI    = NI;
   data->tsize = tsize;
   data->fsize = N/tsize;
   data->Tobs  = Tobs;
   data->fmin  = fmin;
   data->fmax  = fmax;
   data->imin  = (int)floor(fmin*Tobs);
   data->imax  = (int)floor(fmax*Tobs);
   data->dt    = Tobs/(double)(tsize);
   data->df    = 0.5/data->dt;

   // allocate data vectors
   data->h     = double_vector(N-1); // workspace for individual templates
   data->s     = double_matrix(NI-1,N-1); // data
   data->r     = double_matrix(NI-1,N-1); // residual
   data->rh    = double_matrix(NI-1,N-1);

  // number of signal polarizations (0==h+, 1==hx, ...)
  data->Npol = 2;
  if(data->polarizationFlag) data->Npol = 1;


   // initialize data vectors
   for(i=0; i<N; i++)
   {
      data->h[i] = 0.0;

      for(ifo=0; ifo<NI; ifo++)
      {
         data->s[ifo][i]  = s[ifo][i];
         data->r[ifo][i]  = 0.0;
         data->rh[ifo][i] = 0.0;
      }
   }

  data->ORF = double_vector(halfN-1);

   data->logLc = 0.0;

  /*
   Initialize logZ to logL of NULL model, i.e. -(d|d)/2
   That way if models are skipped the Bayes factors tell
   us about the improvement over the naive Gaussian noise
   model
   */
  data->logZglitch = data->logLc;
  data->logZnoise  = data->logLc;
  data->logZsignal = data->logLc;
  data->logZfull   = data->logLc;

  data->varZglitch = 0.0;
  data->varZnoise  = 0.0;
  data->varZsignal = 0.0;
  data->varZfull   = 0.0;

    data->cleanFlag  = 0;
    data->glitchFlag = 0;
    data->signalFlag = 0;
  
  // IFO names
  data->ifos = malloc(NI*sizeof(char *));
  for(i=0; i<NI; i++) data->ifos[i] = malloc(4*sizeof(char));

}

void initialize_model(struct Model *model, struct Data *data, struct Prior *prior, double **psd, gsl_rng *seed)
{
  int i,ifo;
  int N = data->N;
  int halfN = data->N/2;
  int NI = data->NI;
  int Npol = data->Npol;
  int NW = data->NW; //number of intrinsic parameters / frame (5 for wavelet)
  
  
  //gaussian noise model
  model->Sn    = double_vector(NI-1);
  model->SnS   = double_matrix(NI-1,halfN-1);
  model->Snf   = double_matrix(NI-1,halfN-1);
  model->invSnf= double_matrix(NI-1,halfN-1);
  model->SnGeo = double_vector(halfN-1);

  //store sum of 1/psd in 1/SnGeo
  for(i=0; i<halfN; i++)
  {
    model->SnGeo[i] = 0.0;
    for(ifo=0; ifo<NI; ifo++)
    {
      model->Snf[ifo][i] = psd[ifo][i];
      model->SnS[ifo][i] = psd[ifo][i];
      model->invSnf[ifo][i] = 1./psd[ifo][i];
      model->SnGeo[i] += 1./psd[ifo][i];
    }
    model->SnGeo[i] = 1./model->SnGeo[i];
  }

  //extrinsic parameters
  model->extParams = double_vector(NE);

  //full instrument response model (projected geocenter + glitches)
  model->response = double_matrix(NI-1,N-1);

  //incident signal model
  model->h = double_matrix(Npol,N-1);

  //likelhood is a pointer to a function which returns logL
  model->logL = 0.0;

  //detLogL stores the likelihood in each detector (for quick computation of full logL)
  model->detLogL = double_vector(NI-1);

  //draw extrinsic parameters from prior
  extrinsic_uniform_proposal(seed,model->extParams);

  //set "delta" parameters to not modify signal model
  model->extParams[4] = 0.0;  //geocenter phase shift
  model->extParams[5] = 1.0;  //amplitude scale


  //allocate memory for signal wavelet structure
  /*
   New for O3: signal structure is a 2-element vector for (+) and (x) polarizations
   */
  model->Npol = Npol;

  model->signal = malloc(Npol * sizeof(struct Wavelet *));
  for(i=0; i<Npol; i++)
  {
    model->signal[i] = malloc(sizeof(struct Wavelet));
    
    //assign wavelet dimension for signal model
    model->signal[i]->dimension = NW; //wavelets
    
    //Setup wavelet structure holding signal model
    if(data->signalFlag)
      initialize_wavelet(model->signal[i],N, prior->smax, 1);
    else
      initialize_wavelet(model->signal[i],N, prior->smax, 0);
  }

  //allocate memory for structure holding projection coefficients
  model->projection = malloc(sizeof(struct Network));

  //set up projection coefficients
  initialize_network(model->projection, N, NI);

  //Allocate and initialize glitch structures
  model->glitch = malloc(NI * sizeof(struct Wavelet *));
  

  for(i=0; i<NI; i++) model->glitch[i] = malloc(sizeof(struct Wavelet));
  for(i=0; i<NI; i++)
  {
    //assign wavelet dimension for glitch model
    model->glitch[i]->dimension = NW; //wavelets

    if(data->glitchFlag)
      initialize_wavelet(model->glitch[i],N, prior->smax, 1);
    else
      initialize_wavelet(model->glitch[i],N, prior->smax, 0);
  }

  //allocate memory for structure holding extrinsic fisher matrix
  model->fisher = malloc(sizeof(struct FisherMatrix));
  initialize_fisher(model->fisher, NE);

  //allocate memory for structure holding intrinsic fisher matrix
  model->intfisher = malloc(sizeof(struct FisherMatrix));
  initialize_fisher(model->intfisher, NW);

  //select wavelet basis function
  if(data->chirpletFlag)
  {
    model->wavelet           = ChirpletFourier;
    model->wavelet_bandwidth = ChirpletBandwidth;
  }
  else
  {
    model->wavelet           = SineGaussianFourier;
    model->wavelet_bandwidth = SineGaussianBandwidth;
  }
  

  //Stochastic background
  if(data->stochasticFlag)
  {
    model->background = malloc(sizeof(struct Background));
    initialize_background(model->background, NI, halfN);
  }
    
}

void reset_model(struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model)
{
   int i,j,ic,ifo;
   int NC= chain->NC;
   int NI= data->NI;

   struct Wavelet **signal;
   struct Wavelet **glitch;

   for(ic=0; ic<NC; ic++)
   {
      //reset model
      signal = model[ic]->signal;
      glitch = model[ic]->glitch;
      model[ic]->size=0;
      for(ifo=0; ifo<NI; ifo++) glitch[ifo]->size=0;

     for(i=0; i<model[ic]->Npol; i++)
     {
       signal[i]->size=0;
       signal[i]->smax = prior->smax;
       for(j=1; j<=signal[i]->smax; j++) signal[i]->index[j]=j;
     }
     
     for(i=0; i<NI; i++)
     {
       glitch[i]->smax = prior->smax;
       for(j=1; j<=glitch[i]->smax; j++) glitch[i]->index[j]=j;
      }

      for(ifo=0; ifo<NI; ifo++)
      {
         if(data->glitchFlag)
         {
            glitch[ifo]->size = 1;
            model[ic]->size++;
         }
      }

      if(data->signalFlag)
      {
        for(i=0; i<model[ic]->Npol; i++)
        {
         signal[i]->size = 1;
        }
         model[ic]->size++;
      }

      model[ic]->logL=data->logLc;
   }
}

void free_bayesline(struct BayesLineParams **bayesline, struct Data *data)
{
  int ifo;

  for(ifo=0; ifo<data->NI; ifo++)
  {
    free(bayesline[ifo]->priors->upper);
    free(bayesline[ifo]->priors->lower);
    free(bayesline[ifo]->priors->mean);
    free(bayesline[ifo]->priors->sigma);
    free(bayesline[ifo]->priors);
    BayesLineFree(bayesline[ifo]);
  }
}


void initialize_bayesline(struct BayesLineParams **bayesline, struct Data *data, double **psd, int nstep)
{
  int i,ifo;

  int N    = (int)(data->Tobs*(data->fmax-data->fmin))+1;
  int imin = (int)(data->Tobs*data->fmin);

  for(ifo=0; ifo<data->NI; ifo++)
  {
    bayesline[ifo] = malloc(sizeof(struct BayesLineParams));
    bayesline[ifo]->priors = malloc(sizeof(BayesLinePriors));
    //bayesline[ifo]->priors->invsigma = malloc((int)(Tobs*(fmax-fmin))*sizeof(double));
    bayesline[ifo]->priors->upper = malloc(N*sizeof(double));
    bayesline[ifo]->priors->lower = malloc(N*sizeof(double));
    bayesline[ifo]->priors->mean  = malloc(N*sizeof(double));
    bayesline[ifo]->priors->sigma = malloc(N*sizeof(double));

    //Set BayesLine priors based on the data channel being used
    set_bayesline_priors(data->channels[ifo], bayesline[ifo], data->Tobs);

    // set default flags
    bayesline[ifo]->constantLogLFlag = data->constantLogLFlag;

    //Use initial estimate of PSD to set priors
    for(i=0; i<N; i++)
    {
      //bayesline[ifo]->priors->invsigma[i] = 1./(psd[ifo][i+(int)(Tobs*fmin)]*1.0);
      bayesline[ifo]->priors->sigma[i] = psd[ifo][i+imin];
      bayesline[ifo]->priors->mean[i]  = psd[ifo][i+imin];
      bayesline[ifo]->priors->lower[i] = psd[ifo][i+imin]/100.;
      bayesline[ifo]->priors->upper[i] = psd[ifo][i+imin]*2.;

    }

    //Allocates arrays and sets constants for for BayesLine
    BayesLineSetup(bayesline[ifo], data->s[ifo], data->fmin, data->fmax, data->dt, data->Tobs, nstep);
  }

//  FILE *fptr=fopen("psdprior.dat","w");
//  for(i=0; i<(int)(data->Tobs*(data->fmax-data->fmin)); i++)
//  {
//    fprintf(fptr,"%lg ",data->fmin + i*1./data->Tobs);
//    for(ifo=0; ifo<data->NI; ifo++)
//    {
//      fprintf(fptr,"%lg ",psd[ifo][i+(int)(data->Tobs*data->fmin)]);
//      fprintf(fptr,"%lg ",bayesline[ifo]->priors->lower[i]);
//      fprintf(fptr,"%lg ",bayesline[ifo]->priors->upper[i]);
//    }
//    fprintf(fptr,"\n");
//  }
//  fclose(fptr);
}


void reset_likelihood(struct Data *data)
{
    if(data->constantLogLFlag)
    {
        // logL=const to test detailed balance
        data->intrinsic_likelihood = EvaluateConstantLogLikelihood;
        data->extrinsic_likelihood = EvaluateExtrinsicConstantLogLikelihood;
    }
    else
    {
        data->intrinsic_likelihood = EvaluateMarkovianLogLikelihood;
        data->extrinsic_likelihood = EvaluateExtrinsicMarkovianLogLikelihood;
    }
}

void initialize_network(struct Network *projection, int N, int NI)
{
    projection->expPhase = malloc(NI*sizeof(double *));
    projection->deltaT   = malloc(NI*sizeof(double));
    projection->Fplus    = malloc(NI*sizeof(double));
    projection->Fcross   = malloc(NI*sizeof(double));
    projection->dtimes   = malloc(NI*sizeof(double));

    int i;
    for(i=0; i<NI; i++) projection->expPhase[i] = malloc(N*sizeof(double));
}

void free_network(struct Network *projection, int NI)
{
    int i;
    for(i=0; i<NI; i++) free(projection->expPhase[i]);

    free(projection->expPhase);
    free(projection->deltaT);
    free(projection->Fplus);
    free(projection->Fcross);
    free(projection->dtimes);
}

void copy_int_model(struct Model *origin, struct Model *copy, int N, int NI, int det)
{
   int i,ip,ifo;

   copy->logL=origin->logL;

   copy->size=origin->size;
  
   copy->Npol=origin->Npol;

   if(det==-1)
   {
      for(ip=0;  ip<origin->Npol;  ip++) copy_wavelet(origin->signal[ip], copy->signal[ip], N);
      for(ip=0;  ip<=origin->Npol; ip++) for(i=0; i<N; i++) copy->h[ip][i] = origin->h[ip][i];
      for(ifo=0; ifo<NI;          ifo++) for(i=0; i<N; i++) copy->response[ifo][i] = origin->response[ifo][i];
   }
   else
   {
      copy_wavelet(origin->glitch[det],copy->glitch[det], N);
   }

   for(ifo=0; ifo<NI; ifo++)
   {
      copy->detLogL[ifo] = origin->detLogL[ifo];

      copy->Sn[ifo] = origin->Sn[ifo]; //noise PSD scale
   }
}

void copy_psd_model(struct Model *origin, struct Model *copy, int N, int NI)
{
  int n,ifo;
  int halfN = N/2;
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<halfN; n++)
    {
      copy->SnS[ifo][n]    = origin->SnS[ifo][n];
      copy->Snf[ifo][n]    = origin->Snf[ifo][n];
      copy->invSnf[ifo][n] = origin->invSnf[ifo][n];
    }
  }
  for(n=0; n<halfN; n++)
  {
    copy->SnGeo[n] = origin->SnGeo[n];
  }
}

void copy_ext_model(struct Model *origin, struct Model *copy, int N, int NI)
{
    int i,ifo;
    for(i=0; i<NE; i++) copy->extParams[i] = origin->extParams[i];

    for(ifo=0; ifo<NI; ifo++)
    {
        copy->projection->Fplus[ifo]  = origin->projection->Fplus[ifo];
        copy->projection->Fcross[ifo] = origin->projection->Fcross[ifo];
        copy->projection->deltaT[ifo] = origin->projection->deltaT[ifo];
        copy->projection->dtimes[ifo] = origin->projection->dtimes[ifo];
        for(i=0; i<N; i++) copy->projection->expPhase[ifo][i] = origin->projection->expPhase[ifo][i];
    }
}

void free_model(struct Model *model, struct Data *data, struct Prior *prior)
{
  int i;
  int NI = data->NI;
  int Npol = model->Npol;
  
  free_double_vector(model->Sn);
  free_double_vector(model->SnGeo);
  free_double_matrix(model->SnS,NI-1);
  free_double_matrix(model->Snf,NI-1);
  free_double_matrix(model->invSnf,NI-1);

  free_double_vector(model->detLogL);
  free_double_vector(model->extParams);
  free_double_matrix(model->response,NI-1);
  free_double_matrix(model->h,Npol);

  for(i=0; i<Npol; i++)
  {
    free_wavelet(model->signal[i],prior->smax);
    free(model->signal[i]);
  }
  free(model->signal);
  
  free_network(model->projection, NI);
  free(model->projection);

  free_fisher(model->fisher);
  free(model->fisher);

  free_fisher(model->intfisher);
  free(model->intfisher);

  for(i=0; i<NI; i++)
  {
    free_wavelet(model->glitch[i],prior->smax);
    free(model->glitch[i]);
  }
  free(model->glitch);

  if(data->stochasticFlag)
  {
    free_background(model->background, NI);
    free(model->background);
  }
}

void initialize_wavelet(struct Wavelet *wave, int N, int smax, int size)
{
   int j;

   // initial number of wavelets
   wave->size = size;

   // maximum number of basis functions
   /*
    redundent to make Wavelet more self-contained
    should consider doing the same with N, and for
    other structures as well...
    //TODO: give all structures a copy of N??
    */
   wave->smax = smax;

   //store wavelet parameters
   wave->intParams = double_matrix(smax,wave->dimension-1);

   // this keeps track of who is who
   wave->index = int_vector(smax);

   // initialize reference order
   for(j=1; j<= smax; j++) wave->index[j] = j;

   // Fourier domain linear combination of wavelets
   wave->templates = double_vector(N-1);
   for(j=0; j<N; j++) wave->templates[j] = 0.0;

   // clustering prior
   wave->Ncluster = 0;
   wave->cosy = 0.0;
   wave->logp = 0.0;

}

void copy_wavelet(struct Wavelet *origin, struct Wavelet *copy, int N)
{
   int i,j, iend=origin->smax+1;

  for(i=1; i<iend; i++)
   {
      copy->index[i] = origin->index[i];
      for(j=0; j<origin->dimension; j++) copy->intParams[i][j] = origin->intParams[i][j];
   }
   for(j=0; j<N; j++) copy->templates[j] = origin->templates[j];

   copy->size      = origin->size;
   copy->dimension = origin->dimension;

   // clustering prior
   copy->Ncluster = origin->Ncluster;
   copy->cosy     = origin->cosy;
   copy->logp     = origin->logp;

}

void free_wavelet(struct Wavelet *wave, int smax)
{
   free_double_matrix(wave->intParams,smax);
   free_int_vector(wave->index);
   free_double_vector(wave->templates);
}

void initialize_background(struct Background *background, int NI, int N)
{
  background->Cij      = double_tensor(NI-1,NI-1,N-1);
  background->detCij   = double_vector(N-1);
  background->spectrum = double_vector(N-1);
  background->logamp   =-100.0;
  background->index    = 1.0;
  background->fref     = 100.0;
  background->invfref  = 1./background->fref;
}

void copy_background(struct Background *origin, struct Background *copy, int NI, int N)
{
  int n,i,j;

  copy->logamp  = origin->logamp;
  copy->index   = origin->index;
  copy->fref    = origin->fref;
  copy->invfref = origin->invfref;

  for(n=0; n<N; n++)
  {
    copy->spectrum[n] = origin->spectrum[n];
    copy->detCij[n]   = origin->detCij[n];
  }
  for(i=0; i<NI; i++)
  {
    for(j=0; j<NI; j++)
    {
      for(n=0; n<N; n++)
      {
        copy->Cij[i][j][n] = origin->Cij[i][j][n];
      }
    }
  }
}

void free_background(struct Background *background, int NI)
{
  free_double_tensor(background->Cij,NI-1,NI-1);
  free_double_vector(background->detCij);
  free_double_vector(background->spectrum);
}

void initialize_TF_proposal(struct Data *data, struct Prior *prior, struct TimeFrequencyMap *tf)
{
  int ifo;
  //int i,ifo;
  //int N =data->N;
  int NI=data->NI;

  tf->N = data->N;

  //4s grid for Q
  tf->nQ = (int)(prior->range[2][1] - prior->range[2][0])/2;

  //4Hz grid for frequency (normalized to 1024Hz sampling rate)
  tf->nf = (int)((prior->range[1][1] - prior->range[1][0])/(4.*((data->fmax*2.)/1024.)));

//  double tmin = prior->range[0][0];
//  double tmax = prior->range[0][1];
//
//  tf->nt = 0;
//  double t;
//  for(i = 0; i < N; i++)
//  {
//    t = ((double)(i)+0.5)*(8*data->Tobs/data->N);
//    if(t > tmin && t < tmax) tf->nt++;
//  }
  //.005s grid for t (normalized to 2s seglen)
  //FIXME: Revisit grid spacing--is this optimal for the large TFVs?
  tf->nt = (int)((prior->range[0][1] - prior->range[0][0])/(0.005*(data->Tobs/2.) )); //1024;

  tf->pdf = malloc((NI+1)*sizeof(double ***));
  tf->snr = malloc((NI+1)*sizeof(double ***));
  tf->max = malloc((NI+1)*sizeof(double *));

  for(ifo=0; ifo<NI+1; ifo++)
  {
    tf->pdf[ifo] = double_tensor(tf->nQ-1,tf->nf-1,tf->nt-1);
    tf->snr[ifo] = double_tensor(tf->nQ-1,tf->nf-1,tf->nt-1);
    tf->max[ifo] = double_vector(tf->nQ-1);

  }
}

void free_TF_proposal(struct Data *data, struct TimeFrequencyMap *tf)
{
  int ifo;

  for(ifo=0; ifo<data->NI+1; ifo++)
  {
    free_double_tensor(tf->pdf[ifo],tf->nQ-1,tf->nf-1);
    free_double_tensor(tf->snr[ifo],tf->nQ-1,tf->nf-1);
    free_double_vector(tf->max[ifo]);

  }

  free(tf->pdf);
  free(tf->snr);
  free(tf->max);
  
}

int checkfile(char *name)
{
   FILE *fp=fopen(name,"r");
   if(fp)
   {
      fclose(fp);
      return 1;
   }
   else return 0;
}


int *int_vector(int N)
{
   return malloc( (N+1) * sizeof(int) );
}

void free_int_vector(int *v)
{
   free(v);
}

double *double_vector(int N)
{
  return malloc( (N+1) * sizeof(double) );
}

void free_double_vector(double *v)
{
   free(v);
}

double **double_matrix(int N, int M)
{
  int i;
  double **m = malloc( (N+1) * sizeof(double *));
  
  for(i=0; i<N+1; i++)
  {
    m[i] = malloc( (M+1) * sizeof(double));
  }

  return m;
}

void free_double_matrix(double **m, int N)
{
  int i;
  for(i=0; i<N+1; i++) free_double_vector(m[i]);
  free(m);
}

double ***double_tensor(int N, int M, int L)
{
  int i,j;
  
  double ***t = malloc( (N+1) * sizeof(double **));
  for(i=0; i<N+1; i++)
  {
    t[i] = malloc( (M+1) * sizeof(double *));
    for(j=0; j<M+1; j++)
    {
      t[i][j] = malloc( (L+1) * sizeof(double));
    }
  }
  
  return t;
}

void free_double_tensor(double ***t, int N, int M)
{
  int i;
  
  for(i=0; i<N+1; i++) free_double_matrix(t[i],M);
  
  free(t);
}

