

/***************************  REQUIRED LIBRARIES  ***************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <math.h>
#include <fftw3.h>
#include <sys/stat.h>
#include <gsl/gsl_cdf.h>


#include <lal/LALInferenceTemplate.h>
#include <lal/LALSimIMR.h>
#include <lal/LALSimulation.h>
#include <lal/RingUtils.h>

#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h"
#include "BayesWaveMCMC.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveEvidence.h"
#include "BayesWaveLikelihood.h"

/*************  PROTOTYPE DECLARATIONS FOR INTERNAL FUNCTIONS  **************/

struct Moments
{
  double *overlap;
  double *energy;
  double *frequency;
  double *time;
  double *bandwidth;
  double *duration;
  double *snr;
  double networkOverlap;
  double *hmax;
  double *t_at_hmax;
  double *medianfrequency;
  double *mediantime;
  double *bandwidth90percentinterval;
  double *duration90percentinterval;
  double *tl;
  double *th;


};

void print_help_message(void);
void ComputeWaveformOverlap(struct Data *data, double **hinj, double **hrec, double **psd, struct Moments *m);
void PrintRhof(struct Data *data, double deltaF, double **h, double **psd, struct Moments *m);
void ComputeWaveformMoments(struct Data *data, double deltaF, double deltaT, double **h, double **psd, struct Moments *m);
void ComputeGeocenterMoments(struct Data *data, double deltaF, double deltaT, double *h, double *psd, struct Moments *m);

void initialize_moments(struct Moments *m, int NI);
void setup_output_files(int NI, int injectionFlag, char *outdir, char *model, char *weighting, FILE **momentsOut, struct Data *data);
void discard_burnin_samples(FILE *params, int *COUNT, int *BURNIN);
void print_moments(int NI, int injectionFlag, FILE **outfile, struct Moments *m);
void print_waveforms(struct Data *data, double **h, double **psd, FILE **ofile);
void print_spectrum(struct Data *data, double **h, FILE **ofile);
void print_hdot(struct Data *data, double **h, double **psd, FILE **ofile, double deltaF);

void get_time_domain_hdot(double *hdot, double *h, int N, int imin, int imax, double deltaF);

void print_stats(struct Data *data, LIGOTimeGPS GPS, int injectionFlag, char *outdir, char *model, int **dimension);

double get_energy(int imin, int imax, double *a, double deltaF);
double get_rhof(int imin, int imax, double *a, double deltaF, double *arg);
double get_rhof_net(int imin, int imax, double **a, double deltaF, double *arg, int NI);
double rhonorm(int imax, double *a, double deltaF);
double get_f0(int imax, double *a, double deltaF);

void get_f_quantiles(int imin, int imax, double *rhof, double deltaF, double *fm, double *bw);
void get_t_quantiles(int N, double *rhot, double deltaT, double *tm, double *dur);
double get_rhof_f2weighted(int imin, int imax, double *a, double deltaF, double *arg);

double get_band(int imax, double *a, double deltaF, double f0);
void get_hmax_and_t(int imax, double *a, double deltaT, double *hmax, double *t_at_hmax);
void get_time_domain_waveforms(double *hoft, double *h, int N, int imin, int imax);
double get_rhot(int N, double *a, double deltaT, double *arg);
double get_rhot_net(int N, double **a, double deltaT, double *arg, int NI);
void whiten_data(int imin, int imax, double *a, double *Snf);

void write_time_freq_samples(struct Data *data);
void window_fourier_waveform(LALInferenceRunState *runState, struct Data *data, double **hinj, double **winhinj);

void print_single_waveform(struct Data *data, double **h, double **psd, FILE *ofile);

void tf_tracks(double *hoft, double *frequency_out, double deltaT, int N);
void interp_tf(double **tf, double **pts, int tlength, int Nsample);
void median_and_CIs(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform);
void median_and_CIs_fourierdom(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform);
void Q_scan(struct Data *data, double *median_waveform, double Q, double deltaT, double deltaF, double df_resolution, double *invPSD, FILE *outfile);
void Transform(double *hoff, double Q, double dt, double df_resolution, double fmin, double *invPSD, int n, int m, FILE *outfile);

void anderson_darling_test(char *outdir, int ifo, double fmin, double Tobs, char *model, char **ifos);


/* ============================  MAIN PROGRAM  ============================ */

int main(int argc, char *argv[])
{

  /*   Variable declaration   */
  int i, n, ifo, imin, ii;//, imax;

  int COUNT; //total number of chain iterations
  int BURNIN;//burn-in samples (COUNT/2)

  char filename[100];

  /* LAL data structure that will contain all of the data from the frame files */
  LALInferenceRunState *runState = XLALCalloc(1,sizeof(LALInferenceRunState));

  runState->commandLine = LALInferenceParseCommandLine(argc,argv);

  print_version(stdout);

  /******************************************************************************/
  /*                                                                            */
  /*  Usage statment if --help                                                  */
  /*                                                                            */
  /******************************************************************************/

  if(LALInferenceGetProcParamVal(runState->commandLine, "--help") || argc==1)
  {
    print_help_message();
    return 0;
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Read/store  data                                                          */
  /*                                                                            */
  /******************************************************************************/

  /*
   Unlike in BayesWaveBurst, we DO NOT want to read and store the IFO data,
   but we have to use LALInferenceReadData to properly setup the runState
   structure and use the injection software.

   This is a hack to fill the runState detector with zeros so that all we store
   in runState is the injected waveform.
   */

  if( LALInferenceGetProcParamVal(runState->commandLine, "--0noise") )
  {
    fprintf(stdout,"Found --0noise in the command line\n");
  } else {
    fprintf(stdout," \n");
    fprintf(stdout,"Error:  You must include --0noise in the command line\n");
    return 0;
  }

  fprintf(stdout, "\n");
  fprintf(stdout, " ======= LALInferenceReadData() ======\n");
  runState->data = LALInferenceReadData(runState->commandLine);
  fprintf(stdout, "\n");


  /******************************************************************************/
  /*                                                                            */
  /*  read/store data                                                           */
  /*                                                                            */
  /******************************************************************************/
  LALInferenceIFOData *dataPtr = NULL;

  dataPtr = runState->data;

  double *SNRinj = NULL;

  int NI;
  int N = dataPtr->timeData->data->length;
  double Tobs = (double)N*dataPtr->timeData->deltaT;

  ifo=0;
  while(dataPtr!=NULL)
  {
    dataPtr = dataPtr->next;
    ifo++;
  }
  NI=ifo;

  double **freqData = double_matrix(NI-1,N-1);
  double **psd      = double_matrix(NI-1,N/2-1);
  double **one      = double_matrix(NI-1,N/2-1);
  double *Sgeo      = double_vector(N/2-1);

  struct Data *data = malloc(sizeof(struct Data));

  /*
   Unused, but needed for command line and initialization functions
   */
  struct Prior *prior = malloc(sizeof(struct Prior));
  struct Chain *chain = malloc(sizeof(struct Chain));

  const gsl_rng_type *T = gsl_rng_default;
  chain->seed = gsl_rng_alloc(T);
  gsl_rng_env_setup();
  
  parse_command_line(data, chain, prior, runState->commandLine);
    
    
    // Median stuff
    double Q, norm;
    double fres = 8.0; // Resolution freq for Qscans
    int random_draw;

    
    

  /*
   Move to the directory specified by the user, or stay in ./
   */
  chdir(data->outputDirectory);
  fprintf(stdout,"\nOutputting all data to: %s \n\n", data->outputDirectory);

  /*
   OUTPUT DIRECTORY
   */
  char outdir[100];
  sprintf(outdir,"%spost",data->runName);
  mkdir(outdir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  char wavedir[100];
  sprintf(wavedir,"%s/waveforms",outdir);
  mkdir(wavedir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  /*
   Get trigger time in GPS seconds
   */
  LALStatus status;
  char *chartmp=NULL;
  LIGOTimeGPS GPStrig;
  ProcessParamsTable *ppt = NULL;

  memset(&status,0,sizeof(status));
  ppt = LALInferenceGetProcParamVal(runState->commandLine,"--trigtime");
  LALStringToGPS(&status,&GPStrig,ppt->value,&chartmp);


  // burstMDC injection
  int injectionFlag = 0;
  int xmlInjectionFlag = 0;
  if(LALInferenceGetProcParamVal(runState->commandLine, "--MDC-cache"))
  {
    SNRinj = malloc((NI+1)*sizeof(double));
    fprintf(stdout, " ==== LALInferenceInjectFromMDC(): started. ====\n");
    InjectFromMDC(runState->commandLine, runState->data, SNRinj);
    fprintf(stdout, " ==== LALInferenceInjectFromMDC(): finished. ====\n\n");
    free(SNRinj);

    injectionFlag = 1;
  }

  //CBC injection
  if(LALInferenceGetProcParamVal(runState->commandLine, "--inj"))
  {
    fprintf(stdout, " ==== LALInferenceInjectInspiralSignal(): started. ====\n");
    LALInferenceInjectInspiralSignal(runState->data,runState->commandLine);
    fprintf(stdout, " ==== LALInferenceInjectInspiralSignal(): finished. ====\n\n");

    injectionFlag = 1;
    //if(!LALInferenceGetProcParamVal(runState->commandLine, "--lalinspiralinjection"))xmlInjectionFlag = 1;
  }

  
  // Check chirplet flag
  int chirpletFlag = 0;
  if (LALInferenceGetProcParamVal(runState->commandLine, "--chirplets")) {
    chirpletFlag = 1;
  }
  
  
  //Output Fourier domain data for BayesLine
  dataPtr = runState->data;

  double fmin   = dataPtr->fLow;
  double fmax   = dataPtr->fHigh;
  double deltaT = dataPtr->timeData->deltaT;
  double deltaF = dataPtr->freqData->deltaF;

  int tsize = dataPtr->timeData->data->length/4;

  //Copy Fourier domain data & PSD from LAL data types into simple arrays
  ifo=0;
  imin = (int)(fmin*Tobs);
  while(dataPtr!=NULL)
  {
    for(i=0; i<N/2; i++)
    {
      if(i<imin)
      {
        freqData[ifo][2*i]   = 0.0;
        freqData[ifo][2*i+1] = 0.0;
        psd[ifo][i] = 1.0;
        one[ifo][i] = 1.0;
      }
      else
      {
        freqData[ifo][2*i]   = creal(dataPtr->freqData->data->data[i]);
        freqData[ifo][2*i+1] = cimag(dataPtr->freqData->data->data[i]);

        /*
         Innerproducts are expecting <n_i^2> instead of Sn(f):
         So psd arrays == <n_i^2> = T/2 * Sn(f)
         */
        psd[ifo][i] = dataPtr->oneSidedNoisePowerSpectrum->data->data[i]*Tobs/2.0;
        one[ifo][i] = 1.0;
      }
    }
    dataPtr = dataPtr->next;
    ifo++;
  }

  //Storage for h+(f) and hx(f) of all IFOs
  double **freqDataPlus  = double_matrix(NI-1,N-1);
  double **freqDataCross = double_matrix(NI-1,N-1);


  /******************************************************************************/
  /*                                                                            */
  /*  Setup DATA, CHAIN, PRIOR, MODEL structures                                */
  /*                                                                            */
  /******************************************************************************/

  /*
   DATA
   */
  initialize_data(data,freqData,N,tsize,Tobs,NI,fmin,fmax);
  
  // Check chirplet flag
  if (chirpletFlag) data->NW = 6;
  else data->NW = 5;
  
  //Write out time and freq. samples
  write_time_freq_samples(data);

  data->detector  = malloc(NI*sizeof(LALDetector*));
  data->epoch     = runState->data->epoch;

  ifo=0;
  dataPtr = runState->data;
  while(dataPtr!=NULL)
  {
    sprintf(data->ifos[ifo],"%s",dataPtr->name);
    data->detector[ifo] = dataPtr->detector;
    ifo++;
    dataPtr = dataPtr->next;
  }

  /*
   CHAIN
   */
  //chain->NC = chain->NCmin;
  allocate_chain(chain,NI,data->Npol);

  /*
   PRIOR
   */
  data->Dmax = 200;

  int omax = 10;
  if(data->Dmax < omax) omax = data->Dmax;

  //Maximum number of sineGaussians
  prior->gmin = int_vector(data->NI-1);
  prior->gmax = int_vector(data->NI-1);
  //Wavelet parameters
  prior->range = double_matrix(4,1);

  initialize_priors(data, prior, omax);

  /*
   MODEL
   */
  struct Model *model = malloc(sizeof(struct Model));
  initialize_model(model, data, prior, psd, chain->seed);

  //Use Welch averaged PSD from LALInferenceReadData
  for(ifo=0; ifo<data->NI; ifo++)
  {
    for(i=0; i<N/2; i++)
    {
      model->SnS[ifo][i] = psd[ifo][i];
      model->Snf[ifo][i] = psd[ifo][i];
    }
  }

  // BayesWave internal injection
  if(LALInferenceGetProcParamVal(runState->commandLine,"--BW-inject"))
  {
    fprintf(stdout, " ==== BayesWaveInjection(): started. ====\n");
    BayesWaveInjection(runState->commandLine, data, chain, prior, psd, &chain->NC);
    fprintf(stdout, " ==== BayesWaveInjection(): finished. ====\n\n");
    injectionFlag = 1;
  }


  /*
   BAYESLINE PSD
   */
  int PSDimin = 0;
  int PSDimax = data->N/2;
  FILE **splinechain = malloc((NI)*sizeof(FILE*));
  FILE **linechain   = malloc((NI)*sizeof(FILE*));


  /******************************************************************************/
  /*                                                                            */
  /*  Declare the list of metrics to compute as part                            */
  /*  of the post-processing step.                                              */
  /*                                                                            */
  /******************************************************************************/

  fprintf(stdout, "\n");
  fprintf(stdout, " ===== BayesWave Post Processing =====\n");

  //User-friendly arrays for PSD, signal, and injected waveform
  double **hrec      = malloc(NI*sizeof(double*));
  double **hrecPlus  = malloc(NI*sizeof(double*));
  double **hrecCross = malloc(NI*sizeof(double*));
  double **grec      = malloc(NI*sizeof(double*));
  double **hinj      = malloc(NI*sizeof(double*));
  double **hinjPlus  = malloc(NI*sizeof(double*));
  double **hinjCross = malloc(NI*sizeof(double*));
  double **hoft      = malloc(NI*sizeof(double*));
  double **roft      = malloc(NI*sizeof(double*));
  double **hinj_white = malloc(NI*sizeof(double*));

  double **hgeo      = malloc(data->Npol*sizeof(double));

  for(ifo=0; ifo<NI; ifo++)
  {
    hrec[ifo]      = malloc(N*sizeof(double));
    hrecPlus[ifo]  = malloc(N*sizeof(double));
    hrecCross[ifo] = malloc(N*sizeof(double));
    grec[ifo]      = malloc(N*sizeof(double));
    hinj[ifo]      = malloc(N*sizeof(double));
    hinjPlus[ifo]  = malloc(N*sizeof(double));
    hinjCross[ifo] = malloc(N*sizeof(double));
    hoft[ifo]      = malloc(N*sizeof(double));
    roft[ifo]      = malloc(N*sizeof(double));
    hinj_white[ifo]   =malloc(N*sizeof(double));
  }
  
  for(ifo=0; ifo<data->Npol; ifo++)
  {
    hgeo[ifo] = malloc(N*sizeof(double));
  }

  //Fill hinj, hinjPlus, and hCrossPlus with the injected waveform (held in data because did a 0-noise simulation) and its + and x polarizations
  for(ifo=0; ifo<NI; ifo++) for(i=0; i<N; i++) hinj[ifo][i] = data->s[ifo][i];
  if(xmlInjectionFlag)
  {
    for(ifo=0; ifo<NI; ifo++) for(i=0; i<N; i++) hinjPlus[ifo][i] = freqDataPlus[ifo][i];
    for(ifo=0; ifo<NI; ifo++) for(i=0; i<N; i++) hinjCross[ifo][i] = freqDataCross[ifo][i];
  }

  // Different structures for whitened and "colored" moments
  struct Moments *whitenedInjectionMoments      = malloc(sizeof(struct Moments));
  struct Moments *whitenedPlusInjectionMoments  = malloc(sizeof(struct Moments));
  struct Moments *whitenedCrossInjectionMoments = malloc(sizeof(struct Moments));
  struct Moments *whitenedSignalMoments         = malloc(sizeof(struct Moments));
  struct Moments *whitenedPlusSignalMoments     = malloc(sizeof(struct Moments));
  struct Moments *whitenedCrossSignalMoments    = malloc(sizeof(struct Moments));
  struct Moments *whitenedGlitchMoments         = malloc(sizeof(struct Moments));
  struct Moments *whitenedCleanMoments          = malloc(sizeof(struct Moments));
  //struct Moments *whitenedGeocenterMoments      = malloc(sizeof(struct Moments));

  struct Moments *geoInjectionMoments = malloc(sizeof(struct Moments));

  initialize_moments(whitenedInjectionMoments, NI);
  if(xmlInjectionFlag)
  {
    initialize_moments(whitenedPlusInjectionMoments, NI);
    initialize_moments(whitenedCrossInjectionMoments, NI);
  }
  initialize_moments(whitenedSignalMoments, NI);
  initialize_moments(whitenedPlusSignalMoments, NI);
  initialize_moments(whitenedCrossSignalMoments, NI);
  initialize_moments(whitenedGlitchMoments, NI);
  initialize_moments(whitenedCleanMoments, NI);
  //initialize_moments(whitenedGeocenterMoments, NI);

  initialize_moments(geoInjectionMoments, NI);

  struct Moments *coloredInjectionMoments      = malloc(sizeof(struct Moments));
  struct Moments *coloredPlusInjectionMoments  = malloc(sizeof(struct Moments));
  struct Moments *coloredCrossInjectionMoments = malloc(sizeof(struct Moments));
  struct Moments *coloredSignalMoments         = malloc(sizeof(struct Moments));
  struct Moments *coloredPlusSignalMoments     = malloc(sizeof(struct Moments));
  struct Moments *coloredCrossSignalMoments    = malloc(sizeof(struct Moments));
  struct Moments *coloredGlitchMoments         = malloc(sizeof(struct Moments));
  struct Moments *coloredCleanMoments          = malloc(sizeof(struct Moments));
  //struct Moments *coloredGeocenterMoments      = malloc(sizeof(struct Moments));

  initialize_moments(coloredInjectionMoments, NI);
  if(xmlInjectionFlag)
  {
    initialize_moments(coloredPlusInjectionMoments, NI);
    initialize_moments(coloredCrossInjectionMoments, NI);
  }
  initialize_moments(coloredSignalMoments, NI);
  initialize_moments(coloredPlusSignalMoments, NI);
  initialize_moments(coloredCrossSignalMoments, NI);
  initialize_moments(coloredGlitchMoments, NI);
  initialize_moments(coloredCleanMoments, NI);
  //initialize_moments(coloredGeocenterMoments, NI);


  // Keep track of dimension distribution for output stats
  int **dimension = malloc(NI*sizeof(int *));
  for(ifo=0; ifo<NI; ifo++) dimension[ifo] = malloc(data->Dmax*sizeof(int));

  // Shortcut pointers to different members of the Model structre
  struct Wavelet **glitch = NULL;
  struct Wavelet **signal = NULL;

  // Different files for whitened and "colored" output
  FILE **whitenedFile      = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredFile       = malloc((NI+1)*sizeof(FILE*));
  FILE **whitenedPlusFile  = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredPlusFile   = malloc((NI+1)*sizeof(FILE*));
  FILE **whitenedCrossFile = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredCrossFile  = malloc((NI+1)*sizeof(FILE*));

  FILE **whitenedGeocenterFile = malloc(1*sizeof(FILE*));
  FILE **coloredGeocenterFile = malloc(1*sizeof(FILE*));

  // Output injected moments separately
  FILE **whitenedInjFile      = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredInjFile       = malloc((NI+1)*sizeof(FILE*));
  FILE **whitenedPlusInjFile  = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredPlusInjFile   = malloc((NI+1)*sizeof(FILE*));
  FILE **whitenedCrossInjFile = malloc((NI+1)*sizeof(FILE*));
  FILE **coloredCrossInjFile  = malloc((NI+1)*sizeof(FILE*));
  FILE **geoInjFile = malloc(1*sizeof(FILE*));


  // Output a subsample of all whitened waveforms to a single (gigantic) file
  //FILE **recWhitenedSpectrumFile      = malloc(NI*sizeof(FILE*));
  FILE **recWhitenedWaveformFile      = malloc(NI*sizeof(FILE*));
  FILE **recPlusWhitenedWaveformFile  = malloc(NI*sizeof(FILE*));
  FILE **recCrossWhitenedWaveformFile = malloc(NI*sizeof(FILE*));
  FILE **recColoredWaveformFile       = malloc(NI*sizeof(FILE*));
  FILE **recPlusColoredWaveformFile   = malloc(NI*sizeof(FILE*));
  FILE **recCrossColoredWaveformFile  = malloc(NI*sizeof(FILE*));

  FILE **injWhitenedSpectrumFile      = malloc(NI*sizeof(FILE*));
  FILE **injWhitenedWaveformFile      = malloc(NI*sizeof(FILE*));
  FILE **injPlusWhitenedWaveformFile  = malloc(NI*sizeof(FILE*));
  FILE **injCrossWhitenedWaveformFile = malloc(NI*sizeof(FILE*));
  FILE **injColoredWaveformFile       = malloc(NI*sizeof(FILE*));
  FILE **injPlusColoredWaveformFile   = malloc(NI*sizeof(FILE*));
  FILE **injCrossColoredWaveformFile  = malloc(NI*sizeof(FILE*));
    FILE **whitenedInjQscanFile       = malloc(NI*sizeof(FILE*));
    FILE **injectedTfFile             = malloc(NI*sizeof(FILE*));
    
    
    // tf tracks
    //FILE **tfWhitenedFile              = malloc(NI*sizeof(FILE*));
    FILE **medianWaveformFile          = malloc(NI*sizeof(FILE*));
    FILE **medianFrequencyWaveformFile = malloc(NI*sizeof(FILE*));
    FILE **medianPSDFile               = malloc(NI*sizeof(FILE*));
    FILE **medianTimeFrequencyFile     = malloc(NI*sizeof(FILE*));
    FILE **spectrogramFile             = malloc(NI*sizeof(FILE*));
    FILE **medianNoiseFile             = malloc(NI*sizeof(FILE*));
    
  /********************************/
  /*                              */
  /*  Post process data           */
  /*                              */
  /********************************/

  // read data from file made by BayesWaveBurst
  FILE *FDdata = NULL;

  // Output a subsample of all whitened waveforms to a single (gigantic) file
  FILE **whitenedDataFile      = malloc(NI*sizeof(FILE*));
  FILE **coloredDataFile       = malloc(NI*sizeof(FILE*));
  FILE **whitenedDataQscanFile = malloc(NI*sizeof(FILE*));

  double junk;

    FILE **noiseResidualFile = malloc(NI*sizeof(FILE *));
    
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"waveforms/%sfourier_domain_data_%s.dat",data->runName,data->ifos[ifo]);

    if( (FDdata = fopen(filename,"r")) == NULL)
    {
      fprintf(stdout,"Could not find %s.  Filling data array with 0's\n",filename);
      for(i=0; i<data->N; i++) hoft[ifo][i] = 0.0;

      fclose(FDdata);
    }
    else
    {
      for(i=0; i<data->N/2; i++) fscanf(FDdata,"%lg %lg %lg",&junk,&hoft[ifo][2*i],&hoft[ifo][2*i+1]);
      fclose(FDdata);

      sprintf(filename,"waveforms/%sfourier_domain_residual_%s.dat",data->runName,data->ifos[ifo]);
      if( (FDdata = fopen(filename,"r")) == NULL)
      {
        for(i=0; i<data->N/2; i++)
        {
          roft[ifo][2*i]   = hoft[ifo][2*i];
          roft[ifo][2*i+1] = hoft[ifo][2*i+1];
        }
      }
      else
      {
        for(i=0; i<data->N/2; i++) fscanf(FDdata,"%lg %lg %lg",&junk,&roft[ifo][2*i],&roft[ifo][2*i+1]);
        fclose(FDdata);
      }
      fprintf(stdout, "Getting data into same format as waveforms\n");

    }
      
      sprintf(filename,"%s/fourier_domain_noise_residual_%s.dat",outdir,data->ifos[ifo]);
      noiseResidualFile[ifo] = fopen(filename,"w");
      
      for (ii = 0; ii < data->N/2; ii++) {
          fprintf(noiseResidualFile[ifo],"%g %g %g\n",(double)ii*deltaF,roft[ifo][2*ii],roft[ifo][2*ii+1]);
      }
      fclose(noiseResidualFile[ifo]);
      
      
  }
    
    free(noiseResidualFile);

  //print fourier-domain data and model PSD
  FILE *PSDfile=NULL;
  double dpower;
  double rpower;
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"%s/gaussian_noise_model_%s.dat",outdir,data->ifos[ifo]);
    PSDfile = fopen(filename,"w");
    for(i=PSDimin; i<PSDimax; i++)
    {
      dpower = hoft[ifo][2*i]*hoft[ifo][2*i]+hoft[ifo][2*i+1]*hoft[ifo][2*i+1];
      rpower = roft[ifo][2*i]*roft[ifo][2*i]+roft[ifo][2*i+1]*roft[ifo][2*i+1];
      fprintf(PSDfile,"%g %g %g %g\n",(double)i/data->Tobs,dpower,rpower,psd[ifo][i]);
    }
    fclose(PSDfile);

    sprintf(filename,"%s/colored_data_%s.dat",outdir,data->ifos[ifo]);
    coloredDataFile[ifo] = fopen(filename,"w");

    sprintf(filename,"%s/whitened_data_%s.dat",outdir,data->ifos[ifo]);
    whitenedDataFile[ifo] = fopen(filename,"w");
      

  }

  //print time-domain data
  print_waveforms(data, hoft, psd, whitenedDataFile);
  print_waveforms(data, hoft, one, coloredDataFile);
    
    double **hdata = double_matrix(NI-1,data->N-1);
    double **htemp = double_matrix(NI-1,data->N-1);
    double **invPSD = double_matrix(NI-1,data->N/2-1);
    
    
    for(ifo=0; ifo<NI; ifo++){
        
        
        whiten_data(data->imin, data->imax, hoft[ifo], psd[ifo]);
        
        
        get_time_domain_waveforms(hdata[ifo], hoft[ifo], data->N, data->imin, data->imax);
        
        norm = sqrt((double)data->N);
        for (ii = 0; ii < data->N; ii++) {
            hdata[ifo][ii] /= norm;
        }
        
        Q = 4.0;
        sprintf(filename,"%s/data_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
        whitenedDataQscanFile[ifo] = fopen(filename,"w");
        Q_scan(data, hdata[ifo], Q, deltaT, deltaF, fres, one[ifo],whitenedDataQscanFile[ifo]);
        fclose(whitenedDataQscanFile[ifo]);
       
        
        Q = 8.0;
        sprintf(filename,"%s/data_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
        whitenedDataQscanFile[ifo] = fopen(filename,"w");
        Q_scan(data, hdata[ifo], Q, deltaT, deltaF, fres, one[ifo],whitenedDataQscanFile[ifo]);
        fclose(whitenedDataQscanFile[ifo]);
        
        Q = 16.0;
        sprintf(filename,"%s/data_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
        whitenedDataQscanFile[ifo] = fopen(filename,"w");
        Q_scan(data, hdata[ifo], Q, deltaT, deltaF, fres, one[ifo],whitenedDataQscanFile[ifo]);
        fclose(whitenedDataQscanFile[ifo]);
        

        
        for (ii = 0; ii < data->N/2; ii++) {
            invPSD[ifo][ii] = 1.0/psd[ifo][ii];
        }
        
        //unwhiten-- need hoft to check fourier domain residuals
        whiten_data(data->imin, data->imax, hoft[ifo], invPSD[ifo]);
        

        
        
    }


  for(ifo=0; ifo<NI; ifo++)
  {
    fclose(whitenedDataFile[ifo]);
    fclose(coloredDataFile[ifo]);
  }

  free(whitenedDataFile);
  free(coloredDataFile);
    free(whitenedDataQscanFile);

    

  /********************************/
  /*                              */
  /*  Post process injection      */
  /*                              */
  /********************************/
  if(injectionFlag)
  {

      
    fprintf(stdout, "Computing moments for injected waveform\n");

    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/injected_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
      injWhitenedWaveformFile[ifo] = fopen(filename,"w");
      sprintf(filename,"%s/injected_whitened_spectrum_%s.dat",outdir,data->ifos[ifo]);
      injWhitenedSpectrumFile[ifo] = fopen(filename,"w");
      if(xmlInjectionFlag)
      {
        sprintf(filename,"%s/injected_plus_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
        injPlusWhitenedWaveformFile[ifo] = fopen(filename,"w");
        sprintf(filename,"%s/injected_cross_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
        injCrossWhitenedWaveformFile[ifo] = fopen(filename,"w");
      }

      sprintf(filename,"%s/injected_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
      injColoredWaveformFile[ifo] = fopen(filename,"w");
      if(xmlInjectionFlag)
      {
        sprintf(filename,"%s/injected_plus_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
        injPlusColoredWaveformFile[ifo] = fopen(filename,"w");
        sprintf(filename,"%s/injected_cross_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
        injCrossColoredWaveformFile[ifo] = fopen(filename,"w");
      }

        
        sprintf(filename,"%s/injected_tf_%s.dat",outdir,data->ifos[ifo]);
        injectedTfFile[ifo] = fopen(filename,"w");
        



    }

    setup_output_files(NI, injectionFlag, outdir, "injection", "whitened", whitenedInjFile, data);
    setup_output_files(NI, injectionFlag, outdir, "injection", "colored", coloredInjFile, data);

    setup_output_files(-1, injectionFlag, outdir, "injection", "geocenter", geoInjFile, data);

    if(xmlInjectionFlag)
    {
      setup_output_files(NI, injectionFlag, outdir, "injectionPlus", "whitened", whitenedPlusInjFile,data);
      setup_output_files(NI, injectionFlag, outdir, "injectionPlus", "colored", coloredPlusInjFile, data);
      setup_output_files(NI, injectionFlag, outdir, "injectionCross", "whitened", whitenedCrossInjFile, data);
      setup_output_files(NI, injectionFlag, outdir, "injectionCross", "colored", coloredCrossInjFile,data);
    }

    for(ifo=0; ifo<NI; ifo++)
    {
      whitenedInjectionMoments->overlap[ifo] = 1.0;
      coloredInjectionMoments->overlap[ifo]  = 1.0;
      if(xmlInjectionFlag)
      {
        whitenedPlusInjectionMoments->overlap[ifo]  = 1.0;
        coloredPlusInjectionMoments->overlap[ifo]   = 1.0;
        whitenedCrossInjectionMoments->overlap[ifo] = 1.0;
        coloredCrossInjectionMoments->overlap[ifo]  = 1.0;
      }
    }
    whitenedInjectionMoments->networkOverlap = 1.0;
    coloredInjectionMoments->networkOverlap  = 1.0;
    geoInjectionMoments->networkOverlap = 1.0;

    ComputeWaveformMoments(data, deltaF, deltaT, hinj, psd, whitenedInjectionMoments); // INJECTION
    ComputeWaveformMoments(data, deltaF, deltaT, hinj, one, coloredInjectionMoments);

    /* Calculate "geocenter" */
    //ComputeGeocenterMoments(data, deltaF, deltaT, hinj[0], one[0], geoInjectionMoments);
    //print_moments(0, injectionFlag, geoInjFile, geoInjectionMoments);

    if(xmlInjectionFlag)
    {
      ComputeWaveformMoments(data, deltaF, deltaT, hinjPlus, psd, whitenedPlusInjectionMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hinjPlus, one, coloredPlusInjectionMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hinjCross, psd, whitenedCrossInjectionMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hinjCross, one, coloredCrossInjectionMoments);
    }

    
    // Whitened hrss is meaningless and colored SNR are meaningless
     //Swap their values in the structures so the moments files are self-contained

     for(ifo=0; ifo<NI; ifo++)
     {
     whitenedInjectionMoments->energy[ifo] = coloredInjectionMoments->energy[ifo];
     coloredInjectionMoments->snr[ifo]     = whitenedInjectionMoments->snr[ifo];
     }
     

    //Detector-by-detector moments
    for(ifo=0; ifo<NI; ifo++)
    {
      print_moments(ifo, injectionFlag, whitenedInjFile, whitenedInjectionMoments);
      print_moments(ifo, injectionFlag, coloredInjFile, coloredInjectionMoments);
      if(xmlInjectionFlag)
      {
        print_moments(ifo, injectionFlag, whitenedPlusInjFile, whitenedPlusInjectionMoments);
        print_moments(ifo, injectionFlag, coloredPlusInjFile, coloredPlusInjectionMoments);
        print_moments(ifo, injectionFlag, whitenedCrossInjFile, whitenedCrossInjectionMoments);
        print_moments(ifo, injectionFlag, coloredCrossInjFile, coloredCrossInjectionMoments);
      }
    }


    //Network moments
    /*
     print_moments(NI, injectionFlag, whitenedInjFile, whitenedInjectionMoments);
     print_moments(NI, injectionFlag, coloredInjFile, coloredInjectionMoments);
     if(xmlInjectionFlag)
     {
     print_moments(NI, injectionFlag, whitenedPlusInjFile, whitenedPlusInjectionMoments);
     print_moments(NI, injectionFlag, coloredPlusInjFile, coloredPlusInjectionMoments);
     print_moments(NI, injectionFlag, whitenedCrossInjFile, whitenedCrossInjectionMoments);
     print_moments(NI, injectionFlag, coloredCrossInjFile, coloredCrossInjectionMoments);
     }
     */

    //Print injected time-domain waveforms (and their + and x polarizations if possible)
    if(xmlInjectionFlag)
    {
        
        printf("???\n");

        
      //LALinference injections are windowed to improve FFTs to the time domain
      double **windowed_injection = malloc(NI*sizeof(double*));
      for(ifo=0; ifo<NI; ifo++) windowed_injection[ifo] = malloc(N*sizeof(double));
      //Window and output hinj in the time domain
      window_fourier_waveform(runState, data, hinj, windowed_injection);
      print_waveforms(data, windowed_injection, psd, injWhitenedWaveformFile);
      print_waveforms(data, windowed_injection, one, injColoredWaveformFile);
      print_spectrum(data, windowed_injection, injWhitenedSpectrumFile);
      //Window and output hinjPlus in the time domain
      window_fourier_waveform(runState, data, hinjPlus, windowed_injection);
      print_waveforms(data, windowed_injection, psd, injPlusWhitenedWaveformFile);
      print_waveforms(data, windowed_injection, one, injPlusColoredWaveformFile);
      //Window and output hinjCross in the time domain
      window_fourier_waveform(runState, data, hinjCross, windowed_injection);
      print_waveforms(data, windowed_injection, psd, injCrossWhitenedWaveformFile);
      print_waveforms(data, windowed_injection, one, injCrossColoredWaveformFile);
        
        for (ifo = 0; ifo < NI; ifo++) {
            
            for (i = 0; i<data->N; i++) {
                hinj_white[ifo][i] = hinj[ifo][i];
            }
            
            whiten_data(data->imin, data->imax, hinj_white[ifo], psd[ifo]);
            
            get_time_domain_waveforms(htemp[ifo], hinj_white[ifo], data->N, data->imin, data->imax);
            
            
            Q = 4.0;
            sprintf(filename, "%s/injected_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
            whitenedInjQscanFile[ifo] = fopen(filename,"w");
            Q_scan(data, htemp[ifo], Q, deltaT, deltaF, fres, one[ifo], whitenedInjQscanFile[ifo]);
            fclose(whitenedInjQscanFile[ifo]);
            
            Q = 8.0;
            sprintf(filename, "%s/injected_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
            whitenedInjQscanFile[ifo] = fopen(filename,"w");
            Q_scan(data, htemp[ifo], Q, deltaT, deltaF, fres, one[ifo], whitenedInjQscanFile[ifo]);
            fclose(whitenedInjQscanFile[ifo]);
            
            Q = 16.0;
            sprintf(filename, "%s/injected_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
            whitenedInjQscanFile[ifo] = fopen(filename,"w");
            Q_scan(data, htemp[ifo], Q, deltaT, deltaF, fres, one[ifo], whitenedInjQscanFile[ifo]);
            fclose(whitenedInjQscanFile[ifo]);
            
        }

        

    }
    else
    {
      print_waveforms(data, hinj, psd, injWhitenedWaveformFile);
      print_waveforms(data, hinj, one, injColoredWaveformFile);

      print_spectrum(data, hinj, injWhitenedSpectrumFile);
        
        
        for (ifo = 0; ifo < NI; ifo++) {
            
            for (i = 0; i<data->N; i++) {
                hinj_white[ifo][i] = hinj[ifo][i];
            }
            
            whiten_data(data->imin, data->imax, hinj_white[ifo], psd[ifo]);
            
            get_time_domain_waveforms(htemp[ifo], hinj_white[ifo], data->N, data->imin, data->imax);
            
            get_time_domain_waveforms(htemp[ifo], hinj_white[ifo], data->N, data->imin, data->imax);
            
            
            Q = 4.0;
            sprintf(filename, "%s/injected_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
            whitenedInjQscanFile[ifo] = fopen(filename,"w");
            Q_scan(data, htemp[ifo], Q, deltaT, deltaF, fres, one[ifo], whitenedInjQscanFile[ifo]);
            fclose(whitenedInjQscanFile[ifo]);
            
            Q = 8.0;
            sprintf(filename, "%s/injected_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
            whitenedInjQscanFile[ifo] = fopen(filename,"w");
            Q_scan(data, htemp[ifo], Q, deltaT, deltaF, fres, one[ifo], whitenedInjQscanFile[ifo]);
            fclose(whitenedInjQscanFile[ifo]);
            
            Q = 16.0;
            sprintf(filename, "%s/injected_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
            whitenedInjQscanFile[ifo] = fopen(filename,"w");
            Q_scan(data, htemp[ifo], Q, deltaT, deltaF, fres, one[ifo], whitenedInjQscanFile[ifo]);
            fclose(whitenedInjQscanFile[ifo]);
            
            // injected tf
            tf_tracks(htemp[ifo], hinj_white[ifo], deltaT, data->N);
            for (i = 0; i < data->N; i++) {
                fprintf(injectedTfFile[ifo],"%g %g\n",i*deltaT,hinj_white[ifo][i]);
            }
            
        }
        

        
    }

    for(ifo=0; ifo<NI; ifo++)
    {
      fclose(injWhitenedWaveformFile[ifo]);
      fclose(injColoredWaveformFile[ifo]);
      fclose(whitenedInjFile[ifo]);
      fclose(coloredInjFile[ifo]);
      if(xmlInjectionFlag)
      {
        fclose(injPlusWhitenedWaveformFile[ifo]);
        fclose(injCrossWhitenedWaveformFile[ifo]);
        fclose(injPlusColoredWaveformFile[ifo]);
        fclose(injCrossColoredWaveformFile[ifo]);
        fclose(whitenedPlusInjFile[ifo]);
        fclose(whitenedCrossInjFile[ifo]);
        fclose(coloredPlusInjFile[ifo]);
        fclose(coloredCrossInjFile[ifo]);
          fclose(injectedTfFile[ifo]);
      }
    }

    fclose(geoInjFile[0]);
    free(geoInjFile);

    free(injWhitenedWaveformFile);
    free(injColoredWaveformFile);
    free(whitenedInjFile);
    free(coloredInjFile);
    if(xmlInjectionFlag)
    {
      free(injPlusWhitenedWaveformFile);
      free(injCrossWhitenedWaveformFile);
      free(injPlusColoredWaveformFile);
      free(injCrossColoredWaveformFile);
      free(whitenedPlusInjFile);
      free(whitenedCrossInjFile);
      free(coloredPlusInjFile);
      free(coloredCrossInjFile);
        free(injectedTfFile);
        
        
    }
      free(whitenedInjQscanFile);
      free_double_matrix(htemp,NI-1);
      

  }



  /********************************/
  /*                              */
  /*  Post process signal model   */
  /*                              */
  /********************************/

  //File containing signal-model waveform parameters
  sprintf(filename,"chains/%ssignal_params_h0.dat.0",data->runName);

  FILE **signalParams = malloc(data->Npol*sizeof(FILE *));
    
    
  // FILE *temp;
  if( (signalParams[0] = fopen(filename,"r")) == NULL)
  {
    fprintf(stdout,"Could not find %s.  Skipping to glitch model\n",filename);
  }
  else
  {
    fprintf(stdout, "Starting loop over %s  ...\n", filename);

    signal = model->signal;
    signal[0] = model->signal[0];

    if(data->Npol > 1)
    {
      for(ifo=1; ifo<data->Npol; ifo++)
      {
        sprintf(filename,"chains/%ssignal_params_h%i.dat.0",data->runName, ifo);
        signalParams[ifo] = fopen(filename,"r");
        
        signal[ifo] = model->signal[ifo];
      }
    }
    
    for(ifo=0; ifo<data->Npol; ifo++)
      discard_burnin_samples(signalParams[ifo], &COUNT, &BURNIN);

    
    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/signal_recovered_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
      recWhitenedWaveformFile[ifo] = fopen(filename,"w");
      //sprintf(filename,"%s/signal_recovered_whitened_spectrum_%s.dat",outdir,ifo);
      //recWhitenedSpectrumFile[ifo] = fopen(filename,"w");
      sprintf(filename,"%s/signal_recovered_plus_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
      recPlusWhitenedWaveformFile[ifo] = fopen(filename,"w");
      sprintf(filename,"%s/signal_recovered_cross_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
      recCrossWhitenedWaveformFile[ifo] = fopen(filename,"w");

      sprintf(filename,"%s/signal_recovered_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
      recColoredWaveformFile[ifo] = fopen(filename,"w");
      sprintf(filename,"%s/signal_recovered_plus_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
      recPlusColoredWaveformFile[ifo] = fopen(filename,"w");
      sprintf(filename,"%s/signal_recovered_cross_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
      recCrossColoredWaveformFile[ifo] = fopen(filename,"w");
        
        
        //sprintf(filename,"%s/signal_tf_whitened_%s.dat",outdir,data->ifos[ifo]);
        //tfWhitenedFile[ifo] = fopen(filename,"w");
        
        
        sprintf(filename,"%s/signal_median_time_domain_waveform_%s.dat",outdir,data->ifos[ifo]);
        medianWaveformFile[ifo] = fopen(filename,"w");
        
        sprintf(filename,"%s/signal_median_frequency_domain_waveform_%s.dat",outdir,data->ifos[ifo]);
        medianFrequencyWaveformFile[ifo] = fopen(filename,"w");
        
        sprintf(filename,"%s/signal_median_PSD_%s.dat",outdir,data->ifos[ifo]);
        medianPSDFile[ifo] = fopen(filename,"w");
        
        sprintf(filename,"%s/signal_median_tf_%s.dat",outdir,data->ifos[ifo]);
        medianTimeFrequencyFile[ifo] = fopen(filename,"w");
        
//        sprintf(filename,"%s/signal_spectrogram_%s.dat",outdir,ifo);
//        spectrogramFile[ifo] = fopen(filename,"w");
        
        
    }

    setup_output_files(NI, injectionFlag, outdir, "signal", "whitened", whitenedFile, data);
    setup_output_files(NI, injectionFlag, outdir, "signal", "colored", coloredFile,data);
    setup_output_files(-1, injectionFlag, outdir, "signal", "whitened", whitenedGeocenterFile,data);
    setup_output_files(-1, injectionFlag, outdir, "signal", "colored", coloredGeocenterFile,data);
    setup_output_files(NI, injectionFlag, outdir, "plusSignal", "whitened", whitenedPlusFile,data);
    setup_output_files(NI, injectionFlag, outdir, "plusSignal", "colored", coloredPlusFile,data);
    setup_output_files(NI, injectionFlag, outdir, "crossSignal", "whitened", whitenedCrossFile,data);
    setup_output_files(NI, injectionFlag, outdir, "crossSignal", "colored", coloredCrossFile,data);

    for(ifo=0; ifo<NI; ifo++) for(n=0; n<data->Dmax; n++) dimension[ifo][n] = 0;


    //BayesLine
    if(data->bayesLineFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"chains/%ssignal_spline_%s.dat.0",data->runName,data->ifos[ifo]);
        splinechain[ifo] = fopen(filename,"r");

        sprintf(filename,"chains/%ssignal_lorentz_%s.dat.0",data->runName,data->ifos[ifo]);
        linechain[ifo] = fopen(filename,"r");

        discard_burnin_samples(splinechain[ifo], &COUNT, &BURNIN);
        discard_burnin_samples(linechain[ifo], &COUNT, &BURNIN);
      }
    }
      
      // Set up arrays for waveforms, tf tracks, PSD, etc
      
      int Nwaveform_signal = COUNT-BURNIN;
      printf("Nwave = %i\n",Nwaveform_signal);
      
      double **hoft_median    = double_matrix(NI-1,N-1);
      double **hoff_median    = double_matrix(NI-1,N/2-1);
      double **psd_median     = double_matrix(NI-1,N/2-1);
      double **tftrack_median = double_matrix(NI-1,N-1);
      
      double ***waveform_mega = double_tensor(NI-1,Nwaveform_signal-1,N-1);
      double ***psd_mega      = double_tensor(NI-1,Nwaveform_signal-1,N/2-1);
      
      FILE **signalResidualFile = malloc(NI*sizeof(FILE *));

      

    for(n=0; n<COUNT-BURNIN; n++)
    {
      /******************************************************************************/
      /*                                                                            */
      /*  Get current state of MCMC output into format for BayesWave code library   */
      /*                                                                            */
      /******************************************************************************/

      /*
       SIGNAL MODEL
       */
      parse_signal_parameters(data, model, signalParams, hrec, hrecPlus, hrecCross);




      for(ifo=0; ifo<data->Npol; ifo++) for(i=0; i<N; i++) hgeo[ifo][i] = model->signal[ifo]->templates[i];


      /*
       PSD MODEL
       */
      if(data->bayesLineFlag)parse_bayesline_parameters(data, model, splinechain, linechain, psd);


      Shf_Geocenter_full(data, model->projection, model->Snf, Sgeo, model->extParams);
      dimension[0][signal[0]->size]++;

      /******************************************************************************/
      /*                                                                            */
      /*  Compute various moments/overlaps to quantify waveform reconstruction      */
      /*                                                                            */
      /******************************************************************************/

      /*
       MATCH BETWEEN INJECTED AND RECOVERED WAVEFORM
       */

      if(injectionFlag)
      {
        ComputeWaveformOverlap(data, hinj, hrec, psd, whitenedSignalMoments);
        ComputeWaveformOverlap(data, hinj, hrec, one, coloredSignalMoments);
        if(xmlInjectionFlag)
        {
          ComputeWaveformOverlap(data, hinjPlus, hrecPlus, psd, whitenedPlusSignalMoments);
          ComputeWaveformOverlap(data, hinjPlus, hrecPlus, one, coloredPlusSignalMoments);
          ComputeWaveformOverlap(data, hinjCross, hrecCross, psd, whitenedCrossSignalMoments);
          ComputeWaveformOverlap(data, hinjCross, hrecCross, one, coloredCrossSignalMoments);
        }
      }

      ComputeWaveformMoments(data, deltaF, deltaT, hrec, psd, whitenedSignalMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hrec, one, coloredSignalMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hrecPlus, psd, whitenedPlusSignalMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hrecPlus, one, coloredPlusSignalMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hrecCross, psd, whitenedCrossSignalMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, hrecCross, one, coloredCrossSignalMoments);

      //      if(n%2000==0){
      //        PrintRhof(data, deltaF, deltaT, hrec, psd, whitenedSignalMoments);
      //      }


      //ComputeGeocenterMoments(data, deltaF, deltaT, hgeo, Sgeo, whitenedGeocenterMoments);
      //ComputeGeocenterMoments(data, deltaF, deltaT, hgeo, one[0], coloredGeocenterMoments);

      /*
       Whitened hrss is meaningless and colored SNR are meaningless
       Swap their values in the structures so the moments files are self-contained
       */
      for(ifo=0; ifo<NI; ifo++)
      {
        //        coloredSignalMoments->energy[ifo] = whitenedSignalMoments->energy[ifo];
        //        whitenedSignalMoments->snr[ifo] = coloredSignalMoments->snr[ifo];
        //
        //        coloredPlusSignalMoments->energy[ifo] = whitenedPlusSignalMoments->energy[ifo];
        //        whitenedPlusSignalMoments->snr[ifo] = coloredPlusSignalMoments->snr[ifo];
        //
        //        coloredCrossSignalMoments->energy[ifo] = whitenedCrossSignalMoments->energy[ifo];
        //        whitenedCrossSignalMoments->snr[ifo] = coloredCrossSignalMoments->snr[ifo];
        //
        //whitenedGeocenterMoments->energy[ifo] = coloredGeocenterMoments->energy[ifo];
        //coloredGeocenterMoments->snr[ifo]     = whitenedGeocenterMoments->snr[ifo];

        whitenedPlusSignalMoments->energy[ifo] = coloredPlusSignalMoments->energy[ifo];
        coloredPlusSignalMoments->snr[ifo]     = whitenedPlusSignalMoments->snr[ifo];

        whitenedCrossSignalMoments->energy[ifo] = coloredCrossSignalMoments->energy[ifo];
        coloredCrossSignalMoments->snr[ifo]     = whitenedCrossSignalMoments->snr[ifo];
          
          whitenedSignalMoments->energy[ifo] = coloredSignalMoments->energy[ifo];
          coloredSignalMoments->snr[ifo]     = whitenedSignalMoments->snr[ifo];
          
          
      }
//      whitenedSignalMoments->energy[0] = coloredSignalMoments->energy[0];
//      coloredSignalMoments->snr[0]     = whitenedSignalMoments->snr[0];

      if(signal[0]->size>0)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          print_moments(ifo, injectionFlag, whitenedFile, whitenedSignalMoments);
          print_moments(ifo, injectionFlag, coloredFile, coloredSignalMoments);
          print_moments(ifo, injectionFlag, whitenedPlusFile, whitenedPlusSignalMoments);
          print_moments(ifo, injectionFlag, coloredPlusFile, coloredPlusSignalMoments);
          print_moments(ifo, injectionFlag, whitenedCrossFile, whitenedCrossSignalMoments);
          print_moments(ifo, injectionFlag, coloredCrossFile, coloredCrossSignalMoments);
        }
        //print_moments(0, injectionFlag, whitenedGeocenterFile, whitenedGeocenterMoments);
        //print_moments(0, injectionFlag, coloredGeocenterFile, coloredGeocenterMoments);
      }

        
        // Median waveforms etc
 
        for(ifo = 0; ifo<NI; ifo++){
            
            for (ii = 0; ii < data->N/2; ii++) {
                waveform_mega[ifo][n][2*ii] = hrec[ifo][2*ii];
                waveform_mega[ifo][n][2*ii+1] = hrec[ifo][2*ii+1];
                
                psd_mega[ifo][n][ii] = psd[ifo][ii];

            }
        }
        
      //Print reconstructed time-domain waveforms and their + and x polarizations
        
        
        
        
        
      if(n%100 == 0)
      {
        /*
         sprintf(filename,"%s/temp_%s_%s.dat",outdir,ifo,n);
         temp = fopen(filename,"w");
         print_single_waveform(data, hrec, psd, temp);
         fclose(temp);
         */
        print_waveforms(data, hrec, psd, recWhitenedWaveformFile);
        print_waveforms(data, hrec, one, recColoredWaveformFile);
        print_waveforms(data, hrecPlus, psd, recPlusWhitenedWaveformFile);
        print_waveforms(data, hrecPlus, one, recPlusColoredWaveformFile);
        print_waveforms(data, hrecCross, psd, recCrossWhitenedWaveformFile);
        print_waveforms(data, hrecCross, one, recCrossColoredWaveformFile);

      }

    }

      
      
      
    for(ifo=0; ifo<data->Npol; ifo++) fclose(signalParams[ifo]);
    free(signalParams);
      
      double **hres = double_matrix(NI-1,N-1);
      
      random_draw = (double)(Nwaveform_signal)*uniform_draw(chain->seed);
      printf("random_draw = %i\n",random_draw);
      
      // Calculate median
      for (ifo=0; ifo<NI; ifo++) {
          
          // Print fourier domain residuals from a random draw from the chain
          sprintf(filename,"%s/fourier_domain_signal_residual_%s.dat",outdir,data->ifos[ifo]);
          signalResidualFile[ifo] = fopen(filename,"w");
          
          // Pad with zeros upto imin
          
          for (ii = 0; ii < data->imin; ii++) {
              fprintf(signalResidualFile[ifo],"%g %g %g\n", (double)ii*deltaF, 0.0, 0.0);
          }
          
          for (ii = data->imin; ii < data->N/2; ii++) {
              fprintf(signalResidualFile[ifo],"%g %g %g\n", (double)ii*deltaF, hoft[ifo][2*ii] - waveform_mega[ifo][random_draw][2*ii], hoft[ifo][2*ii+1] - waveform_mega[ifo][random_draw][2*ii+1]);
          }
          fclose(signalResidualFile[ifo]);

          
          // Calc and print median PSD
          median_and_CIs(psd_mega[ifo], N/2, Nwaveform_signal, deltaF, medianPSDFile[ifo], psd_median[ifo]);
          
          // Use "psd" vector to store glitch power spectrum, then calc and pringt median
          for (n = 0; n < Nwaveform_signal; n++) {
              for (ii = 0; ii < N/2; ii++) {
                  psd_mega[ifo][n][ii] = waveform_mega[ifo][n][2*ii]*waveform_mega[ifo][n][2*ii]+waveform_mega[ifo][n][2*ii+1]*waveform_mega[ifo][n][2*ii+1];
              }
          }
          median_and_CIs(psd_mega[ifo], N/2, Nwaveform_signal, deltaF, medianFrequencyWaveformFile[ifo], hoff_median[ifo]);


          
          // Get (whitened) time domain waveforms, calc a print median
          for (n = 0; n < Nwaveform_signal; n++) {
              whiten_data(data->imin, data->imax, waveform_mega[ifo][n], psd[ifo]);
              get_time_domain_waveforms(waveform_mega[ifo][n], waveform_mega[ifo][n], data->N, data->imin, data->imax);
              norm = sqrt((double)data->N);
              for(ii = 0; ii< data->N; ii++){
                  waveform_mega[ifo][n][ii] /= norm;
              }
              
          }
          median_and_CIs(waveform_mega[ifo], N, Nwaveform_signal, deltaT, medianWaveformFile[ifo], hoft_median[ifo]);
          

          // Use mega waveform vector to store tf tracks. Again, calc and print median
          for (n = 0; n < Nwaveform_signal; n++) {
              tf_tracks(waveform_mega[ifo][n], waveform_mega[ifo][n], deltaT, N);
          }
          median_and_CIs(waveform_mega[ifo], N, Nwaveform_signal, deltaT, medianTimeFrequencyFile[ifo], tftrack_median[ifo]);

          

          
          
         // Q transform spectrogram
          Q = 4.0;
          sprintf(filename,"%s/signal_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hoft_median[ifo], Q, deltaT, deltaF, fres, one[ifo],spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 8.0;
          sprintf(filename,"%s/signal_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hoft_median[ifo], Q, deltaT, deltaF, fres, one[ifo], spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 16.0;
          sprintf(filename,"%s/signal_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hoft_median[ifo], Q, deltaT, deltaF, fres, one[ifo],spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          

          //// residual spectrograms /////

          
          for (ii = 0; ii < data->N; ii++) {
              hres[ifo][ii] = hdata[ifo][ii] - hoft_median[ifo][ii];
          }
          
          Q = 4.0;
          sprintf(filename,"%s/signal_residual_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hres[ifo], Q, deltaT, deltaF, fres, one[ifo],spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 8.0;
          sprintf(filename,"%s/signal_residual_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hres[ifo], Q, deltaT, deltaF, fres, one[ifo], spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 16.0;
          sprintf(filename,"%s/signal_residual_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hres[ifo], Q, deltaT, deltaF, fres, one[ifo],spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
      }
      free_double_matrix(hres,NI-1);
      
      
      
      
      

    for(ifo=0; ifo<NI; ifo++)
    {
      if(data->bayesLineFlag)
      {
        fclose(splinechain[ifo]);
        fclose(linechain[ifo]);
      }

      fclose(recWhitenedWaveformFile[ifo]);
      fclose(recColoredWaveformFile[ifo]);
      //fclose(recWhitenedSpectrumFile[ifo]);
      fclose(whitenedFile[ifo]);
      fclose(coloredFile[ifo]);
      fclose(recPlusWhitenedWaveformFile[ifo]);
      fclose(recCrossWhitenedWaveformFile[ifo]);
      fclose(recPlusColoredWaveformFile[ifo]);
      fclose(recCrossColoredWaveformFile[ifo]);
      fclose(whitenedPlusFile[ifo]);
      fclose(whitenedCrossFile[ifo]);
      fclose(coloredPlusFile[ifo]);
      fclose(coloredCrossFile[ifo]);
        
        //fclose(tfWhitenedFile[ifo]);
        
        fclose(medianWaveformFile[ifo]);
        fclose(medianFrequencyWaveformFile[ifo]);
        fclose(medianPSDFile[ifo]);
        fclose(medianTimeFrequencyFile[ifo]);
        //fclose(spectrogramFile[ifo]);
        
        
    }
    fclose(whitenedGeocenterFile[0]);
    fclose(coloredGeocenterFile[0]);
      
      
      free_double_matrix(hoft_median, NI-1);
      free_double_matrix(hoff_median, NI-1);
      free_double_matrix(psd_median, NI-1);
      free_double_matrix(tftrack_median, NI-1);
      
      free_double_tensor(waveform_mega,NI-1,Nwaveform_signal-1);
      free_double_tensor(psd_mega,NI-1,Nwaveform_signal-1);
      
      free(signalResidualFile);
      
    print_stats(data, GPStrig, injectionFlag, outdir, "signal", dimension);

    for(ifo=0; ifo<NI; ifo++)
    {
      anderson_darling_test(outdir, ifo, data->fmin, data->Tobs, "signal", data->ifos);
    }


  } //End post processing of signal model





  /********************************/
  /*                              */
  /*  Post process glitch model   */
  /*                              */
  /********************************/

  FILE **glitchParams = malloc(NI*sizeof(FILE *));

  sprintf(filename,"chains/%sglitch_params_%s.dat.0",data->runName,data->ifos[0]);

  if( (glitchParams[0] = fopen(filename,"r")) == NULL)
  {
    fprintf(stdout,"Could not find %s.  Skipping to cleaning model\n",filename);
  }
  else
  {
    fprintf(stdout, "Starting loop over %s  ...\n", filename);

    glitch = model->glitch;

    glitch[0] = model->glitch[0];

    for(ifo=1; ifo<NI; ifo++)
    {
      sprintf(filename,"chains/%sglitch_params_%s.dat.0",data->runName,data->ifos[ifo]);
      glitchParams[ifo] = fopen(filename,"r");

      glitch[ifo] = model->glitch[ifo];
    }

    for(ifo=0; ifo<NI; ifo++)
      discard_burnin_samples(glitchParams[ifo], &COUNT, &BURNIN);

    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/glitch_recovered_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
      recWhitenedWaveformFile[ifo] = fopen(filename,"w");

      sprintf(filename,"%s/glitch_recovered_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
      recColoredWaveformFile[ifo] = fopen(filename,"w");

        
        sprintf(filename,"%s/glitch_median_time_domain_waveform_%s.dat",outdir,data->ifos[ifo]);
        medianWaveformFile[ifo] = fopen(filename,"w");
        
        sprintf(filename,"%s/glitch_median_frequency_domain_waveform_%s.dat",outdir,data->ifos[ifo]);
        medianFrequencyWaveformFile[ifo] = fopen(filename,"w");
        
        sprintf(filename,"%s/glitch_median_PSD_%s.dat",outdir,data->ifos[ifo]);
        medianPSDFile[ifo] = fopen(filename,"w");
        
        sprintf(filename,"%s/glitch_median_tf_%s.dat",outdir,data->ifos[ifo]);
        medianTimeFrequencyFile[ifo] = fopen(filename,"w");

        
        
    }

    setup_output_files(NI, injectionFlag, outdir, "glitch", "whitened", whitenedFile,data);
    setup_output_files(NI, injectionFlag, outdir, "glitch", "colored", coloredFile,data);

    for(ifo=0; ifo<NI; ifo++) for(n=0; n<data->Dmax; n++) dimension[ifo][n] = 0;

    //BayesLine
    if(data->bayesLineFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"chains/%sglitch_spline_%s.dat.0",data->runName,data->ifos[ifo]);
        splinechain[ifo] = fopen(filename,"r");

        sprintf(filename,"chains/%sglitch_lorentz_%s.dat.0",data->runName,data->ifos[ifo]);
        linechain[ifo] = fopen(filename,"r");

        discard_burnin_samples(splinechain[ifo], &COUNT, &BURNIN);
        discard_burnin_samples(linechain[ifo], &COUNT, &BURNIN);
      }
    }
      
      int Nwaveform_glitch = COUNT-BURNIN;
      printf("Nwave = %i\n",Nwaveform_glitch);
      
      double **hoft_median    = double_matrix(NI-1,N-1);
      double **hoff_median    = double_matrix(NI-1,N/2-1);
      double **psd_median     = double_matrix(NI-1,N/2-1);
      double **tftrack_median = double_matrix(NI-1,N-1);
      
      double ***waveform_mega = double_tensor(NI-1,Nwaveform_glitch-1,N-1);
      double ***psd_mega      = double_tensor(NI-1,Nwaveform_glitch-1,N/2-1);

       FILE **glitchResidualFile = malloc(NI*sizeof(FILE *));

    for(n=0; n<COUNT-BURNIN; n++)
    {

      /******************************************************************************/
      /*                                                                            */
      /*  Get current state of MCMC output into format for BayesWave code library   */
      /*                                                                            */
      /******************************************************************************/

      /*
       GLITCH MODEL
       */
      parse_glitch_parameters(data, model, glitchParams, grec);
      for(ifo=0; ifo<NI; ifo++) dimension[ifo][glitch[ifo]->size]++;

      /*
       PSD MODEL
       */
      if(data->bayesLineFlag)parse_bayesline_parameters(data, model, splinechain, linechain, psd);

      /******************************************************************************/
      /*                                                                            */
      /*  Compute various moments/overlaps to quantify waveform reconstruction      */
      /*                                                                            */
      /******************************************************************************/

      /*
       MATCH BETWEEN INJECTED AND RECOVERED WAVEFORM
       */

      if(injectionFlag)
      {
        ComputeWaveformOverlap(data, hinj, grec, psd, whitenedGlitchMoments);
        ComputeWaveformOverlap(data, hinj, grec, one, coloredGlitchMoments);
      }
      ComputeWaveformMoments(data, deltaF, deltaT, grec, psd, whitenedGlitchMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, grec, one, coloredGlitchMoments);

      for(ifo=0; ifo<NI; ifo++)
      {
        if(glitch[ifo]->size>0)
        {
          print_moments(ifo, injectionFlag, whitenedFile, whitenedGlitchMoments);
          print_moments(ifo, injectionFlag, coloredFile, coloredGlitchMoments);
        }
      }

      //print time-domain waveforms
      if(n%100 == 0)
      {
        print_waveforms(data, grec, psd, recWhitenedWaveformFile);
        print_waveforms(data, grec, one, recColoredWaveformFile);
          

      }
        
        
        // Store data needed to get median waveforms etc
        for(ifo = 0; ifo<NI; ifo++){
            for (ii = 0; ii < data->N/2; ii++) {
                waveform_mega[ifo][n][2*ii] = grec[ifo][2*ii];
                waveform_mega[ifo][n][2*ii+1] = grec[ifo][2*ii+1];
                
                psd_mega[ifo][n][ii] = psd[ifo][ii];
 
            }
        }

        

    } // End loop over chains

      
      double **gres = double_matrix(NI-1,N-1);
      
      random_draw = (double)(Nwaveform_glitch)*uniform_draw(chain->seed);
      printf("random_draw = %i\n",random_draw);
      
      // Calculate median
      for (ifo=0; ifo<NI; ifo++) {
          
          
          // Print fourier domain residuals from a random draw from the chain
          sprintf(filename,"%s/fourier_domain_glitch_residual_%s.dat",outdir,data->ifos[ifo]);
          glitchResidualFile[ifo] = fopen(filename,"w");
          
          
          // Pad with zeros upto imin
          
          for (ii = 0; ii < data->imin; ii++) {
              fprintf(glitchResidualFile[ifo],"%g %g %g\n", (double)ii*deltaF, 0.0, 0.0);
          }
          
          for (ii = data->imin; ii < data->N/2; ii++) {
              fprintf(glitchResidualFile[ifo],"%g %g %g\n", (double)ii*deltaF, hoft[ifo][2*ii] - waveform_mega[ifo][random_draw][2*ii], hoft[ifo][2*ii+1] - waveform_mega[ifo][random_draw][2*ii+1]);
          }
          fclose(glitchResidualFile[ifo]);
          
          
          // Calc and print median PSD
          median_and_CIs(psd_mega[ifo], N/2, Nwaveform_glitch, deltaF, medianPSDFile[ifo], psd_median[ifo]);
          

          // Use "psd" vector to store glitch power spectrum, then calc and pringt median
          for (n = 0; n < Nwaveform_glitch; n++) {
              for (ii = 0; ii < N/2; ii++) {
                  psd_mega[ifo][n][ii] = waveform_mega[ifo][n][2*ii]*waveform_mega[ifo][n][2*ii]+waveform_mega[ifo][n][2*ii+1]*waveform_mega[ifo][n][2*ii+1];
                  
              }
          }
          median_and_CIs(psd_mega[ifo], N/2, Nwaveform_glitch, deltaF, medianFrequencyWaveformFile[ifo], hoff_median[ifo]);
          
          
          
          // Get (whitened) time domain waveforms, calc a print median
          for (n = 0; n < Nwaveform_glitch; n++) {
              whiten_data(data->imin, data->imax, waveform_mega[ifo][n], psd[ifo]);
              get_time_domain_waveforms(waveform_mega[ifo][n], waveform_mega[ifo][n], data->N, data->imin, data->imax);
              norm = sqrt((double)data->N);
              for(ii = 0; ii< data->N; ii++){
                  waveform_mega[ifo][n][ii] /= norm;
              }
              
          }
          median_and_CIs(waveform_mega[ifo], N, Nwaveform_glitch, deltaT, medianWaveformFile[ifo], hoft_median[ifo]);

          
          // Use mega waveform vector to store tf tracks. Again, calc and print median
          for (n = 0; n < Nwaveform_glitch; n++) {
              tf_tracks(waveform_mega[ifo][n], waveform_mega[ifo][n], deltaT, N);
          }
          median_and_CIs(waveform_mega[ifo], N, Nwaveform_glitch, deltaT, medianTimeFrequencyFile[ifo], tftrack_median[ifo]);
          
          
          // Q transform spectrograms
          Q = 4.0;
          sprintf(filename,"%s/glitch_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hoft_median[ifo], Q, deltaT, deltaF, fres, one[ifo], spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 8.0;
          sprintf(filename,"%s/glitch_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hoft_median[ifo], Q, deltaT, deltaF, fres, one[ifo], spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 16.0;
          sprintf(filename,"%s/glitch_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, hoft_median[ifo], Q, deltaT, deltaF, fres, one[ifo], spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          
          
          //// residual spectrograms /////
          
          
          for (ii = 0; ii < data->N; ii++) {
              gres[ifo][ii] = hdata[ifo][ii] - hoft_median[ifo][ii];
          }
          
          
          Q = 4.0;
          sprintf(filename,"%s/glitch_residual_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, gres[ifo], Q, deltaT, deltaF, fres, one[ifo],spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 8.0;
          sprintf(filename,"%s/glitch_residual_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, gres[ifo], Q, deltaT, deltaF, fres, one[ifo], spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          Q = 16.0;
          sprintf(filename,"%s/glitch_residual_spectrogram_%g_%s.dat",outdir,Q,data->ifos[ifo]);
          spectrogramFile[ifo] = fopen(filename,"w");
          Q_scan(data, gres[ifo], Q, deltaT, deltaF, fres, one[ifo],spectrogramFile[ifo]);
          fclose(spectrogramFile[ifo]);
          
          
      }
      free_double_matrix(gres,NI-1);

      
      
    for(ifo=0; ifo<NI; ifo++)
    {
      if(data->bayesLineFlag)
      {
        fclose(splinechain[ifo]);
        fclose(linechain[ifo]);
      }

      fclose(recWhitenedWaveformFile[ifo]);
      fclose(recColoredWaveformFile[ifo]);
      //fclose(recWhitenedSpectrumFile[ifo]);
      fclose(glitchParams[ifo]);
      fclose(whitenedFile[ifo]);
      fclose(coloredFile[ifo]);
        
        fclose(medianWaveformFile[ifo]);
        fclose(medianFrequencyWaveformFile[ifo]);
        fclose(medianPSDFile[ifo]);
        fclose(medianTimeFrequencyFile[ifo]);

    }

      free(glitchResidualFile);
      
      free_double_tensor(waveform_mega,NI-1,Nwaveform_glitch-1);
      free_double_tensor(psd_mega,NI-1,Nwaveform_glitch-1);
      free_double_matrix(hoft_median,NI-1);
      free_double_matrix(hoff_median,NI-1);
      free_double_matrix(psd_median,NI-1);
      free_double_matrix(tftrack_median,NI-1);

      
    print_stats(data, GPStrig, injectionFlag, outdir, "glitch", dimension);

    for(ifo=0; ifo<NI; ifo++)
    {
      anderson_darling_test(outdir, ifo, data->fmin, data->Tobs, "glitch", data->ifos);
    }


  } //End post processing of glitch model





  /**********************************/
  /*                                */
  /*  Post process cleaning phase   */
  /*                                */
  /**********************************/

  FILE **cleanParams = malloc(NI*sizeof(FILE *));

  sprintf(filename,"chains/%sclean_params_%s.dat.0",data->runName, data->ifos[0]);
  if( (cleanParams[0] = fopen(filename,"r")) == NULL)
  {
    fprintf(stdout,"Could not find %s.  Finishing...\n",filename);
  }
  else
  {

    fprintf(stdout, "Starting loop over %s  ...\n", filename);

    glitch = model->glitch;

    glitch[0] = model->glitch[0];

    for(ifo=1; ifo<NI; ifo++)
    {
      sprintf(filename,"chains/%sclean_params_%s.dat.0",data->runName,data->ifos[ifo]);
      cleanParams[ifo] = fopen(filename,"r");

      glitch[ifo] = model->glitch[ifo];
    }

    for(ifo=0; ifo<NI; ifo++)
      discard_burnin_samples(cleanParams[ifo], &COUNT, &BURNIN);

    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/clean_recovered_whitened_waveform_%s.dat",outdir,data->ifos[ifo]);
      recWhitenedWaveformFile[ifo] = fopen(filename,"w");

      //sprintf(filename,"%s/clean_recovered_whitened_spectrum_%s.dat",outdir,ifo);
      //recWhitenedSpectrumFile[ifo] = fopen(filename,"w");

      sprintf(filename,"%s/clean_recovered_colored_waveform_%s.dat",outdir,data->ifos[ifo]);
      recColoredWaveformFile[ifo] = fopen(filename,"w");
    }

    setup_output_files(NI, injectionFlag, outdir, "clean", "whitened", whitenedFile,data);
    setup_output_files(NI, injectionFlag, outdir, "clean", "colored", coloredFile,data);

    for(ifo=0; ifo<NI; ifo++) for(n=0; n<data->Dmax; n++) dimension[ifo][n] = 0;

    //BayesLine
    if(data->bayesLineFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"chains/%sclean_spline_%s.dat.0",data->runName,data->ifos[ifo]);
        splinechain[ifo] = fopen(filename,"r");

        sprintf(filename,"chains/%sclean_lorentz_%s.dat.0",data->runName,data->ifos[ifo]);
        linechain[ifo] = fopen(filename,"r");

        discard_burnin_samples(splinechain[ifo], &COUNT, &BURNIN);
        discard_burnin_samples(linechain[ifo], &COUNT, &BURNIN);
      }
    }

    for(n=0; n<COUNT-BURNIN; n++)
    {

      /******************************************************************************/
      /*                                                                            */
      /*  Get current state of MCMC output into format for BayesWave code library   */
      /*                                                                            */
      /******************************************************************************/

      /*
       GLITCH MODEL
       */
      parse_glitch_parameters(data, model, cleanParams, grec);
      for(ifo=0; ifo<NI; ifo++) dimension[ifo][glitch[ifo]->size]++;

      /*
       PSD MODEL
       */
      if(data->bayesLineFlag)parse_bayesline_parameters(data, model, splinechain, linechain, psd);

      /******************************************************************************/
      /*                                                                            */
      /*  Compute various moments/overlaps to quantify waveform reconstruction      */
      /*                                                                            */
      /******************************************************************************/

      /*
       MATCH BETWEEN INJECTED AND RECOVERED WAVEFORM
       */

      if(injectionFlag)
      {
        ComputeWaveformOverlap(data, hinj, grec, psd, whitenedCleanMoments);
        ComputeWaveformOverlap(data, hinj, grec, one, coloredCleanMoments);
      }
      ComputeWaveformMoments(data, deltaF, deltaT, grec, psd, whitenedCleanMoments);
      ComputeWaveformMoments(data, deltaF, deltaT, grec, one, coloredCleanMoments);

      for(ifo=0; ifo<NI; ifo++)
      {
        if(glitch[ifo]->size>0)
        {
          print_moments(ifo, injectionFlag, whitenedFile, whitenedCleanMoments);
          print_moments(ifo, injectionFlag, coloredFile, coloredCleanMoments);
        }
      }

      //print time-domain waveforms geocenter?
      if(n%100 == 0)
      {
        print_waveforms(data, grec, psd, recWhitenedWaveformFile);
        print_waveforms(data, grec, one, recColoredWaveformFile);
      }
    }

    for(ifo=0; ifo<NI; ifo++)
    {
      if(data->bayesLineFlag)
      {
        fclose(splinechain[ifo]);
        fclose(linechain[ifo]);
      }

      fclose(recWhitenedWaveformFile[ifo]);
      fclose(recColoredWaveformFile[ifo]);
      //fclose(recWhitenedSpectrumFile[ifo]);
      fclose(cleanParams[ifo]);
      fclose(whitenedFile[ifo]);
      fclose(coloredFile[ifo]);
    }

    print_stats(data, GPStrig, injectionFlag, outdir, "clean", dimension);

//    for(ifo=0; ifo<NI; ifo++)
//    {
//      anderson_darling_test(outdir, ifo, data->fmin, data->Tobs, "clean", data->ifos);
//    }


  } //End post processing of cleaning phase
    
    
    /**********************************/
    /*                                */
    /*  Post process noise phase      */
    /*                                */
    /**********************************/
    
    
    sprintf(filename,"chains/%snoise_spline_%s.dat.0",data->runName, data->ifos[0]);
    if( fopen(filename,"r") == NULL)
    {
        fprintf(stdout,"Could not find %s.  Finishing...\n",filename);
    }
    else
    {
        
        printf("Starting noise model\n");
        
        for (ifo=0; ifo<NI; ifo++) {
            sprintf(filename,"%s/noise_median_PSD_%s.dat",outdir,data->ifos[ifo]);
            medianNoiseFile[ifo] = fopen(filename,"w");
        }
        
        
        
        //BayesLine
        if(data->bayesLineFlag)
        {
            for(ifo=0; ifo<NI; ifo++)
            {
                
                sprintf(filename,"chains/%snoise_spline_%s.dat.0",data->runName,data->ifos[ifo]);
                splinechain[ifo] = fopen(filename,"r");
                
                sprintf(filename,"chains/%snoise_lorentz_%s.dat.0",data->runName,data->ifos[ifo]);
                linechain[ifo] = fopen(filename,"r");
                
                
                discard_burnin_samples(splinechain[ifo], &COUNT, &BURNIN);
                discard_burnin_samples(linechain[ifo], &COUNT, &BURNIN);
                
            }
        }
        
        
        int N_sample_noise_model = COUNT-BURNIN;
        printf("N_sample_noise_model = %d\n", N_sample_noise_model);
        
        double ***noise_model = double_tensor(NI-1,N_sample_noise_model-1,N/2-1);
        double **noise_median = double_matrix(NI-1,N/2-1);

        for(n=0; n<COUNT-BURNIN; n++)
        {
            /*
             PSD MODEL
             */
            
            if(data->bayesLineFlag)parse_bayesline_parameters(data, model, splinechain, linechain, psd);
            
            
            for(ifo = 0; ifo<NI; ifo++){
                
                for (ii = 0; ii < data->N/2; ii++) {
                    noise_model[ifo][n][ii] = psd[ifo][ii];
                }
            }

            
        }

        
        for (ifo=0; ifo<NI; ifo++) {
            median_and_CIs(noise_model[ifo], N/2, N_sample_noise_model, deltaF, medianNoiseFile[ifo], noise_median[ifo]);
        }
        
        
        
        for (ifo=0; ifo<NI; ifo++) {
            if(data->bayesLineFlag)
            {
                fclose(splinechain[ifo]);
                fclose(linechain[ifo]);
            }
            
            fclose(medianNoiseFile[ifo]);
        }
        
        free_double_matrix(noise_median,NI-1);
        free_double_tensor(noise_model,NI-1,N_sample_noise_model-1);

      /* Missing files for cleaning phase anderson darling test
      for(ifo=0; ifo<NI; ifo++)
      {
        anderson_darling_test(outdir, ifo, data->fmin, data->Tobs, "noise", data->ifos);
      }
       */
        
    }//End post processing of noise phase
    
    
    
    

  /******************************************************************************/
  /*                                                                            */
  /*  Free memory, exit cleanly                                                 */
  /*                                                                            */
  /******************************************************************************/

    free_double_matrix(hdata,NI-1);
    
    
  free(recWhitenedWaveformFile);
  free(recColoredWaveformFile);
  //free(recWhitenedSpectrumFile);
  free(whitenedFile);
  free(coloredFile);
    
    //free(tfWhitenedFile);
    free(medianWaveformFile);
    free(medianFrequencyWaveformFile);
    free(medianPSDFile);
    free(medianTimeFrequencyFile);
    free(spectrogramFile);
    

  free(splinechain);
  free(linechain);

  if(data->nukeFlag)
  {
    fprintf(stdout, " ======= rm'ing chains/ and checkpoint/ ======\n");
    char command[128];
    sprintf(command,"rm -rvf chains checkpoint");
    system(command);
  }

  fprintf(stdout, " ======= DONE! ======\n");

  return 0;

}


void print_help_message(void)
{
  fprintf(stdout,"\n ======== BayesWavePost USAGE: =======\n\n");
  fprintf(stdout,"REQUIRED:\n");
  fprintf(stdout,"  --ifo IFO              interferometer (H1,L1,V1)\n");
  fprintf(stdout,"  --IFO-flow             minimum frequency (Hz)\n");
  fprintf(stdout,"  --IFO-cache LALAdLIGO  lie about the PSD\n");
  fprintf(stdout,"  --trigtime             GPS trigger time\n");
  fprintf(stdout,"  --srate                sampling rate (Hz)\n");
  fprintf(stdout,"  --seglen               duration of data (s)\n");
  fprintf(stdout,"  --PSDstart             GPS start time for PSD estimation\n");
  fprintf(stdout,"  --PSDlength            duration of PSD estimation length\n");
  fprintf(stdout,"  --dataseed 1234        Lie about random seed for data\n");
  fprintf(stdout,"  --0noise               no noise realization\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"OPTIONAL:\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- Run parameters   -------------------------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --runName              run name for output files\n");
  fprintf(stdout,"  --nuke                 delete chains/ and checkpoint/ when done [NOT RECOMMENDED]\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- Model parameters   -----------------------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --bayesLine            use BayesLine for PSD model\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- LALInference injection  options  ---------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --inj injfile.xml      Injection XML file to use\n");
  fprintf(stdout,"  --event N              Event number from Injection XML file to use\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- Burst MDC injection  options  ------------------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --MDC-channel          IFO1-chan, IFO2-chan, etc\n");
  fprintf(stdout,"  --MDC-cache            IFO1-mdcframe, IFO2-mdcframe, etc\n");
  fprintf(stdout,"  --MDC-prefactor        Rescale injection amplitude (1.0)\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --- BayesWave internal injection options  ----------------------------------------\n");
  fprintf(stdout,"  ----------------------------------------------------------------------------------\n");
  fprintf(stdout,"  --BW-inject            (signal/glitch)\n");
  fprintf(stdout,"  --BW-injName           runName that produced the chain file \n");
  fprintf(stdout,"  --BW-path              Path to BW chain file for injection (./chains) \n");
  fprintf(stdout,"  --BW-event             Which sample from BayesWave chain (200000)\n");
  fprintf(stdout,"\n");
  fprintf(stdout,"EXAMPLE:\n");
  fprintf(stdout,"./bayeswave_post --ifo H1 --H1-flow 32 --H1-cache LALSimAdLIGO --H1-channel LALSimAdLIGO --trigtime 900000000.00 --srate 512 --seglen 4 --PSDstart 900000000 --PSDlength 1024 --dataseed 1234 --0noise\n");
  fprintf(stdout,"\n");
}



void ComputeWaveformOverlap(struct Data *data, double **hinj, double **hrec, double **psd, struct Moments *m)
{
  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;

  int ifo,n;

  double **invpsd = malloc(NI*sizeof(double *));
  for(ifo=0; ifo<NI; ifo++) invpsd[ifo] = malloc(N/2*sizeof(double));

  //get input waveform into right format (either colored or whitened)
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N/2; n++)
    {
      invpsd[ifo][n] = 1/psd[ifo][n];
    }
  }

  double hihi; //Detector (hinj|hinj)
  double hrhr; //Detector (hrec|hrec)
  double hrhi; //Detector (hrec|hinj)
  double ii=0; //Network (hinj|hinj)
  double rr=0; //Network (hrec|hrec)
  double ri=0; //Network (hrec|hinj)

  for(ifo=0; ifo<NI; ifo++)
  {
    hihi = fourier_nwip(imin, N/2, hinj[ifo], hinj[ifo], invpsd[ifo]);
    hrhr = fourier_nwip(imin, N/2, hrec[ifo], hrec[ifo], invpsd[ifo]);
    hrhi = fourier_nwip(imin, N/2, hrec[ifo], hinj[ifo], invpsd[ifo]);

    ii += hihi;
    rr += hrhr;
    ri += hrhi;

    //Store inj-rec overlap of each detector to file
    m->overlap[ifo] =  hrhi/sqrt(hihi*hrhr); // (hrec|hinj)/sqrt((hrec|hrec)(hinj|hinj))
  }

  //Store inj-rec networ overlap to file
  m->networkOverlap = ri/sqrt(ii*rr); // (hrec|hinj)/sqrt((hrec|hrec)(hinj|hinj))

  for(ifo=0; ifo<NI; ifo++) free(invpsd[ifo]);
  free(invpsd);

}

void PrintRhof(struct Data *data, double deltaF, double **h, double **psd, struct Moments *m){
  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;
  int i;

  int ifo,n;
  double *rho = malloc(N*sizeof(double));
  double **wf = malloc(NI*sizeof(double *));

  double **invPSD = malloc(NI*sizeof(double *));

  for(ifo=0; ifo<NI; ifo++)
  {
    wf[ifo]=malloc(N*sizeof(double));

    invPSD[ifo] = malloc(N/2*sizeof(double));
  }

  //get input waveform into right format (either colored or whitened)
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N/2; n++)
    {
      invPSD[ifo][n] = 1/psd[ifo][n];
      wf[ifo][2*n]   = h[ifo][2*n]/sqrt(psd[ifo][n]);
      wf[ifo][2*n+1] = h[ifo][2*n+1]/sqrt(psd[ifo][n]);
    }
  }

  for(ifo=0; ifo<=NI; ifo++)
  {
    /*
     RECOVERED SIGNAL ENERGY
     */
    if(ifo<NI) m->energy[ifo] = get_rhof(imin, imax, wf[ifo], deltaF, rho);
    else m->energy[ifo] = get_rhof_net(imin, imax, wf, deltaF, rho, NI);

    // *********** print rho **************** //


    if(ifo==0){ // do only for H1
      printf("\n\n\n");
      for (i=0; i<imax; i+=10) {
        printf("%g %g\n",(double)i*deltaF,rho[i]);
      }
      printf("\n\n\n");
    }

  }

  for(ifo=0; ifo<NI; ifo++)
  {
    free(wf[ifo]);

    free(invPSD[ifo]);
  }
  free(rho);
  free(wf);

  free(invPSD);

}


void ComputeWaveformMoments(struct Data *data, double deltaF, double deltaT, double **h, double **psd, struct Moments *m)
{
  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;

  int ifo,n;
  double *rho = malloc(N*sizeof(double));
  double **wt = malloc(NI*sizeof(double *));
  double **wf = malloc(NI*sizeof(double *));

  double **invPSD = malloc(NI*sizeof(double *));

  for(ifo=0; ifo<NI; ifo++)
  {
    wt[ifo]=malloc(N*sizeof(double));
    wf[ifo]=malloc(N*sizeof(double));

    invPSD[ifo] = malloc(N/2*sizeof(double));
  }

  //get input waveform into right format (either colored or whitened)
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N/2; n++)
    {
      invPSD[ifo][n] = 1/psd[ifo][n];
      wf[ifo][2*n]   = h[ifo][2*n]/sqrt(psd[ifo][n]);
      wf[ifo][2*n+1] = h[ifo][2*n+1]/sqrt(psd[ifo][n]);
        
    }

    //transform waveform into time-domain
    get_time_domain_waveforms(wt[ifo], wf[ifo], N, imin, imax);
  }

  //SNR
  for(ifo=0; ifo<NI; ifo++) m->snr[ifo] = detector_snr(imin, imax, h[ifo], invPSD[ifo], 1.0);

  /******************************/
  /*                            */
  /*  Frequency domain moments  */
  /*                            */
  /******************************/

  for(ifo=0; ifo<=NI; ifo++)
  {
    /*
     RECOVERED SIGNAL ENERGY
     */
    if(ifo<NI)
      m->energy[ifo] = get_rhof(imin, imax, wf[ifo], deltaF, rho);
    else
      m->energy[ifo] = get_rhof_net(imin, imax, wf, deltaF, rho, NI);

      
      
      
    /*
     RECOVERED CENTRAL FREQUENCY
     */
    m->frequency[ifo] = get_f0(imax, rho, deltaF);


    /*
     RECOVERED BANDWIDTH
     */
    m->bandwidth[ifo] = get_band(imax, rho, deltaF, m->frequency[ifo]);

    /* Median f and 90% interval bw */
    //get_f_quantiles(imin, imax, rho, deltaF, &fmedtemp, &bwtemp);

    //m->medianfrequency[ifo] = fmedtemp;
    //m->bandwidth90percentinterval[ifo] = bwtemp;


  }



  /******************************/
  /*                            */
  /*     Time domain moments    */
  /*                            */
  /******************************/

  for(ifo=0; ifo<=NI; ifo++)
  {
    /*
     RECOVERED SIGNAL ENERGY
     */
    if(ifo<NI)
      m->energy[ifo] = get_rhot(N, wt[ifo], deltaT, rho);
    else
      m->energy[ifo] = get_rhot_net(N, wt, deltaT, rho, NI);
      
      

    /*
     RECOVERED CENTRAL TIME
     */
    m->time[ifo] = get_f0(N, rho, deltaT);

    /*
     RECOVERED WAVEFORM DURATION
     */
    m->duration[ifo] = get_band(N, rho, deltaT, m->time[ifo]);

    /*
     Median t and quantiles
     */

    //get_t_quantiles(N, rho, deltaT, &tmedtemp, &durtemp);
    //m->mediantime[ifo] = tmedtemp;
    //m->duration90percentinterval[ifo] = durtemp;



    /*
     WAVEFORM MAXIMUM AND TIME AT MAXIMUM
     */
    if(ifo<NI)
    {
      get_hmax_and_t(N, wt[ifo], deltaT, &m->hmax[ifo], &m->t_at_hmax[ifo]);
    }

  }


  for(ifo=0; ifo<NI; ifo++)
  {
    free(wf[ifo]);
    free(wt[ifo]);

    free(invPSD[ifo]);
  }
  free(rho);
  free(wt);
  free(wf);

  free(invPSD);

}

void ComputeGeocenterMoments(struct Data *data, double deltaF, double deltaT, double *h, double *psd, struct Moments *m)
{
  int N = data->N;
  int imin = data->imin;
  int imax = data->imax;
  double tmedtemp, durtemp, fmedtemp, bwtemp;

  int n;
  double *rho = malloc(N*sizeof(double));
  double *rhop = malloc(N*sizeof(double));
  double *wt  = malloc(N*sizeof(double));
  double *wf  = malloc(N*sizeof(double));
  double *hdot  = malloc(N*sizeof(double));

  double *invPSD = malloc(N/2*sizeof(double));


  //get input waveform into right format (either colored or whitened)
  for(n=0; n<N/2; n++)
  {
    wf[2*n]   = h[2*n]/sqrt(psd[n]);
    wf[2*n+1] = h[2*n+1]/sqrt(psd[n]);

    invPSD[n] = 1./psd[n];

  }

  //transform waveform into time-domain
  get_time_domain_waveforms(wt, wf, N, imin, imax);

  //SNR
  m->snr[0] = detector_snr(imin, imax, h, invPSD, 1.0);

  /******************************/
  /*                            */
  /*  Frequency domain moments  */
  /*                            */
  /******************************/

  /*
   RECOVERED SIGNAL ENERGY
   */
  m->energy[0] = get_rhof(imin, imax, wf, deltaF, rho);




  /*     New energy weighted rho    */
  //temp = get_rhof_f2weighted(imin, imax, wf, deltaF, rhop);

  /*
   RECOVERED CENTRAL FREQUENCY
   */
  m->frequency[0] = get_f0(imax, rhop, deltaF);


  /*
   RECOVERED BANDWIDTH
   */
  m->bandwidth[0] = get_band(imax, rhop, deltaF, m->frequency[0]);


  /* Median f and bw by quantiles*/

  get_f_quantiles(imin, imax, rhop, deltaF, &fmedtemp, &bwtemp);

  m->medianfrequency[0] = fmedtemp;
  m->bandwidth90percentinterval[0] = bwtemp;


  /******************************/
  /*                            */
  /*     Time domain moments    */
  /*                            */
  /******************************/


  /*
   RECOVERED SIGNAL ENERGY
   */
  m->energy[0] = get_rhot(N, wt, deltaT, rho);



  /*
   WAVEFORM MAXIMUM AND TIME AT MAXIMUM
   */
  get_hmax_and_t(N, wt, deltaT, &m->hmax[0], &m->t_at_hmax[0]);


  /* Get energy weighted rho (use hdot) */

  get_time_domain_hdot(hdot, wf, N, imin, imax, deltaF);

  get_rhot(N, hdot, deltaT, rho);

  /*
   RECOVERED CENTRAL TIME
   */
  m->time[0] = get_f0(N, rho, deltaT);

  /*
   RECOVERED WAVEFORM DURATION
   */
  m->duration[0] = get_band(N, rho, deltaT, m->time[0]);


  /* Median t and duration by quantiles */


  get_t_quantiles(N, rho, deltaT, &tmedtemp, &durtemp);
  m->mediantime[0] = tmedtemp;
  m->duration90percentinterval[0] = durtemp;


  free(invPSD);
  free(rho);
  free(wt);
  free(wf);

}


void initialize_moments(struct Moments *m, int NI)
{
  m->overlap   = malloc((NI+1)*sizeof(double));
  m->energy    = malloc((NI+1)*sizeof(double));
  m->frequency = malloc((NI+1)*sizeof(double));
  m->time      = malloc((NI+1)*sizeof(double));
  m->bandwidth = malloc((NI+1)*sizeof(double));
  m->duration  = malloc((NI+1)*sizeof(double));
  m->snr       = malloc((NI+1)*sizeof(double));
  m->hmax      = malloc((NI+1)*sizeof(double));
  m->t_at_hmax = malloc((NI+1)*sizeof(double));
  m->medianfrequency = malloc((NI+1)*sizeof(double));
  m->bandwidth90percentinterval = malloc((NI+1)*sizeof(double));
  m->mediantime = malloc((NI+1)*sizeof(double));
  m->duration90percentinterval = malloc((NI+1)*sizeof(double));
  m->tl = malloc((NI+1)*sizeof(double));
  m->th = malloc((NI+1)*sizeof(double));
}


void setup_output_files(int NI, int injectionFlag, char *outdir, char *model, char *weighting, FILE **momentsOut, struct Data *data)
{
  int ifo;
  char filename[100];

  //Open files
  if(NI<0)
  {
    sprintf(filename,"%s/%s_%s_moments.dat.geo", outdir, model, weighting);
    momentsOut[0] = fopen(filename,"w");

    //Write headers
    fprintf(momentsOut[0], "# ");
    fprintf(momentsOut[0], "snr ");
    fprintf(momentsOut[0], "t_energy_rec ");
    fprintf(momentsOut[0], "hrss ");
    fprintf(momentsOut[0], "t0_rec ");
    fprintf(momentsOut[0], "dur_rec ");
    fprintf(momentsOut[0], "f0_rec ");
    fprintf(momentsOut[0], "band_rec ");
//    fprintf(momentsOut[0], "median_t0 ");
//    fprintf(momentsOut[0], "dur_quantile ");
//    fprintf(momentsOut[0], "median_f0 ");
//    fprintf(momentsOut[0], "band_quantile ");
    if(injectionFlag)
    {
      fprintf(momentsOut[0], "overlap ");
      fprintf(momentsOut[0], "network_overlap ");
    }
    fprintf(momentsOut[0], "h_max ");
    fprintf(momentsOut[0], "t_at_h_max ");
    fprintf(momentsOut[0], "\n");
  }

  else
  {
    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"%s/%s_%s_moments_%s.dat", outdir, model, weighting, data->ifos[ifo]);
      momentsOut[ifo] = fopen(filename,"w");
    }
    sprintf(filename,"%s/%s_%s_moments.dat.net", outdir, model, weighting);
    momentsOut[NI] = fopen(filename,"w");

    //Write headers
    for(ifo=0; ifo<=NI; ifo++)
    {
      fprintf(momentsOut[ifo], "# ");
      fprintf(momentsOut[ifo], "snr ");
      fprintf(momentsOut[ifo], "t_energy_rec ");
      fprintf(momentsOut[ifo], "hrss ");
      fprintf(momentsOut[ifo], "t0_rec ");
      fprintf(momentsOut[ifo], "dur_rec ");
      fprintf(momentsOut[ifo], "f0_rec ");
      fprintf(momentsOut[ifo], "band_rec ");
//      fprintf(momentsOut[ifo], "median_t0 ");
//      fprintf(momentsOut[ifo], "dur_quantile ");
//      fprintf(momentsOut[ifo], "median_f0 ");
//      fprintf(momentsOut[ifo], "band_quantile ");
      if(injectionFlag)
      {
        fprintf(momentsOut[ifo], "overlap ");
        fprintf(momentsOut[ifo], "network_overlap ");
      }
      fprintf(momentsOut[ifo], "h_max ");
      fprintf(momentsOut[ifo], "t_at_h_max ");
      fprintf(momentsOut[ifo], "\n");
    }
  }
}


void discard_burnin_samples(FILE *params, int *COUNT, int *BURNIN)
{
  int n;
  char burnrows[100000];
  (*COUNT) = 0;
  while(!feof(params))
  {
    fgets(burnrows,100000,params);
    (*COUNT)++;
  }
  (*COUNT)--;
  (*BURNIN) = (*COUNT)/2;
  rewind(params);

  for(n=0; n<(*BURNIN); n++) fgets(burnrows,100000,params);
}

void print_moments(int ifo, int injectionFlag, FILE **outfile, struct Moments *m)
{
  fprintf(outfile[ifo], "%e ",m->snr[ifo]);
  fprintf(outfile[ifo], "%e ",m->energy[ifo]);
  fprintf(outfile[ifo], "%e ",log10(sqrt(m->energy[ifo]/2.)));
  fprintf(outfile[ifo], "%e ",m->time[ifo]);
  fprintf(outfile[ifo], "%e ",m->duration[ifo]);
  fprintf(outfile[ifo], "%e ",m->frequency[ifo]);
  fprintf(outfile[ifo], "%e ",m->bandwidth[ifo]);
  // New moments
//  fprintf(outfile[ifo], "%e ",m->mediantime[ifo]);
//  fprintf(outfile[ifo], "%e ",m->duration90percentinterval[ifo]);
//  fprintf(outfile[ifo], "%e ",m->medianfrequency[ifo]);
//  fprintf(outfile[ifo], "%e ",m->bandwidth90percentinterval[ifo]);
  // end new moments
  if(injectionFlag)
  {
    fprintf(outfile[ifo], "%e ",m->overlap[ifo]);
    //if(injectionFlag)fprintf(outfile[ifo], "%e\n",m->overlap[ifo]);
    fprintf(outfile[ifo], "%e ",m->networkOverlap);
  }
  fprintf(outfile[ifo], "%e ",m->hmax[ifo]);
  fprintf(outfile[ifo], "%e ",m->t_at_hmax[ifo]);
  fprintf(outfile[ifo], "\n");
}

void print_waveforms(struct Data *data, double **h, double **psd, FILE **ofile)
{
  int i;
  int ifo;

  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;

  double *hoft = malloc(N*sizeof(double));
  double *w    = malloc(N*sizeof(double));

  double norm = sqrt((double)N);

  for(ifo=0; ifo<NI; ifo++)
  {
    //whiten waveforms
    for(i=0; i<N/2; i++)
    {
      w[2*i]   =   h[ifo][2*i] / sqrt(psd[ifo][i]);
      w[2*i+1] = h[ifo][2*i+1] / sqrt(psd[ifo][i]);
    }

    get_time_domain_waveforms(hoft, w, N, imin, imax);

    for(i=0; i<N; i++)
    {
      hoft[i]/=norm;
    }

    for(i=0; i<N; i++)
      fprintf(ofile[ifo], "%e ", hoft[i]);
    //fprintf(ofile[ifo], "%e\n", hoft[i]);

    fprintf(ofile[ifo], "\n");
  }

  free(hoft);
  free(w);
}

void tf_tracks(double *hoft, double *frequency_out, double deltaT, int N){
    int i, ii, jj;
    
    double slope, intercept, zero, freq;
    
    double *w    = malloc(N*sizeof(double));
    double **tf_raw = malloc(2*sizeof(double *));
    double **tf_track = malloc(2*sizeof(double *));
    double *zerotimes = malloc(N*sizeof(double));
    
    for (i = 0; i < 2; i++) {
        tf_raw[i] = malloc(N*sizeof(double));
        tf_track[i] = malloc(N*sizeof(double));
    }
    
    for (ii = 0; ii < N; ii++) {
        tf_track[0][ii] = (double)ii*deltaT;
    }
    
    jj = 0;
  
    // Get f of t from zero crossings
    for (ii = 0; ii <= 2; ii++) {
        zerotimes[ii] = 0.0;
    }
    
    
    for (ii = 0; ii < N-1; ii++) {
        // find where it crosses
        if (hoft[ii]*hoft[ii+1] < 0.0) {
            
            slope = (hoft[ii+1] - hoft[ii])/deltaT; // rise over run
            intercept = hoft[ii] - slope*(double)ii*deltaT; // y = mx + b;
            zero = -intercept/slope; // gives t value of zero;
            
            
            // update crossing points (ie "current" "previous" and "next")
            zerotimes[2] = zerotimes[1];
            zerotimes[1] = zerotimes[0];
            zerotimes[0] = zero;
            
            // Find the frequency at each zero crossing
            if (jj > 3) {
                freq = (1./(zerotimes[0]-zerotimes[2]));
                //printf("%g %g\n",hoft[ii],hoft[ii+1]);
                //printf("%g %g %g\n",zerotimes[0],zerotimes[2],freq);
                tf_raw[0][jj] = zerotimes[1];
                
                //printf("%g\n",tf[1][jj]);
                tf_raw[1][jj] = freq;
                
                
            }
            else{
                tf_raw[0][jj] = zerotimes[1];
                tf_raw[1][jj] = 0.0;
            }
            jj++; // this gives the number of tf points we have
        }
    }
    
    
    
    // Interpolate
    interp_tf(tf_track, tf_raw, jj, N);
    
    for (ii = 0; ii < N; ii++) {
        //fprintf(ofile[ifo], "%e ", tf_track[1][ii]);
        frequency_out[ii] = tf_track[1][ii];
    }
    
    
    for (ii=0; ii<2; ii++) {
        free(tf_raw[ii]);
        free(tf_track[ii]);
    
    }
    
    free(w);
    free(zerotimes);
    free(tf_raw);
    free(tf_track);
    
    
}


void interp_tf(double **tf, double **pts, int tlength, int Nsample){
    int ii, jj;
    double slope, intercept;
    
    
    ii = 2;
    for (jj = 0; jj < Nsample; jj++) {
        
        while ((pts[0][ii] < tf[0][jj] || fabs(pts[0][ii-1]-pts[0][ii]) < 1e-7 /* sometimes points too close together gets fucked up*/) && ii < tlength) {
            ii++; // this just figures out which two points we're interpolating between
        }
        
        slope = (pts[1][ii]-pts[1][ii-1])/(pts[0][ii]-pts[0][ii-1]);
        intercept = pts[1][ii] - slope*pts[0][ii]; // y = mx + b;
        tf[1][jj] = slope*tf[0][jj]+intercept; // assign frequency
        
        
        
    }
    
    
}


void median_and_CIs(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform){
    
    int ii, n;
    double median, upper50, lower50, upper90, lower90;
    

    double *dataslice;
    dataslice = double_vector(Nwaveforms-1);
    
    
    for (ii = 0; ii < N; ii++) {
        
        for (n = 0; n < Nwaveforms; n++) {
            dataslice[n] = waveforminfo[n][ii];
        }
        
        gsl_sort(dataslice, 1, Nwaveforms);
        
        median = gsl_stats_median_from_sorted_data (dataslice,
                                                    1, Nwaveforms);
        upper90 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                      1, Nwaveforms,
                                                      0.05);
        lower90 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                      1, Nwaveforms,
                                                      0.95);
        upper50 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                       1, Nwaveforms,
                                                       0.25);
        lower50 = gsl_stats_quantile_from_sorted_data (dataslice,
                                                       1, Nwaveforms,
                                                       0.75);
        
        median_waveform[ii] = median;
        
        
        fprintf(outfile,"%g %g %g %g %g %g\n", ii*deltaT, median, lower50, upper50, lower90, upper90);
    }
 
    
    free_double_vector(dataslice);
    
    //fclose(outfile);
    
}

void median_and_CIs_fourierdom(double **waveforminfo, int N, int Nwaveforms, double deltaT, FILE *outfile, double *median_waveform){
    
    int ii, n;
    double median, upper50, lower50, upper90, lower90;
    double medianre, medianim, upper50re, upper50im, lower50re, lower50im, upper90re, upper90im, lower90re, lower90im;
    
    
    double *dataslicere, *datasliceim;
    dataslicere = double_vector(Nwaveforms-1);
    datasliceim = double_vector(Nwaveforms-1);
    
    
    for (ii = 0; ii < N; ii++) {
        
        for (n = 0; n < Nwaveforms; n++) {
            dataslicere[n] = waveforminfo[n][2*ii];
            datasliceim[n] = waveforminfo[n][2*ii+1];
        }
        
        gsl_sort(dataslicere, 1, Nwaveforms);
        gsl_sort(datasliceim, 1, Nwaveforms);
        
        medianre = gsl_stats_median_from_sorted_data (dataslicere,
                                                    1, Nwaveforms);
        medianim = gsl_stats_median_from_sorted_data (datasliceim,
                                                    1, Nwaveforms);
        median = medianre*medianre+medianim*medianim;
        
        upper90re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.05);
        upper90im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.05);
        upper90 = upper90re*upper90re+upper90im*upper90im;
        
        lower90re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.95);
        lower90im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.95);
        lower90 = lower90re*lower90re+lower90im*lower90im;
        
        upper50re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.25);
        upper50im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.25);
        upper50 = upper50re*upper50re+upper50im*upper50im;
        
        lower50re = gsl_stats_quantile_from_sorted_data (dataslicere,
                                                       1, Nwaveforms,
                                                       0.75);
        lower50im = gsl_stats_quantile_from_sorted_data (datasliceim,
                                                       1, Nwaveforms,
                                                       0.75);
        lower50 = lower50re*lower50re+lower50im*lower50im;
        
        median_waveform[ii] = median;
        
        
        fprintf(outfile,"%g %g %g %g %g %g\n", ii*deltaT, median, lower50, upper50, lower90, upper90);
        
//        fprintf(outfile,"%g %g %g %g %g %g %g %g %g %g %g\n", ii*deltaT, medianre, medianim, lower50re, lower50im, upper50re, upper50im, lower90re, lower90im, upper90re, upper90im);
        
    }
    
    
    free_double_vector(dataslicere);
    free_double_vector(datasliceim);
    
    //fclose(outfile);
    
}

void Q_scan(struct Data *data, double *median_waveform, double Q, double deltaT, double deltaF, double df_resolution, double *invPSD, FILE *outfile){
    int ii, M, N, imax, imin;
    
    double fmax, fmin;
    double *hoff;
    
    
    imin = data->imin;
    imax = data->imax;
    fmin = (double)imin*deltaF;
    fmax = (double)imax*deltaF;
    
    
    M = (int)((fmax-fmin)/df_resolution);
    N = data->N;
    
    //tfmap = double_matrix(M-1,N-1);
    hoff = double_vector(N-1);
    
    for(ii = 0; ii < N; ii++){
        hoff[ii] = median_waveform[ii];
    }
    
    
    Transform(hoff, Q, deltaT, df_resolution, fmin, invPSD,N, M, outfile);
    
    free_double_vector(hoff);
    
}


void Transform(double *hoff, double Q, double dt, double df_resolution, double fmin, double *invPSD, int n, int m, FILE *outfile)
{
  int i, j;
  double f;
  double *AC, *AF;
  double *corr, *b;
  double *params;
  double bmag;
  double Tobs;
  double fix;
  
  Tobs = (double)n*dt;
  
  fix = sqrt(Tobs)/((double)n);
  
  // [0] t0 [1] f0 [2] Q [3] Amp [4] phi
  
  params = double_vector(4);
  
  params[0] = 0.0;
  params[2] = Q;
  params[3] = 1.0;
  params[4] = 0.0;
  
  double *timeData = (double *)malloc(sizeof(double)*n);
  fftw_complex *freqData = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*n);
  fftw_plan forward = fftw_plan_dft_r2c_1d(n, timeData, freqData, FFTW_MEASURE);
  for(i=0; i<n; i++) timeData[i] = hoff[i];
  
  fftw_execute(forward);
  
  for(i=0; i<n/2; i++)
  {
    hoff[2*i]   = freqData[i][0];
    hoff[2*i+1] = freqData[i][1];
  }
  //get DC and Nyquist where FFTW wants them
  hoff[1] = freqData[n/2][0];

  
  free(timeData);
  fftw_destroy_plan(forward);
  fftw_free(freqData);

  for (i = 0; i < n; i++) {
    hoff[i] *= fix;
  }
  
  AC   = double_vector(n-1);
  AF   = double_vector(n-1);
  corr = double_vector(n-1);
  b    = double_vector(n-1);
  
  
  for(j = 0; j < m; j++)
  {
    
    f = fmin + df_resolution*(double)(j);
        
    params[1] = f;
    
    SineGaussianFourier(b, params, n, 0, Tobs);
    
    bmag = sqrt(fourier_nwip(0,n/2, b, b, invPSD));
    
    for(i = 0; i < n; i++) corr[i] = 0.0;
    
    phase_blind_time_shift(AC, AF, hoff, b, invPSD, n);
    
    for(i = 0; i < n; i++) corr[i] += sqrt(AC[i]*AC[i]+AF[i]*AF[i])/bmag;
    
    for(i = 0; i < n; i++)
    {
      //tfmap[j][i] = corr[i];
      fprintf(outfile,"%e ",corr[i]);
    }
    
    fprintf(outfile,"\n");
    
    
  }
  
  free_double_vector(AC);
  free_double_vector(AF);
  free_double_vector(corr);
  free_double_vector(b);
  
}

void print_spectrum(struct Data *data, double **h, FILE **ofile)
{
  int i;
  int ifo;
  int re,im;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;
  double f;

  for(ifo=0; ifo<NI; ifo++)
  {
    for(i=imin; i<imax; i++)
    {
      f=(double)i/data->Tobs;
      re = 2*i;
      im = re+1;
      fprintf(ofile[ifo], "%lg %e ", f,h[ifo][re]*h[ifo][re]+h[ifo][im]*h[ifo][im]);
      fprintf(ofile[ifo], "\n");
    }
  }
}

void print_hdot(struct Data *data, double **h, double **psd, FILE **ofile, double deltaF)
{
  int i;
  int ifo;

  int N = data->N;
  int NI = data->NI;
  int imin = data->imin;
  int imax = data->imax;

  double *hoft = malloc(N*sizeof(double));
  double *w    = malloc(N*sizeof(double));

  for(ifo=0; ifo<NI; ifo++)
  {
    //whiten waveforms
    for(i=0; i<N/2; i++)
    {
      w[2*i]   =   h[ifo][2*i] / sqrt(psd[ifo][i]);
      w[2*i+1] = h[ifo][2*i+1] / sqrt(psd[ifo][i]);
    }

    get_time_domain_hdot(hoft, w, N, imin, imax, deltaF);

    for(i=0; i<N; i++)
      fprintf(ofile[ifo], "%e ", hoft[i]);
    //fprintf(ofile[ifo], "%e\n", hoft[i]);

    fprintf(ofile[ifo], "\n");
  }

  free(hoft);
  free(w);
}

void print_single_waveform(struct Data *data, double **h, double **psd, FILE *ofile)
{
  int i;
  int ifo;

  int N = data->N;
  int imin = data->imin;
  int imax = data->imax;

  double *hoft = malloc(N*sizeof(double));
  double *w    = malloc(N*sizeof(double));

  double maxh = 0.0;
  double maxt = 0.0;
  double samp = data->Tobs/(double)data->N;

  ifo=0;
  //whiten waveforms
  for(i=0; i<N/2; i++)
  {
    w[2*i]   =   h[ifo][2*i] / sqrt(psd[ifo][i]);
    w[2*i+1] = h[ifo][2*i+1] / sqrt(psd[ifo][i]);
  }

  //Obtain waveform in the time domain
  get_time_domain_waveforms(hoft, w, N, imin, imax);

  for(i=0; i<N; i++) fprintf(ofile, "%e\n", hoft[i]);

  for(i=0; i<N; i++)
  {
    if(sqrt(hoft[i]*hoft[i])>maxh )
    {
      maxh = sqrt(hoft[i]*hoft[i]);
      maxt = (double)i*samp;
    }
  }

  printf("max waveform %g at %g s \n",maxh,maxt);

  free(hoft);
  free(w);
}

void print_stats(struct Data *data, LIGOTimeGPS GPS, int injectionFlag, char *outdir, char *model, int **dimension)
{
  int i;
  int ifo;
  int NI = data->NI;

  int map,max;
  double bayesfactor;
  int Zs,Zn;

  FILE *momentsfile = NULL;
  FILE *statsfile   = NULL;

  char filename[100];
  char filerow[10000];
  int count;

  double *snr       = NULL;
  double *time      = NULL;
  double *duration  = NULL;
  double *frequency = NULL;
  double *bandwidth = NULL;
  double *hmax      = NULL;
  double *t_at_hmax = NULL;

  int seconds = GPS.gpsSeconds;
  int nanoseconds = GPS.gpsNanoSeconds;
  double GPStime = (double)seconds + 1.0e-9*nanoseconds;

  if(!strcmp(model,"signal")) NI++;

  for(ifo=0; ifo<NI; ifo++)
  {

    //find model with most posterior support

    map=max=-1;
    bayesfactor=0.0;
    Zn = 0;
    Zs = 0;

    if(!strcmp(model,"signal"))
    {
      for(i=0; i<data->Dmax; i++)
      {
        //evidence for each model
        if(i==0) Zn += dimension[0][i];
        else     Zs += dimension[0][i];

        //map model
        if(dimension[0][i]>max)
        {
          max = dimension[0][i];
          map = i;
        }
      }
      if(ifo==0)printf("map dimension for channel \"%s\" = %i\n",model,map);
    }
    else
    {
      for(i=0; i<data->Dmax; i++)
      {
        //evidence for each model
        if(i==0) Zn += dimension[ifo][i];
        else     Zs += dimension[ifo][i];

        //map model
        if(dimension[ifo][i]>max)
        {
          max = dimension[ifo][i];
          map = i;
        }
      }
      printf("map dimension for channel \"%s[%i]\" = %i\n",model,ifo,map);
    }

    if(Zn==0) bayesfactor = 1000.0;
    else      bayesfactor = (double)Zs/(double)Zn;


    if( !strcmp(model,"signal") && ifo==(NI-1) )
    {
      sprintf(filename,"%s/%s_whitened_moments.dat.geo",outdir,model);
      momentsfile = fopen(filename,"r");

      sprintf(filename,"%s/%s_stats.dat.geo",outdir,model);
      statsfile = fopen(filename,"w");
    }
    else
    {
      sprintf(filename,"%s/%s_whitened_moments_%s.dat",outdir,model,data->ifos[ifo]);
      momentsfile = fopen(filename,"r");

      sprintf(filename,"%s/%s_stats_%s.dat",outdir,model,data->ifos[ifo]);
      statsfile = fopen(filename,"w");
    }

    // print header for stats file
    fprintf(statsfile, "# ");
    fprintf(statsfile, "map_D ");
    fprintf(statsfile, "bayesfactor ");
    fprintf(statsfile, "snr ");
    fprintf(statsfile, "time ");
    fprintf(statsfile, "duration ");
    fprintf(statsfile, "frequency ");
    fprintf(statsfile, "bandwidth ");
    fprintf(statsfile, "h_max ");
    fprintf(statsfile, "t_at_h_max ");
    fprintf(statsfile, "\n");

    count=0;

    //ignore header
    fgets(filerow,10000,momentsfile);

    //get size of file
    while(!feof(momentsfile))
    {
      fgets(filerow,10000,momentsfile);
      count++;
    }
    rewind(momentsfile);
    count--;

    //allocate memory, store burst table moments
    double junk;
    snr       = malloc(count*sizeof(double));
    time      = malloc(count*sizeof(double));
    duration  = malloc(count*sizeof(double));
    frequency = malloc(count*sizeof(double));
    bandwidth = malloc(count*sizeof(double));
    hmax = malloc(count*sizeof(double));
    t_at_hmax = malloc(count*sizeof(double));


    //ignore header again
    fgets(filerow,10000,momentsfile);

    for(i=0; i<count; i++)
    {
      fscanf(momentsfile, "%lg",&snr[i]);
      fscanf(momentsfile, "%lg",&junk);
      fscanf(momentsfile, "%lg",&junk); //FP: bug? There are two values between snr and t0_rec but only one was going to &junk
      fscanf(momentsfile, "%lg",&time[i]);
      fscanf(momentsfile, "%lg",&duration[i]);
      fscanf(momentsfile, "%lg",&frequency[i]);
      fscanf(momentsfile, "%lg",&bandwidth[i]);
      if(injectionFlag)
      {
        fscanf(momentsfile, "%lg",&junk);
        fscanf(momentsfile, "%lg",&junk);
      }
      fscanf(momentsfile, "%lg",&hmax[i]);
      fscanf(momentsfile, "%lg",&t_at_hmax[i]);
    }

    //sort each moment array
    gsl_sort(snr,1,count);
    gsl_sort(time,1,count);
    gsl_sort(duration,1,count);
    gsl_sort(frequency,1,count);
    gsl_sort(bandwidth,1,count);
    gsl_sort(hmax,1,count);
    gsl_sort(t_at_hmax,1,count);

    //get and print medians for stats file
    fprintf(statsfile, "%i ",    map );
    fprintf(statsfile, "%g ",    bayesfactor);
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (snr,1,count) );
    fprintf(statsfile, "%.16g ", gsl_stats_median_from_sorted_data (time,1,count)  + (GPStime + 2.0 - data->Tobs) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (duration,1,count) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (frequency,1,count) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (bandwidth,1,count) );
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (hmax,1,count));
    fprintf(statsfile, "%g ",    gsl_stats_median_from_sorted_data (t_at_hmax,1,count) + (GPStime + 2.0 - data->Tobs));
    fprintf(statsfile, "\n");

    fclose(momentsfile);
    fclose(statsfile);

    free(snr);
    free(time);
    free(duration);
    free(frequency);
    free(bandwidth);

  }
}


double get_energy(int imin, int imax, double *a, double deltaF)
{
  int i;
  double result;

  // Calculate total signal energy
  result = 0.0;
  for(i=imin; i<imax; i++){
    result += (a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1]);
  }
  result *= 2*deltaF;
  return(result);
}

double get_energy_f2weighted(int imin, int imax, double *a, double deltaF)
{
  int i;
  double result;

  // Calculate total signal energy
  result = 0.0;
  for(i=imin; i<imax; i++){
    result += (a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1])*i*i;
  }
  result *= 2*deltaF*deltaF*deltaF;
  return(result);
}

double get_rhof(int imin, int imax, double *a, double deltaF, double *arg)
{
  // Calculate rho_f (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)

  int i;
  double norm;

  // We'll normalize by the total signal energy
  norm = get_energy(imin, imax, a, deltaF);
  for(i=0; i<imax; i++) {
    if (i<imin) {
      arg[i] = 0;
    } else {
      arg[i] = 2*(a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1])/norm;
    }
  }
  // Return value is the total signal energy
  return(norm);
}

double get_rhof_f2weighted(int imin, int imax, double *a, double deltaF, double *arg)
{
  // Calculate rho_f (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)

  int i;
  double norm;

  // We'll normalize by the total signal energy
  norm = get_energy_f2weighted(imin, imax, a, deltaF);
  for(i=0; i<imax; i++) {
    if (i<imin) {
      arg[i] = 0;
    } else {
      arg[i] = 2*(a[i*2]*a[i*2] + a[i*2+1]*a[i*2+1])*i*deltaF*i*deltaF/norm;
    }
  }
  // Return value is the total signal energy
  return(norm);
}

double get_rhof_net(int imin, int imax, double **a, double deltaF, double *arg, int NI)
{
  // Calculate rho_f (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)

  int i,ifo;
  double norm;

  // We'll normalize by the total signal energy
  norm=0.0;
  for(ifo=0; ifo<NI; ifo++) norm += get_energy(imin, imax, a[ifo], deltaF);

  for(i=0; i<imax; i++)
  {
    arg[i]=0.0;
    if(i>=imin) for(ifo=0; ifo<NI; ifo++) arg[i] += 2*(a[ifo][i*2]*a[ifo][i*2] + a[ifo][i*2+1]*a[ifo][i*2+1])*i*deltaF*i*deltaF/norm;
  }

  // Return value is the total signal energy
  return(norm);
}

double get_rhot(int N, double *a, double deltaT, double *arg)
{
  // Calculate rho_t (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)
  int i;
  double norm;

  // We'll normalize by the total signal energy
  norm = 0;
  for(i=0;i<N; i++) {
    norm += a[i]*a[i];
  }
  norm *= deltaT;

  for(i=0; i<N; i++) {
    arg[i] = a[i]*a[i]/norm;
  }
  // Return value is the total signal energy
  return(norm);
}

double get_rhot_net(int N, double **a, double deltaT, double *arg, int NI)
{
  // Calculate rho_t (Normalized signal energy frequency distribution.  See moments.pdf eqn 1.5)
  int i,ifo;
  double norm;

  // We'll normalize by the total signal energy
  norm = 0;
  for(ifo=0; ifo<NI; ifo++)
  {
    for(i=0;i<N; i++) {
      norm += a[ifo][i]*a[ifo][i];
    }
  }
  norm *= deltaT;

  for(i=0; i<N; i++) {
    arg[i]=0.0;
    for(ifo=0; ifo<NI; ifo++) arg[i] += a[ifo][i]*a[ifo][i]/norm;
  }
  // Return value is the total signal energy
  return(norm);
}


double rhonorm(int imax, double *a, double deltaF)
{
  // Calculate normalization.  This is a sanity check, since rho is supposed to have a normalization of 1.
  int i;
  double norm;

  norm = 0.0;
  for(i=0; i<imax; i++) {
    norm += (a[i]);
  }
  norm *= deltaF;
  return(norm);
}




void get_f_quantiles(int imin, int imax, double *rhof, double deltaF, double *fm, double *bw){
  double f, flow=imin*deltaF, fhigh=imax*deltaF, fmed=0.5*(flow+fhigh);
  double x;
  double quantile_range = .68;

  int j, flag1, flag2, flag3;

  flag1 = 0;
  flag2 = 0;
  flag3 = 0;

  x = 0.0;

  for(j=0;j<imax;j++){
    f = (double)(j)*deltaF;
    x += rhof[j]*deltaF;

    if(x > 0.5*(1.0-quantile_range) && flag1==0){
      flag1 = 1; // low end of quantile
      flow = f;
    }

    if (x > 0.5 && flag2==0) {
      flag2 = 1; // median
      fmed = f;
    }

    if(x > 0.5*(1.0+quantile_range) && flag3==0){
      flag3 = 1; // high end of quantile
      fhigh = f;
    }

  }


  *fm = fmed;

  *bw = fhigh-flow; // 90% bandwidth


}


void get_t_quantiles(int N, double *rhot, double deltaT, double *tm, double *dur){
  double tlow=0.0, thigh=N*deltaT, tmed=0.5*(tlow+thigh), t;
  double x;
  double quantile_range = .68;


  int flag1, flag2, flag3, j;

  flag1 = 0;
  flag2 = 0;
  flag3 = 0;

  x = 0.0;

  for(j=0;j<N;j++){
    t = (double)(j)*deltaT;
    x += rhot[j]*deltaT;

    if(x > 0.5*(1.0-quantile_range) && flag1==0){
      flag1 = 1;
      tlow = t; // low end of quantile
    }

    if (x > 0.5 && flag2==0) {
      flag2 = 1;
      tmed = t; // median
    }

    if(x > 0.5*(1.0+quantile_range) && flag3==0){
      flag3 = 1;
      thigh = t; // high end of quantile
    }


  }

  *tm = tmed;

  *dur = thigh-tlow; // 90% duration

}

double get_f0(int imax, double *a, double deltaF)
{
  // Calculate the central frequency (aka first moment).
  int i;
  double result;

  result = 0.0;
  for(i=0; i<imax; i++) {
    result += (a[i]*i);
  }
  result *= deltaF*deltaF;
  return(result);
}

double get_band(int imax, double *a, double deltaF, double f0)
{
  // Calculate the bandwidth (aka 2nd moment).
  int i;
  double result;

  result = 0.0;
  for(i=0; i<imax; i++) {
    result += ( a[i]*(i*deltaF - f0)*(i*deltaF - f0) );
  }
  result *= deltaF;
  result = sqrt(result);
  return(result);
}

void get_hmax_and_t(int imax, double *a, double deltaT, double *hmax, double *t_at_hmax)
{
  //Find the maximum value of the vector and the time there
  int i;
  double a_abs;

  *hmax = 0.0;
  *t_at_hmax = 0.0;

  for(i=0; i<imax; i++)
  {
    a_abs = sqrt(a[i]*a[i]);
    if(a_abs>*hmax)
    {
      *hmax = a_abs;
      *t_at_hmax = (double)i*deltaT;
    }
  }
}

void whiten_data(int imin, int imax, double *a, double *Snf)
{
  int i;
  for(i=imin; i<imax; i++) {
    a[i*2] = a[i*2]/sqrt(Snf[i]);
    a[i*2+1] = a[i*2+1]/sqrt(Snf[i]);
  }
}

void get_time_domain_waveforms(double *hoft, double *h, int N, int imin, int imax)
{
  int i;
  
  fftw_complex *hoff = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, hoff, hoft, FFTW_MEASURE);
  
  hoff[0][0] = 0.0;
  hoff[0][1] = 0.0;
  for(i=1; i<N/2; i++)
  {
    if(i>imin && i<imax)
    {
      hoff[i][0] = h[2*i];
      hoff[i][1] = h[2*i+1];
    }
    else
    {
      hoff[i][0] = 0.0;
      hoff[i][1] = 0.0;
    }
  }
  
  //get DCand Nyquist where FFTW wants them
  hoff[N/2][0] = hoff[0][1];
  hoff[N/2][1] = hoff[0][1] = 0.0;

  fftw_execute(reverse);
  
  fftw_destroy_plan(reverse);
  fftw_free(hoff);
}

void get_time_domain_hdot(double *hdot, double *h, int N, int imin, int imax, double deltaF)
{
  int i;
  double xx, yy;

  fftw_complex *hoff = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
  fftw_plan reverse = fftw_plan_dft_c2r_1d(N, hoff, hdot, FFTW_MEASURE);

  for(i=0; i<N/2; i++)
  {
    hoff[0][i] = -LAL_TWOPI*h[2*i]; // Take care of the -2pi part
    hoff[1][i] = -LAL_TWOPI*h[2*i+1];
  }

  hoff[0][0] = 0.0;
  hoff[0][1] = 0.0;
  for(i=1; i< N/2; i++)
  {
    if(i>imin && i<imax)
    {

      xx = hoff[i][0]; // Real part
      yy = hoff[i][1]; // Imaginary part

      // switch real and imaginary parts i*(x+iy) = ix-y

      hoff[i][0] =  yy*(double)(i)*deltaF;  // f = i*deltaf
      hoff[i][1] = -xx*(double)(i)*deltaF;  // Change sign of new imaginary part so it doesn't come out time reversed
    }
    else
    {
      hoff[i][0] = 0.0;
      hoff[i][1] = 0.0;
    }
  }
  
  //get DCand Nyquist where FFTW wants them
  hoff[N/2][0] = hoff[0][1];
  hoff[N/2][1] = hoff[0][1] = 0.0;

  fftw_execute(reverse);

  fftw_destroy_plan(reverse);
  fftw_free(hoff);

}

void write_time_freq_samples(struct Data *data)
{

  int N    = data->N;
  int imax = data->imax;

  double Tobs   = data->Tobs;
  double deltaF = 1./data->Tobs;

  char filename[100];
  sprintf(filename,"%spost/timesamp.dat",data->runName);
  FILE *timesamp = fopen(filename, "w");
  sprintf(filename,"%spost/freqsamp.dat",data->runName);
  FILE *freqsamp = fopen(filename, "w");
  double t0 = Tobs-2.0;
  double f;
  double t;
  int i;
  for(i=0; i<N; i++) {
    t = (i*Tobs)/N;
    fprintf(timesamp, "%e \n", t-t0);
  }
  fclose(timesamp);
  for (i=0;i<imax;i++) {
    f = i*deltaF;
    fprintf(freqsamp, "%e \n", f);
  }
  fclose(freqsamp);
}

void anderson_darling_test(char *outdir, int ifo, double fmin, double Tobs, char *model, char **ifos)
{
  char filename[1024];

  FILE *dFile;
  FILE *nFile;

  int N,n,nn;
  double f,dre,dim,Sn,Sn25,Sn75,Sn05,Sn95;

  int Nsamp;
  double *samples=NULL;

  // how many samples in the data?
  N=0;
  Nsamp=0;

  //get residual file
  sprintf(filename,"%s/fourier_domain_%s_residual_%s.dat",outdir,model,ifos[ifo]);
  dFile = fopen(filename,"r");

  while(!feof(dFile))
  {
    fscanf(dFile,"%lg %lg %lg",&f,&dre,&dim);
    N++;
    if(f>fmin) Nsamp++;
  }
  N--;
  fclose(dFile);

  // get memory to ingest file
  Nsamp *= 2; //re and im parts
  samples=malloc(Nsamp*sizeof(double));
  nn=0;


  // combine Fourier coefficints
  //get residual file
  sprintf(filename,"%s/fourier_domain_%s_residual_%s.dat",outdir,model,ifos[ifo]);
  dFile = fopen(filename,"r");

  //get PSD file
  sprintf(filename,"%s/%s_median_PSD_%s.dat",outdir,model,ifos[ifo]);
  nFile = fopen(filename,"r");

  //whiten residual
  for(n=0; n<N; n++)
  {
    fscanf(dFile,"%lg %lg %lg",&f,&dre,&dim);
    fscanf(nFile,"%lg %lg %lg %lg %lg %lg",&f,&Sn,&Sn25,&Sn75,&Sn05,&Sn95);
    if(f>fmin)
    {
      samples[2*nn]   = dre/sqrt(Sn/(Tobs/2.));
      samples[2*nn+1] = dim/sqrt(Sn/(Tobs/2.));
      nn++;
    }
  }
  fclose(dFile);
  fclose(nFile);

  gsl_sort(samples,1,Nsamp);


  double S,A;

  // Anderson-Darling statistics
  S=0.0;
  for(n=0; n<Nsamp; n++)
  {
    S += ( log(gsl_cdf_ugaussian_P(samples[n])) + log(1 - gsl_cdf_ugaussian_P(samples[Nsamp-1-n])) )*(2.*(double)(n+1)-1.0)/(double)(Nsamp);
  }
  A = (double)(-1*Nsamp) - S;

  free(samples);

  sprintf(filename,"%s/%s_anderson_darling_%s.dat",outdir,model,ifos[ifo]);
  FILE *afile = fopen(filename,"w");
  fprintf(afile,"%lg\n",A);
  fclose(afile);

}
