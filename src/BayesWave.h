#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/* ********************************************************************************** */
/*                                                                                    */
/*                                  LAL Dependencies                                  */
/*                                                                                    */
/* ********************************************************************************** */

#include <lal/LALInspiral.h>
#include <lal/LALCache.h>
#include <lal/LALFrStream.h>
#include <lal/TimeFreqFFT.h>
#include <lal/LALDetectors.h>
#include <lal/ResampleTimeSeries.h>
#include <lal/TimeSeries.h>
#include <lal/FrequencySeries.h>
#include <lal/Units.h>
#include <lal/Date.h>
#include <lal/StringInput.h>
#include <lal/VectorOps.h>
#include <lal/Random.h>
#include <lal/LALNoiseModels.h>
#include <lal/XLALError.h>
#include <lal/GenerateInspiral.h>
#include <lal/LIGOLwXMLRead.h>
#include <lal/LIGOLwXMLInspiralRead.h>
#include <lal/LALConstants.h>
#include <lal/SeqFactories.h>
#include <lal/DetectorSite.h>
#include <lal/GenerateInspiral.h>
#include <lal/GeneratePPNInspiral.h>
#include <lal/SimulateCoherentGW.h>
#include <lal/Inject.h>
#include <lal/LIGOMetadataTables.h>
#include <lal/LIGOMetadataUtils.h>
#include <lal/LIGOMetadataInspiralUtils.h>
#include <lal/LIGOMetadataRingdownUtils.h>
#include <lal/LALInspiralBank.h>
#include <lal/FindChirp.h>
#include <lal/LALInspiralBank.h>
#include <lal/GenerateInspiral.h>
#include <lal/NRWaveInject.h>
#include <lal/GenerateInspRing.h>
#include <lal/LALError.h>

#include <lal/LALInference.h>
#include <lal/LALInferenceReadData.h>

/* ********************************************************************************** */
/*                                                                                    */
/*                            Globally defined parameters                             */
/*                                                                                    */
/* ********************************************************************************** */

#define RTPI 1.772453850905516       // sqrt(Pi)
#define RT2PI 2.5066282746310005024  // sqrt(2 Pi)
#define CLIGHT 2.99792458e8          // Speed of light (m/s)

#define NE 6 //Number of extrinsic parameters

//Tuning parameters for wavelet proximity proposal
//TODO: These should not be global!

#define UNIFORM 0.2    // fraction of proximity prior that is uniform
#define SPRD 4         // width of outer part of proximity proposal
#define HLW 1          // width of hollowed-out region

/* ********************************************************************************** */
/*                                                                                    */
/*                                  Data structures                                   */
/*                                                                                    */
/* ********************************************************************************** */
struct Wavelet
{
   int size;
   int smax;
   int *index;
   int dimension;

   //clustering prior
   int Ncluster;
   double cosy;
   double logp;

   double *templates;
   double **intParams;

   char *name;
};

struct Model
{
  int size;
  int Npol;

  double *Sn;
  double **Snf;
  double **invSnf;
  double **SnS;
  double *SnGeo;

  double logL;
  double logLnorm;
  double *detLogL;
  double *extParams;
  double **h;
  double **response;
    
    int chirpletFlag;
    int NW;

  struct Wavelet **signal;
  struct Wavelet **glitch;

  struct Network *projection;

  struct FisherMatrix *fisher;
  struct FisherMatrix *intfisher;

  struct Background *background;

  void (*wavelet_bandwidth)(double *, double *, double *);
  void (*wavelet)(double *, double *, int, int, double);

};

struct Network
{
  double **expPhase;
  double *deltaT;
  double *Fplus;
  double *Fcross;
  double *dtimes;
};

struct Chain
{
  int mc;
  int NC;     // Number of parallel chains
  int burn;   // Number of burn in samples
  int mod0;   // Counter keeping track of noise model
  int mod1;   // Counter keeping track of glitch model
  int mod2;   // Counter keeping track of signal model
  int mod3;   // Counter keeping track of glitch+signal model

  int cycle;  // # of model updates per BurstRJMCMC iteration
  int count;  // # of BurstRJMCMC iterations
  int blcount;// # of BayesLine burnin iterations
  int zcount; // # of logL samples for thermodynamic integration
  int mcount; // # of MCMC attempts
  int scount; // # of RJMCMC attempts
  int xcount; // # of extrinsic attempts
  int fcount; // # of intrinsic fisher attempts
  int pcount; // # of intrinsic phase attempts
  int ccount; // # of cluster proposal attempts
  int dcount; // # of density proposal attempts
  int ucount; // # of uniform proposal attempts
  int macc;   // # of MCMC successess
  int sacc;   // # of RJMCMC successes
  int xacc;   // # of extrinsic successes
  int facc;   // # of intrinsic fisher successes
  int pacc;   // # of intrinsic phase successes
  int cacc;   // # of cluster proposal successes
  int dacc;   // # of cluster proposal successes
  int uacc;   // # of uniform proposal successes

  int burnFlag;// flag telling proposals if we are in burn-in phase

  int *index; // keep track of which chain is at which temperature
  int *ptacc;
  int *ptprop;

  double *A;

  gsl_rng *seed;

  double modelRate;   //fraction of updates to signal model
  double rjmcmcRate;  //fraction of trans- vs. inter-dimensional moves
  double intPropRate; //fraction of prior vs. fisher intrinsic moves

  double *dT;
  double beta;
  double tempMin;
  double tempStep;
  double *temperature;
  double *avgLogLikelihood;
  double *varLogLikelihood;

  int nPoints;// = 3*chain->count/4/chain->cycle;
  double **logLchain;//=dmatrix(0,chain->NC-1,0,nPoints-1);


  FILE *runFile;
  FILE **intChainFile;
  FILE ***intWaveChainFile;
  FILE ***intGlitchChainFile;
  FILE ***lineChainFile;
  FILE ***splineChainFile;

};

struct Prior
{
  int smin;
  int smax;
  int *gmin;
  int *gmax;
  int omax;

  double Snmin;
  double Snmax;

  double TFV;
  double logTFV;

  double *bias;
  double **range;

  //background prior
    char bkg_name[1024];
    double bkg_df;
    double bkg_dQ;
    int bkg_nf;
    int bkg_nQ;
    double **bkg_dist;

  //cluster prior parameters
  double alpha;
  double beta;
  double gamma;

  //amplitude prior parameters
  double sSNRpeak;

  //glitch prior parameters
  double gSNRpeak;

  //dimension prior parameters
  double Dtau;
    double *Nwavelet;

  char path[100];

};

struct Data
{
  int N;
  int NI;
  int Npol;
  int tsize;
  int fsize;
  int imin;
  int imax;
  int Dmax;
  int Dmin;

  int runPhase;
  int psdFitFlag;
  int noiseSimFlag;
  int bayesLineFlag;
  int stochasticFlag;

  int fullModelFlag;
  int noiseModelFlag;
  int glitchModelFlag;
  int signalModelFlag;
  int cleanModelFlag;
  int cleanOnlyFlag;

  int cleanFlag;
  int glitchFlag;
  int signalFlag;
  int noiseFlag;

  int polarizationFlag;
  int fixIntrinsicFlag;
  int fixExtrinsicFlag;
  int geocenterPSDFlag;
  int clusterPriorFlag;
  int waveletPriorFlag;
  int backgroundPriorFlag;
  int amplitudePriorFlag;
  int amplitudeProposalFlag;
  int signalAmplitudePriorFlag;
  int extrinsicAmplitudeFlag;
  int orientationProposalFlag;
  int clusterProposalFlag;
  int logClusterProposalFlag;
  int adaptTemperatureFlag;
  int splineEvidenceFlag;
  int constantLogLFlag;
  int loudGlitchFlag;
  int gnuplotFlag;
  int verboseFlag;
  int checkpointFlag;
  int resumeFlag;
  int searchFlag;
  int rjFlag;

  int nukeFlag; //tell BWP to remove chains/ and checkpoint/ directories

    int srate;
    
    int chirpletFlag;
    int NW;
    
  double dt;
  double df;
  double fmin;
  double fmax;
  double Tobs;
  double Twin;
  double Qmin;
  double Qmax;
  double gmst;
  double logLc;
  double TwoDeltaToverN;

  double TFtoProx;

  double **r;
  double **s;
  double **rh;
  double *h;
  double *ORF;

  double logZglitch;
  double logZnoise;
  double logZsignal;
  double logZfull;

  double varZglitch;
  double varZnoise;
  double varZsignal;
  double varZfull;


  void (*signal_amplitude_proposal)(double *, double *, double, gsl_rng *, double, double **, double);
  void (*glitch_amplitude_proposal)(double *, double *, double, gsl_rng *, double, double **, double);

  double (*signal_amplitude_prior)   (double *, double *, double, double);
  double (*glitch_amplitude_prior)   (double *, double *, double, double);

  double (*intrinsic_likelihood)(int, int, int, struct Model*, struct Model*, struct Prior*, struct Chain*, struct Data*);
  double (*extrinsic_likelihood)(struct Network*, double *, double **, double *, struct Wavelet **, double **, struct Data*, double, double);

  char runName[100];
  char outputDirectory[1000];
  char **channels;
  char **ifos;

  ProcessParamsTable *commandLine;
  LALDetector **detector;
  LIGOTimeGPS epoch;
};

struct TimeFrequencyMap
{
  int N;
  int nQ;
  int nt;
  int nf;
  double ****pdf;
  double ****snr;
  double **max;

};

struct FisherMatrix
{
  int N;
   double *sigma;
   double *evalue;
   double **evector;
   double **matrix;
   double *aamps;
   double **count;
};

struct Background
{
  double logamp;
  double index;
  double fref;
  double invfref;
  double ***Cij;    //actually stores the inverse noise covariance matrix
  double *detCij;   //frequency-dependent determenant of noise covariance matrix
  double *spectrum;
};

