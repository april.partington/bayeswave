#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif


/* ********************************************************************************** */
/*                                                                                    */
/*                        Data handling and injection routines                        */
/*                                                                                    */
/* ********************************************************************************** */

REAL8TimeSeries *readTseries(CHAR *cachefile, CHAR *channel, LIGOTimeGPS start, REAL8 length);

void InjectFromMDC(ProcessParamsTable *commandLine, LALInferenceIFOData *IFOdata, double *SNR);
void BayesWaveInjection(ProcessParamsTable *commandLine, struct Data *data, struct Chain *chain, struct Prior *prior, double **psd, int *NC);
void getChannels(ProcessParamsTable *commandLine, char **channel);

/* ********************************************************************************** */
/*                                                                                    */
/*                                   Output Files                                     */
/*                                                                                    */
/* ********************************************************************************** */

void write_gnuplot_script_header(FILE *script, char modelname[]);
void write_gnuplot_script_frame (FILE *script, char modelname[], int signal, int glitch, int cycle, int NI);
void write_bayesline_gnuplot_script_frame(FILE *script, char modelname[], int cycle);
void write_bayeswave_gnuplot_script_frame(FILE *script, char modelname[], double Tobs, double fmin, double fmax, int phase, int signal, int glitch, int cycle, int NI);
void write_evidence_gnuplot_script(FILE *script, char modelname[]);
void print_bayesline_spectrum(char filename[], double *f, double *power, double *Sbase, double *Sline, int N);

void print_version(FILE *fptr);
void print_run_stats(FILE *fptr, struct Data *data, struct Chain *chain);
void print_run_flags(FILE *fptr, struct Data *data, struct Prior *prior);
void print_chain_files(struct Data *data, struct Chain *chain, struct Model **model, struct BayesLineParams ***bayesline, int ic);
void print_chain_status(struct Data *data, struct Chain *chain, struct Model **model, int searchFlag);
void flush_chain_files(struct Data *data, struct Chain *chain, int ic);

void print_model(FILE *fptr, struct Data *data, struct Chain *chain, struct Model *model);
void print_signal_model(FILE *fptr, struct Model *model, int n);
void print_glitch_model(FILE *fptr, struct Wavelet *glitch);

void print_time_domain_waveforms(char filename[], double *h, int N, double *Snf, double eta, double Tobs, int imin, int imax, double tmin, double tmax);
void print_time_domain_hdot(char filename[], double *h, int N, double *Snf, double eta, double Tobs, int imin, int imax, double tmin, double tmax);
void print_frequency_domain_waveforms(char filename[], double *h, int N, double *Snf, double Tobs, double eta, int imin, int imax);
void print_frequency_domain_data(char filename[], double *h, int N, double Tobs, int imin, int imax);
void print_colored_time_domain_waveforms(char filename[], double *h, int N, double Tobs, int imin, int imax, double tmin, double tmax);

void parse_command_line(struct Data *data, struct Chain *chain, struct Prior *prior, ProcessParamsTable *commandLine);
void parse_glitch_parameters(struct Data *data, struct Model *model, FILE **paramsfile, double **grec);
void parse_signal_parameters(struct Data *data, struct Model *model, FILE **paramsfile, double **hrec, double **hrecPlus, double **hrecCross);
void parse_bayesline_parameters(struct Data *data, struct Model *model, FILE **splinechain, FILE **linechain, double **psd);
void dump_time_domain_waveform(struct Data *data, struct Model *model, struct Prior *prior, char root[]);

void export_cleaned_data(struct Data *data, struct Model *model);

void system_pause();

