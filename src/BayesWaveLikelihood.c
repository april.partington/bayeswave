#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "BayesWave.h"
#include "BayesLine.h"
#include "BayesWaveIO.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveLikelihood.h"

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* ********************************************************************************** */
/*                                                                                    */
/*                             Likelihood Functions                                   */
/*                                                                                    */
/* ********************************************************************************** */


void phase_blind_time_shift(double *corr, double *corrf, double *data1, double *data2, double *invpsd, int n)
{
  int nb2, i, l, k;
  
  nb2 = n / 2;
  
  fftw_complex *freqData_corr  = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*(n));
  fftw_complex *freqData_corrf = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*(n));

  double *timeData_corr = (double*) fftw_malloc(sizeof(double)* n);
  double *timeData_corrf = (double*) fftw_malloc(sizeof(double)* n);

  for (i=0; i < n; i++)
  {
    corr[i]  = 0.0;
    corrf[i] = 0.0;
  }
  
  freqData_corr[0][0]  = 0.0;
  freqData_corr[0][1]  = 0.0;
  freqData_corrf[0][0] = 0.0;
  freqData_corrf[0][1] = 0.0;
  freqData_corr[nb2][0]  = 0.0;
  freqData_corr[nb2][1]  = 0.0;
  freqData_corrf[nb2][0] = 0.0;
  freqData_corrf[nb2][1] = 0.0;

  for (i=1; i < nb2; i++)
  {
    l=2*i;
    k=l+1;
    
    freqData_corr[i][0]	 = ( data1[l]*data2[l] + data1[k]*data2[k]) * invpsd[i];
    freqData_corr[i][1]	 = ( data1[k]*data2[l] - data1[l]*data2[k]) * invpsd[i];
    freqData_corrf[i][0] = ( data1[l]*data2[k] - data1[k]*data2[l]) * invpsd[i];
    freqData_corrf[i][1] = ( data1[k]*data2[k] + data1[l]*data2[l]) * invpsd[i];
  }
  
  //get DC and Nyquist where FFTW wants them
  freqData_corr[nb2][0]  = freqData_corr[0][1];
  freqData_corr[nb2][1]  = freqData_corr[0][1] = 0.0;
  freqData_corrf[nb2][0] = freqData_corrf[0][1];
  freqData_corrf[nb2][1] = freqData_corrf[0][1] = 0.0;

  
  fftw_plan reverse_corr = fftw_plan_dft_c2r_1d(n, freqData_corr, timeData_corr, FFTW_MEASURE);
  fftw_plan reverse_corrf = fftw_plan_dft_c2r_1d(n, freqData_corrf, timeData_corrf, FFTW_MEASURE);

  fftw_execute(reverse_corr);
  fftw_execute(reverse_corrf);
  
  for (i=0; i < n; i++)
  {
    corr[i]  = timeData_corr[i];
    corrf[i] = timeData_corrf[i];
  }

  fftw_destroy_plan(reverse_corr);
  fftw_destroy_plan(reverse_corrf);
  
  fftw_free(freqData_corr);
  fftw_free(freqData_corrf);
  
}

double loglike(int imin, int imax, double *r, double Sn, double *invSnf)
{
   /*
    logL = -(r|r)/2 - Nlog(Sn)
    */
   return -0.5*(fourier_nwip(imin, imax, r, r, invSnf)/Sn) - (double)(imax-imin)*log(Sn);
}

double loglike_stochastic(int NI, int imin, int imax, double **r, double ***invCij, double *detC)
{

  int i,j;
  double logL = 0.0;

  for(i=0; i<NI; i++)
  {
    for(j=0; j<NI; j++)
    {
      logL += -0.5*(fourier_nwip(imin, imax, r[i], r[j], invCij[i][j]));
    }
  }
  for(i=imin; i<imax; i++)
  {
    //logL -= log(detC[i]); // this should have a square-root, no?
    logL -= log(detC[i]);
  }
  return logL;
}

void recompute_residual(struct Data *data, struct Model **model, struct Chain *chain)
{
  int i,j;
  int ic,ip;
  int ifo;
  int NP = model[0]->Npol;
  int NI = data->NI;
  int N  = data->N;
  int NC = chain->NC;
  struct Model *model_x = NULL;

  for(ic=0; ic<NC; ic++)
  {
    model_x = model[chain->index[ic]];

    // re-do residual calculation occasionally to counter any round-off error from delta updates
    for(i=0; i< N; i++)
    {
      for(ip=0; ip<NP; ip++)    model_x->signal[ip]->templates[i] = 0.0;
      for(ifo=0; ifo<NI; ifo++) model_x->glitch[ifo]->templates[i] = 0.0;
    }

    if(data->signalFlag)
    {
      for(j=1; j<=model_x->signal[0]->size; j++)
      {
        i = model_x->signal[0]->index[j];
        model_x->wavelet(model_x->signal[0]->templates, model_x->signal[0]->intParams[i], N, 1, data->Tobs);
      }

      combinePolarizations(data, model_x->signal, model_x->h, model_x->extParams, model_x->Npol);

      computeProjectionCoeffs(data, model_x->projection, model_x->extParams, data->fmin, data->fmax);

      waveformProject(data, model_x->projection, model_x->extParams, model_x->response, model_x->h, data->fmin, data->fmax);

    }

    if(data->glitchFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        for(j=1; j<=model_x->glitch[ifo]->size; j++)
        {
          i = model_x->glitch[ifo]->index[j];
          model_x->wavelet(model_x->glitch[ifo]->templates, model_x->glitch[ifo]->intParams[i], N, 1, data->Tobs);
        }
      }
    }

    //recompute likelihood
    for(ifo=0; ifo<NI; ifo++)
    {
      for(i=0; i<data->N; i++)
      {
        data->r[ifo][i] = data->s[ifo][i];
        if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];
        if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      }
    }

    model_x->logL = 0.0;
    model_x->logLnorm = 0.0;
    if(!data->constantLogLFlag)
    {
      if(data->stochasticFlag)
      {
        //TODO: No support for glitch model in stochastic likelihood
        ComputeNoiseCorrelationMatrix(data, model_x->Snf, model_x->Sn, model_x->background);
        model_x->logL = loglike_stochastic(data->NI, data->imin, data->imax, data->r, model_x->background->Cij, model_x->background->detCij);
      }
      else
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          model_x->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model_x->Sn[ifo], model_x->invSnf[ifo]);
          model_x->logL += model_x->detLogL[ifo];
          for(i=0; i<data->N/2; i++)
          {
            model_x->logLnorm -= log(model_x->Snf[ifo][i]);
          }

        }
      }
    }


  }//end loop over chains
}

double EvaluateConstantLogLikelihood(int typ, int ii, int det, UNUSED struct Model *mx, struct Model *my, struct Prior *prior, UNUSED struct Chain *chain, UNUSED struct Data *data)
{
   // Rejection sample on prior range
   struct Wavelet *wave = NULL;

  int n;
  
  int Npol = mx->Npol;

  //Only check (+) model if assuming elliptical polarization
  if(data->polarizationFlag) Npol=1;
  
  if(det==-1)
  {
    for(n=0; n<Npol; n++)
    {
      wave = my->signal[n];
      if(typ!=3 && wave->size > 0)
      {
        if( checkrange(wave->intParams[ii],prior->range, data->NW)) return -1.0e60;
      }
    }
  }
  else
  {
    wave = my->glitch[det];
    
    /*
     type 3 is a death move -- params[ii] is out of the picture
     and doesn't need to be checked
     */
    if(typ!=3 && wave->size > 0)
    {
      if( checkrange(wave->intParams[ii],prior->range, wave->dimension)) return -1.0e60;
    }
  }
  
  return 0.0;
}

double EvaluateMarkovianLogLikelihood(int typ, int ii, int det, struct Model *mx, struct Model *my, struct Prior *prior, struct Chain *chain, struct Data *data)
{
   int i,j,k,n,ifo,jj;
   double logLy = 0.0;
   double logLseg = 0.0;
   double fmin=data->fmin;
   double fmax=data->fmax;
   double fi,ff;

   //Unpack model_ & data_parameter structures
   int N  = data->N;
   int NI = data->NI;
   int imin = data->imin;
   int imax = data->imax;
   int jmin = imin;
   int jmax = imax;

   double **r   = data->r;
   double **s   = data->s;
   double Tobs  = data->Tobs;

  double **invSnf = mx->invSnf;

   //pointer to model being varied (glitch or signal)
   struct Wavelet *wave_x = NULL;
   struct Wavelet *wave_y = NULL;
   if(det == -1)
   {
      wave_x = mx->signal[0];
      wave_y = my->signal[0];
   }
   else
   {
      wave_x = mx->glitch[det];
      wave_y = my->glitch[det];
   }

   //proposed model
   double *Sny = my->Sn;
   double **ry = my->response;
   double **hy = my->h;
  
   int sy = wave_y->size;

   //short-cut pointer to glitch models
   double **gx = NULL;
   double **gy = NULL;
   gx = malloc(NI*sizeof(double*));
   gy = malloc(NI*sizeof(double*));

   for(ifo=0; ifo<NI; ifo++)
   {
      gx[ifo] = mx->glitch[ifo]->templates;
      gy[ifo] = my->glitch[ifo]->templates;

      my->detLogL[ifo] = mx->detLogL[ifo];

   }

   // Rejection sample on prior range
   /*
    type 3 is a death move -- params[ii] is out of the picture
    and doesn't need to be checked
    */
   if( (data->signalFlag || data->glitchFlag) && typ!=3 && wave_y->size>0 )
   {
      if(checkrange(wave_y->intParams[ii],prior->range, wave_y->dimension))
      {
         free(gx);
         free(gy);
         return -1.0e60;
      }
   }

   // copy over current multi-template
   for(ifo=0; ifo<NI; ifo++)
   {
      if(ifo==0)
      {
         for(i=0; i<N; i++)
         {
           if(det==-1)
           {
             for(n=0; n<mx->Npol; n++)
             {
               my->signal[n]->templates[i] = mx->signal[n]->templates[i];
             }
           }
            else wave_y->templates[i] = wave_x->templates[i];
           
           //while we're at it, copy data->s (the data) into data->r (workspace for forming the residual)
            r[ifo][i]  = s[ifo][i];
         }
      }
      else
      {
         for(i=0; i<N; i++)
         {
            r[ifo][i]  = s[ifo][i];
         }
      }
      if(mx->glitch[ifo]->size>0)
      {
        //subtract current glitch model from residual
         for(i=0; i<N; i++)
         {
            gy[ifo][i] = gx[ifo][i];
            r[ifo][i] -= gx[ifo][i];
         }
      }
      if(mx->signal[0]->size>0)
      {
        //subtract current signal model from residual
         for(i=0; i<N; i++)
         {
            my->response[ifo][i] = mx->response[ifo][i];
            data->r[ifo][i]     -= mx->response[ifo][i];
         }
      }
   }

   switch(typ)
   {
         // Fixed dimension MCMC move
      case 1:
         if(sy > 0)
         {
           //signal model
           if(det==-1)
           {
             for(n=0; n<mx->Npol; n++)
             {
               // subtract the current wavelet
               mx->wavelet(my->signal[n]->templates, mx->signal[n]->intParams[ii], N, -1, Tobs);
               mx->wavelet_bandwidth(mx->signal[n]->intParams[ii], &fmin,&fmax);
               
               // add the new wavelet
               mx->wavelet(my->signal[n]->templates, my->signal[n]->intParams[ii], N, 1, Tobs);
               mx->wavelet_bandwidth(my->signal[n]->intParams[ii], &fi,&ff);
             }
           }
           //glitch model
           else
           {
             // subtract the current wavelet
             mx->wavelet(wave_y->templates, wave_x->intParams[ii], N, -1, Tobs);
             mx->wavelet_bandwidth(wave_x->intParams[ii], &fmin,&fmax);
             
             // add the new wavelet
             mx->wavelet(wave_y->templates, wave_y->intParams[ii], N, 1, Tobs);
             mx->wavelet_bandwidth(wave_y->intParams[ii], &fi,&ff);
           }
            //Make sure subtracted frequency is in range
            if(fi < fmin)
               fmin = fi;
            if(ff > fmax)
               fmax = ff;

            //Make sure added frequency is in range
            if(fmin < data->fmin)
               fmin = data->fmin;
            if(fmax > data->fmax)
               fmax = data->fmax;

         }
         break;

         // RJMCMC birth move
      case 2:
       //signal model
       if(det==-1)
       {
         for(n=0; n<mx->Npol; n++)
         {
           // add the new wavelet
           mx->wavelet(my->signal[n]->templates, my->signal[n]->intParams[ii], N, 1, Tobs);
           mx->wavelet_bandwidth(my->signal[n]->intParams[ii], &fmin,&fmax);
         }
       }
       //glitch model
       else
       {

         mx->wavelet(wave_y->templates, wave_y->intParams[ii], N, 1, Tobs);
       
         mx->wavelet_bandwidth(wave_y->intParams[ii], &fmin,&fmax);
       }
         //Make sure frequency is in range
         if(fmin < data->fmin)
            fmin = data->fmin;
         if(fmax > data->fmax)
            fmax = data->fmax;

         break;

         // RJMCMC death move
      case 3:
       if(det==-1)
       {
         for(n=0; n<mx->Npol; n++)
         {
           // add the new wavelet
           mx->wavelet(my->signal[n]->templates, mx->signal[n]->intParams[ii], N, -1, Tobs);
           mx->wavelet_bandwidth(mx->signal[n]->intParams[ii], &fmin,&fmax);
         }
       }
       //glitch model
       else
       {

         // subtract the current wavelet
         mx->wavelet(wave_y->templates, wave_x->intParams[ii], N, -1, Tobs);

         mx->wavelet_bandwidth(wave_x->intParams[ii], &fmin,&fmax);
       }
         //Make sure frequency is in range
         if(fmin < data->fmin)
            fmin = data->fmin;
         if(fmax > data->fmax)
            fmax = data->fmax;

         break;

         //RJMCMC birth-death move
      case 4:
       printf("WARNING: NO SUPPORT FOR BIRTH DEATH MOVE\n");
       exit(1);
         //Some helper pointers
         if(det==-1) ifo = (int)floor(uniform_draw(chain->seed)*NI);

         struct Wavelet *wBirth = NULL;
         struct Wavelet *wDeath = NULL;

         if(det==-1)
         {
            wDeath = my->signal[0];
            wBirth = my->glitch[ifo];
         }
         else
         {
            wDeath = my->glitch[det];
            wBirth = my->signal[0];
         }

         // subtract the current wavelet from the signal model
         mx->wavelet(wDeath->templates, wDeath->intParams[ii], N, -1, Tobs);
         mx->wavelet_bandwidth(wDeath->intParams[ii], &fmin, &fmax);
         //Make sure frequency is in range
         if(fmin < data->fmin)
            fmin = data->fmin;
         if(fmax > data->fmax)
            fmax = data->fmax;

         combinePolarizations(data, my->signal, my->h, my->extParams, my->Npol);

         waveformProject(data,my->projection, my->extParams, ry, hy, fmin, fmax);

         // where in glitch model do I put the new parameters?
         // find a label that isn't in use
         jj = 0;
         do
         {
            jj++;
            k = 0;
            for(j=0; j< wBirth->size; j++) if(jj == wBirth->index[j]) k = 1;
         } while(k == 1);

         wBirth->index[wBirth->size] = jj;

         wBirth->size++;

         // shift time of signal
         for(i=0; i<wave_x->dimension; i++) wBirth->intParams[jj][i] = wDeath->intParams[ii][i];
         if(det==-1)
         {
            wBirth->intParams[jj][0] += my->projection->deltaT[ifo];
            wBirth->intParams[jj][3] *= sqrt(my->projection->Fplus[ifo]*my->projection->Fplus[ifo]+my->projection->Fcross[ifo]*my->projection->Fcross[ifo]);
            wBirth->intParams[jj][4] = uniform_draw(chain->seed)*LAL_TWOPI;
         }
         else wBirth->intParams[jj][0] -= my->projection->deltaT[ifo];

         // add wavelet
         mx->wavelet(wBirth->templates, wBirth->intParams[jj], N, 1, Tobs);

         break;

      default:
         break;
   }
   //Make sure template array is zero if we have no wavelets in model
   if(sy == 0)
   {
     for(i=0; i< N; i++)
     {
       if(det==-1)
       {
         for(n=0; n>mx->Npol; n++)
         {
           my->signal[n]->templates[i] = 0.0;
         }
       }
       else wave_y->templates[i] = 0.0;
     }
   }

   // form up the resdial s-h
   if(det==-1 && data->signalFlag)
   {
     combinePolarizations(data, my->signal, my->h, my->extParams, my->Npol);

      waveformProject(data,my->projection, my->extParams, ry, hy, data->fmin, data->fmax);
   }
  
  //try just computing deltaLogL for birth-death moves in glitch model
  if(typ==1 && data->intrinsic_likelihood == EvaluateMarkovianLogLikelihood)
  {

    if(data->stochasticFlag)
    {
      //TODO: No support for glitch model in stochastic likelihood
      ComputeNoiseCorrelationMatrix(data, mx->Snf, my->Sn, my->background);

      logLy = loglike_stochastic(NI, imin, imax, r, my->background->Cij, my->background->detCij);
    }
    else
    {

      logLy = 0.0;

      /*
       Compute logLy if we only change Sn:
       logLx = -(r|r)/2/Snx - NlogSnx
       logLy = -(r|r)/2/Sny - NlogSny
       -(r|r)/2 = (logLx + NlogSnx)*Snx
       so
       logLy = (logLx + NlogSnx)*(Snx/Sny) - NlogSny
       */
      for(ifo=0; ifo<NI; ifo++)
      {
        my->detLogL[ifo] = (mx->detLogL[ifo] + (imax-imin)*log(mx->Sn[ifo]))*(mx->Sn[ifo]/Sny[ifo]) -(imax-imin)*log(Sny[ifo]);
        logLy += my->detLogL[ifo];
      }

      if(sy > 0)
      {
        /*
         Now adjust logLy for changes in the glitch/signal parameters
         Keep the cost down by only computing deltaLogL in bins
         where modified wavelets have support
         */
        double fminx,fmaxx;
        double fminy,fmaxy;

        // subtract the current wavelet
        mx->wavelet_bandwidth(wave_x->intParams[ii],&fminx,&fmaxx);

        // add the new wavelet
        mx->wavelet_bandwidth(wave_y->intParams[ii],&fminy,&fmaxy);

        // make sure bandwidth spans both wavelets
        fmin = fminx;
        if(fminy<fmin) fmin = fminy;

        fmax = fmaxx;
        if(fmaxy>fmax) fmax = fmaxy;

        jmin = (int)floor(fmin*Tobs);
        jmax = (int)floor(fmax*Tobs);

        if(jmin<imin) jmin = imin;
        if(jmax>imax) jmax = imax;

        if(det==-1)
        {
          for(ifo=0; ifo<NI; ifo++)
          {
            //remove contribution between imin and imax of old model
            logLseg = loglike(jmin, jmax, r[ifo], Sny[ifo], invSnf[ifo]);
            my->detLogL[ifo] -= logLseg;
            logLy -= logLseg;

            //add contribution between imin and imax of new model
            for(i=jmin; i<jmax; i++)
            {
              j = 2*i;
              k = j+1;
              r[ifo][j] = s[ifo][j];
              r[ifo][k] = s[ifo][k];
              if(data->glitchFlag)
              {
                r[ifo][j] -= gy[ifo][j];
                r[ifo][k] -= gy[ifo][k];
              }
              if(data->signalFlag)
              {
                r[ifo][j] -= ry[ifo][j];
                r[ifo][k] -= ry[ifo][k];
              }
            }
            logLseg = loglike(jmin, jmax, r[ifo], Sny[ifo], invSnf[ifo]);
            my->detLogL[ifo] += logLseg;
            logLy += logLseg;
          }
        }
        else
        {
          //remove contribution between imin and imax of old model
          logLseg = loglike(jmin, jmax, r[det], Sny[det], invSnf[det]);
          my->detLogL[det] -= logLseg;
          logLy -= logLseg;

          //add contribution between imin and imax of new model
          for(i=jmin; i<jmax; i++)
          {
            j = 2*i;
            k = j+1;
            r[det][j] = s[det][j] - ry[det][j] - gy[det][j];
            r[det][k] = s[det][k] - ry[det][k] - gy[det][k];
          }
          logLseg = loglike(jmin, jmax, r[det], Sny[det], invSnf[det]);
          my->detLogL[det] += logLseg;
          logLy += logLseg;
        }
      }
    }
  }
  else if((typ==2 || typ==3) && data->intrinsic_likelihood == EvaluateMarkovianLogLikelihood)
  {
    if(typ==2)
      mx->wavelet_bandwidth(wave_y->intParams[ii],&fmin,&fmax);
    else
      mx->wavelet_bandwidth(wave_x->intParams[ii],&fmin,&fmax);

    for(ifo=0; ifo<NI; ifo++) my->detLogL[ifo] = mx->detLogL[ifo];
    logLy = mx->logL;
    jmin = (int)floor(fmin*Tobs);
    jmax = (int)floor(fmax*Tobs);

    if(jmin<imin) jmin = imin;
    if(jmax>imax) jmax = imax;

    if(det==-1)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        my->detLogL[ifo] = mx->detLogL[ifo];

        //remove contribution between imin and imax of old model
        logLseg = loglike(jmin, jmax, r[ifo], mx->Sn[ifo], invSnf[ifo]);
        my->detLogL[ifo] -= logLseg;
        logLy -= logLseg;

        //add contribution between imin and imax of new model
        for(i=jmin; i<jmax; i++)
        {
          j = 2*i;
          k = j+1;
          r[ifo][j] = s[ifo][j];// - ry[ifo][j] - gy[ifo][j];
          r[ifo][k] = s[ifo][k];// - ry[ifo][k] - gy[ifo][k];
          if(data->glitchFlag)
          {
            r[ifo][j] -= gy[ifo][j];
            r[ifo][k] -= gy[ifo][k];
          }
          if(data->signalFlag)
          {
            r[ifo][j] -= ry[ifo][j];
            r[ifo][k] -= ry[ifo][k];
          }
        }
        logLseg = loglike(jmin, jmax, r[ifo], Sny[ifo], invSnf[ifo]);
        my->detLogL[ifo] += logLseg;
        logLy += logLseg;
      }
    }
    else
    {
      my->detLogL[det] = mx->detLogL[det];

      //remove contribution between imin and imax of old model
      logLseg = loglike(jmin, jmax, r[det], mx->Sn[det], invSnf[det]);
      my->detLogL[det] -= logLseg;
      logLy -=  logLseg;

      //add contribution between imin and imax of new model
      for(i=jmin; i<jmax; i++)
      {
        j = 2*i;
        k = j+1;
        r[det][j] = s[det][j] - ry[det][j] - gy[det][j];
        r[det][k] = s[det][k] - ry[det][k] - gy[det][k];
      }
      logLseg = loglike(jmin, jmax, r[det], Sny[det], invSnf[det]);
      my->detLogL[det] += logLseg;
      logLy += logLseg;
    }
  }
  else
  {
    for(ifo=0; ifo<NI; ifo++)
    {
      for(i=0; i< N; i++)
      {
        r[ifo][i] = s[ifo][i];
        //if(data->signalFlag || data->glitchFlag) r[ifo][i] -= ry[ifo][i] + gy[ifo][i];
        if(data->glitchFlag)
        {
          r[ifo][i] -= gy[ifo][i];
        }
        if(data->signalFlag)
        {
          r[ifo][i] -= ry[ifo][i];
        }
      }
      my->detLogL[ifo] = loglike(imin, imax, r[ifo], Sny[ifo], invSnf[ifo]);
      logLy += my->detLogL[ifo];
    }
  }

  free(gx);
  free(gy);

  return logLy;

}

double EvaluateExtrinsicConstantLogLikelihood(UNUSED struct Network *projection, double *params, UNUSED double **invSnf, UNUSED double *Sn, UNUSED struct Wavelet **geo, UNUSED double **g, UNUSED struct Data *data, UNUSED double fmin, UNUSED double fmax)
{
   if(extrinsic_checkrange(params)) return -1.0e60;

   else return 0.0;
}

double EvaluateExtrinsicMarkovianLogLikelihood(struct Network *projection, double *params, double **invSnf, double *Sn, struct Wavelet **geo, double **g, struct Data *data, double fmin, double fmax)
{
   int i, n;
   int NI,NP,N;
   int ifo;
   int imin,imax;

   //double sum = 0.0;
   double logL = 0.0;
   double **h, **d, **r, **t;

   if(extrinsic_checkrange(params)) return -1.0e60;

   NI   = data->NI;
   N = data->N;
  NP = data->Npol;
  
  /*
   if Npol = 1 assume elliptical polarization, but still need room for hx
   */
  if(NP<2) NP = 2;
  
   //imin = data->imin;
   //imax = data->imax;
   imin = (int)(fmin*data->Tobs);
   imax = (int)(fmax*data->Tobs);

   d   = data->s;
   t   = double_matrix(NP-1,N-1); //template
   h   = double_matrix(NI-1,N-1); //response
   r   = double_matrix(NI-1,N-1); //residual


   //initialize template and residual to 0's
   for(i=0; i<NI; i++) for(n=0; n<N; n++) h[i][n] = 0.0;

   //compute instrument response h[] to geocenter waveform geo[]
   combinePolarizations(data, geo, t, params, data->Npol);
   computeProjectionCoeffs(data, projection, params, fmin, fmax);
   waveformProject(data, projection, params, h, t, fmin, fmax);
   
   
  //Form up residual
  for(ifo=0; ifo<NI; ifo++)
  {
    for(n=0; n<N; n++)
    {
      r[ifo][n] = d[ifo][n];
      if(data->signalFlag) r[ifo][n] -= h[ifo][n];
      if(data->glitchFlag) r[ifo][n] -= g[ifo][n];
    }
    logL += loglike(imin, imax, r[ifo], Sn[ifo], invSnf[ifo]);
  }


   free_double_matrix(h,NI-1);
   free_double_matrix(r,NI-1);
   free_double_matrix(t,NP-1);
  
   return logL;
}

double EvaluateExtrinsicSearchLogLikelihood(struct Network *projection, double *params, double **invSnf, double *Sn, struct Wavelet **geo, double **g, struct Data *data, double fmin, double fmax)
{
   int i, n;
   int NI,N;
   int ifo;
   int imin,imax;
   
   double Tobs;
   double logL = 0.0;
   double dt=0,dphi=0,dA=1;
   double **h, **d, **r, **t;
   
   if(extrinsic_checkrange(params)) return -1.0e60;
   
   NI   = data->NI;
   N = data->N;
   Tobs = data->Tobs;
   imin = (int)floor(fmin*data->Tobs);//data->imin;
   imax = (int)floor(fmax*data->Tobs);//data->imax;
   
   d   = data->s;
   t   = double_matrix(NI-1,N-1); //template
   h   = double_matrix(NI-1,N-1); //response
   r   = double_matrix(NI-1,N-1); //residual
   
   //initialize template and residual to 0's
   for(i=0; i<NI; i++) for(n=0; n<N; n++) h[i][n] = 0.0;
   
   //compute instrument response h[] to geocenter waveform geo[]
   combinePolarizations(data, geo, t, params, data->Npol);
   computeProjectionCoeffs(data, projection, params, data->fmin, data->fmax);
   waveformProject(data, projection, params, h, t, data->fmin, data->fmax);
   
   //Form up residual
   for(ifo=0; ifo<NI; ifo++)
   {
      for(n=0; n<N; n++)
      {
         r[ifo][n] = d[ifo][n];
         if(data->signalFlag) r[ifo][n] -= h[ifo][n];
         if(data->glitchFlag) r[ifo][n] -= g[ifo][n];
      }
   }

   //Amplitude
   params[1] *= dA;
   
   //Time
   params[5] += dt;
   
   //Phase
   params[6] += dphi;
   
   if(params[6] < 0.0)       params[6] += LAL_TWOPI;
   if(params[6] > LAL_TWOPI) params[6] -= LAL_TWOPI;
   
   if(params[5] < 0.0)  params[5] += Tobs;
   if(params[5] > Tobs) params[5] -= Tobs;
   
   // check that we haven't mapped out of range
   if(extrinsic_checkrange(params))
   {
       free_double_matrix(h,NI-1);
       free_double_matrix(r,NI-1);
       return -1.0e60;
   }
   
   //Now update the full model with current extrinsic parameters
   combinePolarizations(data, geo, t, params, data->Npol);
   computeProjectionCoeffs(data, projection, params, data->fmin, data->fmax);
   waveformProject(data, projection, params, h, t, data->fmin, data->fmax);
   //Form up residual
   for(ifo=0; ifo<NI; ifo++)
   {
      for(n=0; n<N; n++)
      {
         r[ifo][n] = d[ifo][n];
         if(data->signalFlag) r[ifo][n] -= h[ifo][n];
         if(data->glitchFlag) r[ifo][n] -= g[ifo][n];
      }
   }
   logL = 0.0;
   for(ifo=0; ifo<NI; ifo++) logL += loglike(imin, imax, r[ifo], Sn[ifo], invSnf[ifo]);
   
   free_double_matrix(h,NI-1);
   free_double_matrix(r,NI-1);
   free_double_matrix(t,NI-1);
  
   return logL;
}
