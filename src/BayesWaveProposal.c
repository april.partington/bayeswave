//TODO: Amplitude prior and proposals need to call same functions!

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gsl/gsl_statistics.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_blas.h>

#include "BayesWave.h"
#include "BayesLine.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveLikelihood.h"

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* ********************************************************************************** */
/*                                                                                    */
/*                                    Prior proposals                                 */
/*                                                                                    */
/* ********************************************************************************** */

void draw_glitch_amplitude(double *params, double *Snf, double Sa, gsl_rng *seed, double Tobs, double **range, double SNRpeak)
{
  int i, k;
  double SNR, den=-1.0, alpha=1.0;
  double max;
  double invmax;

  double dfac, dfac3;

  Sa = 1.0;

  double SNR2 = 2.0*SNRpeak;
  double SNRsq = 2.0*SNRpeak*SNRpeak;

  dfac = 1.+SNRpeak/(SNR2);
  dfac3 = dfac*dfac*dfac;
  max = (SNRpeak)/(2.*SNRsq*dfac3);
  invmax = 1./max;

  i = (int)(params[1]*Tobs);

  double SNRmin = range[3][0]*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));
  double SNRmax = range[3][1]*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));


  k = 0;
  SNR = SNRmin + (SNRmax-SNRmin)*uniform_draw(seed);

  dfac = 1.+SNR/(SNR2);
  dfac3 = dfac*dfac*dfac;

  den = (SNR)/(SNRsq*dfac3);

  den *= invmax;

  alpha = uniform_draw(seed);
  while(alpha > den)
  {

    SNR = SNRmin + (SNRmax-SNRmin)*uniform_draw(seed);

    dfac = 1.+SNR/(SNR2);
    dfac3 = dfac*dfac*dfac;

    den = (SNR)/(2.*SNRsq*dfac3);

    den *= invmax;

    alpha = uniform_draw(seed);

    k++;

    //you had your chance
    if(k>10000)
    {
      SNR=0.0;
      break;
    }
  }

  //SNR defined with Sn(f) but Snf array holdes <n_i^2>
  params[3] = SNR/sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));

}

void draw_signal_amplitude(double *params, double *Snf, double Sa, gsl_rng *seed, double Tobs, double **range, double SNRpeak)
{
  int i, k;
  double SNR, den=-1.0, alpha=1.0;
  double max;
  double invmax;

  double dfac, dfac5;

  Sa = 1.0;

  double SNR4 = 4.0*SNRpeak;
  double SNRsq = 4.0*SNRpeak*SNRpeak;

  dfac = 1.+SNRpeak/(SNR4);
  dfac5 = dfac*dfac*dfac*dfac*dfac;
  max = (3.*SNRpeak)/(SNRsq*dfac5);
  invmax = 1./max;

  i = (int)(params[1]*Tobs);

  double SNRmin = range[3][0]*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));
  double SNRmax = range[3][1]*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));


  k = 0;
  SNR = SNRmin + (SNRmax-SNRmin)*uniform_draw(seed);

  dfac = 1.+SNR/(SNR4);
  dfac5 = dfac*dfac*dfac*dfac*dfac;

  den = (3.*SNR)/(SNRsq*dfac5);

  den *= invmax;

  alpha = uniform_draw(seed);
  while(alpha > den)
  {

    SNR = SNRmin + (SNRmax-SNRmin)*uniform_draw(seed);

    dfac = 1.+SNR/(SNR4);
    dfac5 = dfac*dfac*dfac*dfac*dfac;

    den = (3.*SNR)/(SNRsq*dfac5);

    den *= invmax;

    alpha = uniform_draw(seed);

    k++;

    //you had your chance
    if(k>10000)
    {
      SNR=0.0;
      break;
    }


  }

  //SNR defined with Sn(f) but Snf array holdes <n_i^2>
  params[3] = SNR/sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));
  
//  FILE *temp=fopen("prior.dat","a");
//  fprintf(temp,"%lg\n",params[3]);
//  fclose(temp);

}

void draw_uniform_amplitude(double *params, double *Snf, double Sa, gsl_rng *seed, double Tobs, double **range)
{
  int i;
  double SNR;

  Sa = 1.0;

  i = (int)(params[1]*Tobs);

  double SNRmin = range[3][0];//*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));
  double SNRmax = range[3][1];//*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));
  SNR = SNRmin + (SNRmax-SNRmin)*uniform_draw(seed);


  //SNR defined with Sn(f) but Snf array holdes <n_i^2>
  params[3] = SNR/sqrt((params[2]/(2.0*RT2PI*params[1]))/(Sa*Snf[i]*2.0/Tobs));
  
}

void extrinsic_uniform_proposal(gsl_rng *seed, double *y)
{
  y[0] = uniform_draw(seed)*LAL_TWOPI;
  y[1] = -1.0 + 2.0*uniform_draw(seed);
  y[2] = uniform_draw(seed)*LAL_PI_2;
  y[3] = -0.99 + 1.98*uniform_draw(seed);
  y[4] = uniform_draw(seed)*LAL_TWOPI;
  //y[4] = uniform_draw(seed)*LAL_PI;
  y[5] = 1.0;
}

void uniform_orientation_proposal(double *y, gsl_rng *seed)
{

  double newPsi,newPhi,newEcc;

  newPhi = uniform_draw(seed)*LAL_TWOPI;
  newPsi = uniform_draw(seed)*LAL_PI_2;
  newEcc = -1. + 2.*uniform_draw(seed);

  y[2] = newPsi;
  y[3] = newEcc;
  y[4] = newPhi;

}

void intrinsic_halfcycle_proposal(double *x, double *y, gsl_rng *seed)
{
  double n;
  double t0,f0,ph,dt,dp;
  
  t0 = x[0];
  f0 = x[1];
  ph = x[4];
  
  //choose number of half period shifts
  n = -2.0 + floor(5.0*uniform_draw(seed));
  
  //shift time by half period
  dt = (n/2.)/f0*(1. + 0.1*gaussian_draw(seed));
  dp = 0.0;
  if((int)n%2!=0) dp = LAL_PI*(1. + 0.1*gaussian_draw(seed));
  
  y[0] = t0 + dt;
  y[4] = ph + dp;
  
  while(y[4] > LAL_TWOPI) y[4] -=LAL_TWOPI;
  while(y[4] < LAL_TWOPI) y[4] +=LAL_TWOPI;
  
}

/* ********************************************************************************** */
/*                                                                                    */
/*                                Fisher matrix proposals                             */
/*                                                                                    */
/* ********************************************************************************** */


void extrinsic_fisher_information_matrix(struct FisherMatrix *fisher, double *params, double **invSnf, double *Sn, struct Model *model, double **glitch, struct Data *data)
{
  int i, j, k;
  int NI,N;

  double *paramsP, *paramsM;
  double logLP, logLM;
  double **hP, **hM, **hPP, **hMM;
  double ***hdiv;
  double *epsilon;
  double **s;
  double stab;

  struct Network *projectionP = malloc(sizeof(struct Network));
  struct Network *projectionM = malloc(sizeof(struct Network));

  NI   = data->NI;
  N    = data->N;

  initialize_network(projectionP,N, NI);
  initialize_network(projectionM,N, NI);

  stab = 10.0;

  // Plus and minus parameters:
  paramsP = double_vector(NE-1);
  paramsM = double_vector(NE-1);

  // Plus and minus templates for each detector:
  s     = double_matrix(NI-1,N-1);
  hP		= double_matrix(NI-1,N-1);
  hM		= double_matrix(NI-1,N-1);
  hPP		= double_matrix(NI-1,N-1);
  hMM		= double_matrix(NI-1,N-1);

  hdiv = double_tensor(NE-1,NI-1,N-1);

  epsilon = double_vector(NE-1);

  // Compute SNR of signal model
  combinePolarizations(data,model->signal,model->h,params,model->Npol);
  computeProjectionCoeffs(data, projectionP, params, data->fmin, data->fmax);
  waveformProject(data, projectionP, params, hP, model->h, data->fmin, data->fmax);

  //store data
  for(i=0;i<NI;i++)
  {
    for(j=0;j<N;j++)
    {
      s[i][j]=data->s[i][j];
      data->s[i][j]=hP[i][j];
    }
  }

  double range;
  for(i = 0; i < NE; i++)
  {
    switch(i)
    {
      case 0:
        range = LAL_TWOPI;
        break;
      case 1:
        range = 2.0;
        break;
      case 2:
        range = LAL_PI_2;
        break;
      case 3:
        range = 0.99*2;
        break;
      case 4:
        range = LAL_TWOPI;
        break;
      case 5:
            //TODO:  HACK!  testing extrinsic amplitude update
        range = 2.0;
        break;
    }

    epsilon[i] = 1.0e-5*range;

    for(j = 0; j < NE; j++)
    {
      paramsP[j] = params[j];
      paramsM[j] = params[j];
    }

    paramsP[i] += epsilon[i];
    paramsM[i] -= epsilon[i];


    logLP = EvaluateExtrinsicMarkovianLogLikelihood(projectionP, paramsP, invSnf, Sn, model->signal, glitch, data, data->fmin, data->fmax);
    logLM = EvaluateExtrinsicMarkovianLogLikelihood(projectionM, paramsM, invSnf, Sn, model->signal, glitch, data, data->fmin, data->fmax);
    epsilon[i] = 0.1/sqrt(-(logLM+logLP)/(epsilon[i]*epsilon[i]));
    if(epsilon[i]!=epsilon[i])epsilon[i]=1.0e-5;
  }

  //restore data
  for(i=0;i<NI;i++)  for(j=0;j<N;j++) data->s[i][j]=s[i][j];

  /* assumes all the parameters are log or angles */
  for(i=0; i<NE; i++)
  {
    for(j=0; j<NE; j++)
    {
      paramsP[j] = params[j];
      paramsM[j] = params[j];
    }


    paramsP[i] += epsilon[i];
    paramsM[i] -= epsilon[i];

    combinePolarizations(data,model->signal,model->h,paramsP,model->Npol);
    computeProjectionCoeffs(data, projectionP, paramsP, data->fmin, data->fmax);
    waveformProject(data, projectionP, paramsP, hP, model->h, data->fmin, data->fmax);

    combinePolarizations(data,model->signal,model->h,paramsM,model->Npol);
    computeProjectionCoeffs(data, projectionM, paramsM, data->fmin, data->fmax);
    waveformProject(data, projectionM, paramsM, hM, model->h, data->fmin, data->fmax);

    paramsP[i] += epsilon[i];
    paramsM[i] -= epsilon[i];

    combinePolarizations(data,model->signal,model->h,paramsP,model->Npol);
    computeProjectionCoeffs(data, projectionP, paramsP, data->fmin, data->fmax);
    waveformProject(data, projectionP, paramsP, hPP, model->h, data->fmin, data->fmax);
    
    combinePolarizations(data,model->signal,model->h,paramsM,model->Npol);
    computeProjectionCoeffs(data, projectionM, paramsM, data->fmin, data->fmax);
    waveformProject(data, projectionM, paramsM, hMM, model->h, data->fmin, data->fmax);


    //Central differencing
    for(k=0; k<NI; k++)
    {
      for(j=0; j<N; j++)
      {
        hdiv[i][k][j] = (hMM[k][j] + 8.0*hP[k][j] - 8.0*hM[k][j] - hPP[k][j])/(12.0*epsilon[i]);
      }
    }
  }

  for(i=0; i<NE; i++) for(j=0; j<NE; j++) fisher->matrix[i][j] = network_nwip(data->imin,data->imax,hdiv[i], hdiv[j], invSnf, Sn, NI);

  // stabilizers
  for(i=0; i<NE; i++)  fisher->matrix[i][i] += stab;


  free_double_vector(paramsP);
  free_double_vector(paramsM);

  free_double_tensor(hdiv,NE-1,NI-1);

  free_double_matrix(s,NI-1);
  free_double_matrix(hP,NI-1);
  free_double_matrix(hM,NI-1);
  free_double_matrix(hPP,NI-1);
  free_double_matrix(hMM,NI-1);

  free_double_vector(epsilon);

  free_network(projectionM,NI);
  free_network(projectionP,NI);
  free(projectionP);
  free(projectionM);

}

void extrinsic_fisher_update(struct Data *data, struct Model *model)
{
  int i,j;

  double *params = model->extParams;
  double *Sn     = model->Sn;

  double **glitch = malloc(data->NI*sizeof(double*));
  for(i=0; i<data->NI; i++)
  {
      glitch[i]=malloc(data->N*sizeof(double));
      for(j=0; j<data->N; j++)glitch[i][j] = model->glitch[i]->templates[j];
  }

  struct FisherMatrix *fisher = model->fisher;

  //calculate elements of Fisher
  extrinsic_fisher_information_matrix(fisher, params, model->invSnf, Sn, model, glitch, data);

  matrix_eigenstuff(fisher->matrix, fisher->evector, fisher->evalue, fisher->N);

  for(i=0; i<data->NI; i++) free(glitch[i]);
  free(glitch);

}

void fisher_matrix_proposal(struct FisherMatrix *fisher, double *params, gsl_rng *seed, double *y)
{
  int i, j;

  int N = fisher->N;

  double Amps[N];
  double jump[N];

    for(i=0; i<N; i++) Amps[i]=jump[i]=0.0;

  // not used since we jump along eigendirections
  double sqD = 1./sqrt((double)(N));

  //choose a single eigenvector to jump over
  i = (int)(uniform_draw(seed)*(double)N);

  //draw the eigen-jump amplitudes from N[0,1] scaled by evalue & dimension
//  for(i=0; i<N; i++)
//  {
    Amps[i] = 1.0/sqrt(fisher->evalue[i]);
    jump[i]	= 0.0;
//  }

  //decompose eigenjumps into parameter directions
  //for(i=0; i<N; i++)
  for (j=0; j<N; j++) jump[j] += Amps[i]*fisher->evector[j][i]*sqD;

  //jump from current position
  for(i=0; i<N; i++) y[i] = params[i] + gaussian_draw(seed)*jump[i];
}

void intrinsic_fisher_update(double *params, double *dparams, double *Snf, double Snx, double Tobs, int NW, int TFQFLAG)
{
  int i;
  double SNR;//,SNR2;
  i = (int)(params[1]*Tobs);
  double betasq;

  //SNR defined with Sn(f) but Snf array holdes <n_i^2>
  SNR = params[3]*sqrt((params[2]/(2.0*RT2PI*params[1]))/(Snx*Snf[i]*2.0/Tobs));

  if(SNR < 5.0) SNR = 5.0;  // this caps the size of the proposed jumps

  double sqrt3=1.73205080756888;
  double invSNR = 1./SNR;
  
  //double rtQ = sqrt(params[2]);
  betasq = 0.0;
  if (NW == 6) betasq = params[5]*params[5];

  double Qsq = params[2]*params[2];
  
  for(int k=0; k<NW; k++) dparams[k] = 0.0;

  // [0] t, [1] f, [2] Q, [3] A, [4] phi, [5] beta

  //always compute deltas for amplitude and phase
  dparams[3] = params[3]*invSNR; //amplitude
  dparams[4] = 1.0*invSNR;       //phase

  /*
   only update deltas for intrinsic parameters when TFQ flag is FALSE
   TFQ flag is TRUE when holding TFQ(beta) fixed, e.g. when updating hx
   */
  if(!TFQFLAG)
  {
    dparams[0] = params[2]/LAL_TWOPI/params[1]/sqrt(Qsq+1.0+LAL_PI_2*betasq)*invSNR; //time
    dparams[1] = 2.0*params[1]/sqrt(Qsq+3.0+3.0*LAL_PI_2*betasq)*invSNR;             //frequency
    dparams[2] = 2.0*params[2]/sqrt3/sqrt(1.0+LAL_PI_2*betasq)*invSNR;               //Q
    if (NW == 6) dparams[5] = 4.0/sqrt3/LAL_PI*invSNR;                               //beta
  }
}

void intrinsic_fisher_matrix(double *params, struct FisherMatrix *fisher, int NW)
{
  double **analytic_fisher = fisher->matrix;

  double t, f, Q, A, phi, beta = 0.0;
  //double f, Q, A, beta=0.0;
  int ii, jj, Nparams;

  //TODO: intrinsic_fisher_matrix has hard-coded D-wavelet!
  Nparams = NW;

  t = params[0];
  f = params[1];
  Q = params[2];
  A = params[3];
  phi = params[4];
  if(NW>5) beta = params[5];

  //precomput repeated constants
  double PI2 = LAL_PI*LAL_PI;
  double f2  = f*f;
  double Q2  = Q*Q;
  double b2  = beta*beta;
  double invf= 1./f;
  double invQ= 1./Q;
  double invA= 1./A;

  // Zero matrix out
  for (ii = 0; ii < Nparams; ii++) {
    for (jj = 0; jj < Nparams; jj++) {
      analytic_fisher[ii][jj] = 0.0;
    }
  }

  // t t
  analytic_fisher[0][0] = (4.0*f2*PI2*(1.+Q2+PI2*b2))*invQ*invQ;

  // t f
  analytic_fisher[0][1] = (-2.0*PI2*beta);
  analytic_fisher[1][0] = analytic_fisher[0][1];

  // t Q
  analytic_fisher[0][2] = PI2*beta*f*invQ;
  analytic_fisher[2][0] = analytic_fisher[0][2];

  // t phi
  analytic_fisher[0][4] = (-2.0*f*LAL_PI);
  analytic_fisher[4][0] = analytic_fisher[0][4];

  // t beta
  analytic_fisher[0][5] = (-PI2*f)/2.0;
  analytic_fisher[5][0] = analytic_fisher[0][5];

  // f f
  analytic_fisher[1][1] = 0.25*((3.0+Q2+3.0*PI2*b2))*invf*invf;//((3.0+Q*Q+3.0*LAL_PI*LAL_PI*beta*beta)/(4.0*f*f));

  // f Q
  analytic_fisher[1][2] = 0.25*(-3.0*(1.0+PI2*b2))*invf*invQ;//(-3.0*(1.0+LAL_PI*LAL_PI*beta*beta)/(4.0*f*Q));
  analytic_fisher[2][1] = analytic_fisher[1][2];

  // f lnA
  analytic_fisher[1][3] = -0.5*invf*invA;//-(1.0/(2.0*f));
  analytic_fisher[3][1] = analytic_fisher[1][3];

  // f phi
  analytic_fisher[1][4] = 0.5*LAL_PI*beta*invf;//LAL_PI*beta/(2.0*f);
  analytic_fisher[4][1] = analytic_fisher[1][4];

  // f beta
  //analytic_fisher[1][5] = 3.0*beta*PI2/(8.0*f);
  //analytic_fisher[5][1] = analytic_fisher[1][5];

  // Q Q
  analytic_fisher[2][2] = 0.25*(3.0*(1.0+PI2*b2))*invQ*invQ;//(3.0*(1.0+LAL_PI*LAL_PI*beta*beta)/(4.0*Q*Q));

  // Q lnA
  analytic_fisher[2][3] = 0.5*invQ*invA;//(1.0/(2.0*Q));
  analytic_fisher[3][2] = analytic_fisher[2][3];

  // Q phi
  analytic_fisher[2][4] = -0.5*LAL_PI*beta*invQ;//-(LAL_PI*beta)/(2.0*Q);
  analytic_fisher[4][2] = analytic_fisher[2][4];

  // Q beta
  analytic_fisher[2][5] = -(3.0*beta*LAL_PI*LAL_PI)/(8.0*Q);
  analytic_fisher[5][2] = analytic_fisher[2][5];

  // lnA lnA
  analytic_fisher[3][3] = invA*invA;

  // phi phi
  analytic_fisher[4][4] = 1.0;

  // phi beta
  analytic_fisher[4][5] = 0.25*LAL_PI;//LAL_PI/4.0;
  analytic_fisher[5][4] = analytic_fisher[4][5];

  // beta beta
  analytic_fisher[5][5] = 3.0*LAL_PI*LAL_PI/16.0;

  // Get eigenvectors/values for Fisher matrix
  matrix_eigenstuff(fisher->matrix, fisher->evector, fisher->evalue, fisher->N);
  
}

void intrinsic_fisher_proposal(UNUSED struct Model *model, double **range, double *paramsx, double *paramsy, double *Snf, double Snx, double *logpx, double *logpy, gsl_rng *seed, double Tobs, int *test, int NW, int TFQFLAG)
{
  int k;
  double x,y;
  double qyx = *logpy;
  double qxy = *logpx;
  double *dparamsx = double_vector(NW-1);
  double *dparamsy = double_vector(NW-1);
  double scale = 1./sqrt((double)NW);//Number of intrinsic parameters

  // this is a crude diagonal Fisher matrix for individual sine-gaussians
  intrinsic_fisher_update(paramsx,dparamsx,Snf,Snx,Tobs, NW, TFQFLAG);

  // [0] t0 [1] f0 [2] Q [3] Amp [4] phi ([5] beta chirplet)
  
  //perturb current location by N[0,scale*sigma]
  for(k=0; k<NW; k++)
  {
    paramsy[k] = paramsx[k] + scale*dparamsx[k]*gaussian_draw(seed);
  }

  if(checkrange(paramsy, range, NW))
  {
    free_double_vector(dparamsx);
    free_double_vector(dparamsy);

    *test=1;
    return;
  }

  //compute probabiltiy of such a proposal (could just product the draws from gaussian_draw() above
  y = 1.0;
  for(k=0; k<NW; k++)
  {
    x = (paramsx[k]-paramsy[k])/(scale*dparamsx[k]);
    y *= scale*dparamsx[k];
    qyx +=  -0.5*x*x;
  }
  qyx -= log(y);

  //need fisher at new location to compute reverse probability
  intrinsic_fisher_update(paramsy,dparamsy,Snf,Snx,Tobs, NW, TFQFLAG);

  //compute probabiltiy of reverse proposal
  y = 1.0;
  for(k=0; k<NW; k++)
  {
    x = (paramsx[k]-paramsy[k])/(scale*dparamsy[k]);
    y *= scale*dparamsy[k];
    qxy +=  -0.5*x*x;
  }
  qxy -= log(y);

  // map phase onto [0,2pi]
  while(paramsy[4] > LAL_TWOPI) paramsy[4] -=LAL_TWOPI;
  while(paramsy[4] < 0.0)       paramsy[4] +=LAL_TWOPI;

  //update incoming pointers and return
  *logpy += qyx;
  *logpx += qxy;

  free_double_vector(dparamsx);
  free_double_vector(dparamsy);
}



/* ********************************************************************************** */
/*                                                                                    */
/*                       Custom intrinsic proposal distributions                      */
/*                                                                                    */
/* ********************************************************************************** */

double wavelet_proximity_density(double f, double t, double **params, int *larray, int cnt, struct Prior *prior)
{
  int jj, kk;
  double tau;
  double sigwt, signt, sigwf, signf;
  double sigwt2, signt2, sigwf2, signf2;
  double df, dt, df2, dt2;
  double pden;

  double **range = prior->range;
  double TFV = prior->TFV;

  // distribution we are drawing from is hollowed out 2-d Gaussian


  // we give equal weight to each wavelet (equally likely to draw from any of them)
  // so have to sum up densities for each wavelet and divide by the number of wavelets

  double *norm = NULL;


  // if no other active wavelets, the draw would have been from the full TF volume
  if(cnt == 0)
  {
    pden = 1.0/TFV;
  }
  else
  {

    //get normalization for prior
    norm = malloc( (prior->smax+1)*sizeof(double)  );
    proximity_normalization(params, range, norm, larray, cnt);


    pden = 0.0;

    // loop over the the existing wavelets
    for(jj=1; jj<= cnt; jj++)
    {

      kk = larray[jj];

      df = fabs(f-params[kk][1]);
      dt = fabs(t-params[kk][0]);

      tau = params[kk][2]/(LAL_TWOPI*params[kk][1]);

      sigwt = SPRD*tau;
      sigwf = SPRD/(LAL_PI*tau);

      signt = HLW*tau;
      signf = HLW/(LAL_PI*tau);


      if(dt < 4.0*sigwt && df < 4.0*sigwf)   // don't bother adding in contribution wavelets that are too far away
      {

        sigwt2 = sigwt*sigwt;
        signt2 = signt*signt;

        sigwf2 = sigwf*sigwf;
        signf2 = signf*signf;

        df2 = df*df;
        dt2 = dt*dt;

        pden += norm[kk]*(exp(-df2/(2.0*sigwf2)-dt2/(2.0*sigwt2))-exp(-df2/(2.0*signf2)-dt2/(2.0*signt2)));

      }

    }

    pden /= (double)(cnt);

    pden *= (1.0-UNIFORM);

    pden += UNIFORM/TFV;   // the uniform fraction

    free(norm);

  }

  return pden;
}

void wavelet_proximity(double *paramsy, double **params, int *larray, double **range, int cnt, gsl_rng *seed)
{
  int i, jj, kk;
  double alpha;
  double tau;
  double sigwt, signt, sigwf, signf;
  double sigwt2, signt2, sigwf2, signf2;
  double df, dt, df2, dt2;
  double rat=-1;

  alpha = uniform_draw(seed);

  if(alpha < UNIFORM || cnt == 0)  // 100% uniform if no other pixels lit
  {
    for(i=0; i< 2; i++)
    {
      paramsy[i] = range[i][0] + (range[i][1]-range[i][0])*uniform_draw(seed);
    }
  }
  else
  {

    // select one of the existing pixels and propose near it
    jj = (int)(uniform_draw(seed)*(double)(cnt))+1;

    kk = larray[jj];

    tau = params[kk][2]/(LAL_TWOPI*params[kk][1]);

    // scaling have been chosen to give distances of about 1 to 5 ish

    sigwt = SPRD*tau;
    signt = HLW*tau;
    sigwt2 = sigwt*sigwt;
    signt2 = signt*signt;

    sigwf = SPRD/(LAL_PI*tau);
    signf = HLW/(LAL_PI*tau);
    sigwf2 = sigwf*sigwf;
    signf2 = signf*signf;

    // distribution we are drawing from is hollowed out 2-D Gaussian

    // rejection sample to find t2, f2
    while(alpha < rat)
    {
      df = sigwf*gaussian_draw(seed);
      df2 = df*df;

      dt = sigwt*gaussian_draw(seed);
      dt2 = dt*dt;

      rat = (exp(-df2/(2.0*sigwf2)-dt2/(2.0*sigwt2))-exp(-df2/(2.0*signf2)-dt2/(2.0*signt2)));

      alpha = uniform_draw(seed);

      paramsy[1] = params[kk][1]+df;
      paramsy[0] = params[kk][0]+dt;

      if (paramsy[0]<range[0][0] || paramsy[0]>range[0][1]) rat = 0.0;
      if (paramsy[1]<range[1][0] || paramsy[1]>range[1][1]) rat = 0.0;

    }


  }
  return;
}

void wavelet_proximity_new(double *paramsy, double **params, int *larray, double **range, int cnt, gsl_rng *seed)
{
  int i, jj, kk;
  double alpha=1.0;
  double tau;
  double sigwt, signt, sigwf, signf;
  double sigwt2, signt2, sigwf2, signf2;
  double df, dt, df2, dt2;
  double rat=-1.0;


  alpha = uniform_draw(seed);

  if(alpha < UNIFORM || cnt == 0)  // 100% uniform if no other pixels lit
  {
    for(i=0; i< 2; i++)
    {
      paramsy[i] = range[i][0] + (range[i][1]-range[i][0])*uniform_draw(seed);
    }
  }
  else
  {

    // select one of the existing pixels and propose near it
    jj = (int)(uniform_draw(seed)*(double)(cnt))+1;

    kk = larray[jj];

    tau = params[kk][2]/(LAL_TWOPI*params[kk][1]);

    // scaling have been chosen to give distances of about 1 to 5 ish

    sigwt = SPRD*tau;
    signt = HLW*tau;
    sigwt2 = sigwt*sigwt;
    signt2 = signt*signt;

    sigwf = SPRD/(LAL_PI*tau);
    signf = HLW/(LAL_PI*tau);
    sigwf2 = sigwf*sigwf;
    signf2 = signf*signf;

    // distribution we are drawing from is hollowed out Gaussian
    // 1/(sqrt(2 Pi)(sigmaW-sigmaN))*(exp(-x^2/2 sigmaW^2) - exp(-x^2/2 sigmaN^2))

    // rejection sample to find t2, f2
    //TODO: Why were we rejection sampling??
//    while(alpha > rat)
//    {
      df = sigwf*gaussian_draw(seed);
      df2 = df*df;

      dt = sigwt*gaussian_draw(seed);
      dt2 = dt*dt;

      rat = 1.0-exp(df2*(1.0/(2.0*sigwf2) - 1.0/(2.0*signf2)))*exp(dt2*(1.0/(2.0*sigwt2) - 1.0/(2.0*signt2)));

      alpha = uniform_draw(seed);

      paramsy[1] = params[kk][1]+df;
      paramsy[0] = params[kk][0]+dt;

      if
        (paramsy[0]<range[0][0] || paramsy[0]>range[0][1]) rat = -1.0;
      else if
        (paramsy[1]<range[1][0] || paramsy[1]>range[1][1]) rat = -1.0;

//    }
  }
}

void wavelet_sample(double *params, double **range, double **prop, int tsize, double Tobs, double pmax, gsl_rng *seed)
{
  int i, j;
  double f, t, alpha;
  double df, dt;

  // Note: it is possible to speed the rejection sampling up by separating out the high density pixels
  // and the uniform piece.

  dt = Tobs/(double)tsize;
  df = 0.5/dt;

  f = range[1][0]+(range[1][1]-range[1][0])*uniform_draw(seed);
  t = range[0][0]+(range[0][1]-range[0][0])*uniform_draw(seed);

  i = (int)(f/df);
  j = (int)(t/dt);

  alpha = 2.*pmax*uniform_draw(seed);

  while(alpha > prop[i][j])
  {
    f = range[1][0]+(range[1][1]-range[1][0])*uniform_draw(seed);
    t = range[0][0]+(range[0][1]-range[0][0])*uniform_draw(seed);

    i = (int)(f/df);
    j = (int)(t/dt);

    alpha = 2.*pmax*uniform_draw(seed);
  }

  params[0] = t;
  params[1] = f;

  return;
}

void draw_wavelet_params(double *params, double **range, gsl_rng *seed, int NW)
{
  int i;

  // Draw the rest from uniform distribution
  for(i=0; i<NW; i++)
  {
    params[i] = range[i][0] + (range[i][1]-range[i][0])*uniform_draw(seed);
  }

  return;
}

void draw_time_frequency(int ifo, int ii, struct Wavelet *wx, struct Wavelet *wy, double **range, gsl_rng *seed, double fr, struct TimeFrequencyMap *tf, int *prop)
{
  if(uniform_draw(seed)<fr)
  {
    TFprop_draw(wy->intParams[ii], range, tf, ifo, seed);
    *prop=0;
  }
  else
  {
    wavelet_proximity_new(wy->intParams[ii],wx->intParams,wy->index,range,wx->size, seed);
    *prop=1;
  }
}

void TFprop_draw(double *params, double **range, struct TimeFrequencyMap *tf, int ifo, gsl_rng *seed)
{
  int j, i, k;
  double f, t, Q, alpha;

  double tmin = range[0][0];
  double tmax = range[0][1];
  double fmin = range[1][0];
  double fmax = range[1][1];
  double Qmin = range[2][0];
  double Qmax = range[2][1];

  double dt = (tmax-tmin)/(double)tf->nt;
  double df = (fmax-fmin)/(double)tf->nf;
  double dQ = (Qmax-Qmin)/(double)tf->nQ;

  do
  {
    Q = Qmin + (Qmax-Qmin)*uniform_draw(seed);
    f = fmin + (fmax-fmin)*uniform_draw(seed);
    t = tmin + (tmax-tmin)*uniform_draw(seed);

    i = (int)((f-fmin)/df);
    j = (int)((t-tmin)/dt);
    k = (int)((Q-Qmin)/dQ);

    alpha = tf->max[ifo][k]*uniform_draw(seed);

  } while(alpha > tf->pdf[ifo][k][i][j]);

  params[0] = t;
  params[1] = f;
  params[2] = Q;
}

double TFprop_density(double *params, double **range, struct TimeFrequencyMap *tf, int ifo)
{
  double x;

  if(checkrange(params,range,3)) return 1.0e-60;
  
  double tmin = range[0][0];
  double tmax = range[0][1];
  double fmin = range[1][0];
  double fmax = range[1][1];
  double Qmin = range[2][0];
  double Qmax = range[2][1];

  double dt = (tmax-tmin)/(double)tf->nt;
  double df = (fmax-fmin)/(double)tf->nf;
  double dQ = (Qmax-Qmin)/(double)tf->nQ;


  int i = (int)((params[0]-tmin)/dt);
  int j = (int)((params[1]-fmin)/df);
  int k = (int)((params[2]-Qmin)/dQ);


  x = tf->pdf[ifo][k][j][i];

  return(x);
  
}

void TFprop_setup(struct Data *data, struct Model *model, double **range, struct TimeFrequencyMap *tf, int ifo)
{
  int nt = tf->nt;
  int nf = tf->nf;
  int nQ = tf->nQ;
  
  double ***tfden = tf->pdf[ifo];
  double ***tfsnr = tf->snr[ifo];
  double *tfmax   = tf->max[ifo];
  
  double Tobs = data->Tobs;
  
  int i, j, m, ii, ioff,n;
  double Qmin, Qmax;
  double tmin, tmax;
  double fmin, fmax;
  double dfx, dQx, dV;
  double fsnr;
  double tsnr;
  double norm;
  
  double *d  = double_vector(tf->N-1);
  double *h  = double_vector(tf->N-1);
  double *AC = double_vector(tf->N-1);
  double *AF = double_vector(tf->N-1);
  double *AC2 = double_vector(tf->N-1);
  double *AF2 = double_vector(tf->N-1);

  double *params = double_vector(4);
  
  tmin = range[0][0];
  tmax = range[0][1];
  fmin = range[1][0];
  fmax = range[1][1];
  Qmin = range[2][0];
  Qmax = range[2][1];
  
  //Fix time, amplitude, and phse of wavelet filter
  params[0] = 0.0;
  params[3] = 1.0;
  params[4] = 0.0;
  
  dfx = (fmax-fmin)/(double)(nf);  // spacing of grid in frequency
  dQx = (Qmax-Qmin)/(double)(nQ);  // spacing of grid in frequency
  
  dV = (double)(nt*nf*nQ)/( (tmax-tmin) * (fmax-fmin) * (Qmax-Qmin) );
  
  // offset in time index between start of data and start of analysis region
  ioff = (int)((tmin)/(data->Tobs/(double)data->N));
  
  //how many time bins between tmin and tmax?
  int nbin = (int)((tmax-tmin)/(data->Tobs/(double)data->N))/nt;
  
  // Copy data into local array (Fourier transform will destroy contents)
  for(i=0; i<tf->N/2; i++)
  {
    d[2*i]  = data->s[ifo][2*i];
    d[2*i+1]= data->s[ifo][2*i+1];
  }
  norm = 0.0;
  
  // Loop over different Q layers
  for(m = 0; m < nQ; m++)
  {
    // Set Q of wavelet filter to median of layer
    params[2] = Qmin + ((double)(m)+0.5)*dQx;
    
    for(j = 0; j < nf; j++)
    {
      // Step trhough different frequencies
      params[1] = fmin + dfx*((double)(j)+0.5);

      SineGaussianFourier(h, params, tf->N, 0, Tobs);

      fsnr = fourier_nwip(data->imin, data->imax, h, h, model->invSnf[ifo]);

      for(i=0; i<tf->N; i++)
      {
        AC2[i]=0.0;
        AF2[i]=0.0;
      }

      phase_blind_time_shift(AC, AF, d, h, model->invSnf[ifo], tf->N);
      
      // Get filter weight at each time step
      for(i = 0; i < nt; i++)
      {
        tsnr = 0.0;

        //integrate snr over nbin
        for(n=0; n<nbin; n++)
        {
          ii = nbin*i + ioff + n;
          tsnr += (AC[ii]*AC[ii]+AF[ii]*AF[ii])/(fsnr);
        }
        tfsnr[m][j][i] = tsnr;

        // Proposal density scales as BF~exp(SNR^2/2)
        if(data->logClusterProposalFlag)
          tfden[m][j][i] = tsnr/2.;
        else
          tfden[m][j][i] = exp(tsnr/2.0)-1.0;
        norm += tfden[m][j][i];
      }
    }
  }  // end Q loop
  
  //Normalize and find max density for rejection sampling
  tfmax[0] = 0;
  for(m = 0; m < nQ; m++)
  {
    for(j = 0; j < nf; j++)
    {
      for(i = 0; i < nt; i++)
      {
        tfden[m][j][i] /= norm;
        tfden[m][j][i] *= dV;
        if(tfden[m][j][i] > tfmax[0]) tfmax[0] = tfden[m][j][i];
      }
    }
  }
  for(m = 1; m < nQ; m++) tfmax[m] = tfmax[0];
  
  
  free_double_vector(AC);
  free_double_vector(AF);
  free_double_vector(h);
  free_double_vector(d);
  
  free_double_vector(params);
  
}

void TFprop_plot(double **range, struct TimeFrequencyMap *tf, double dt, int ifo)
{
  int nt= tf->nt;
  int nf= tf->nf;
  int nQ= tf->nQ;
  double ***tfden = tf->pdf[ifo];

  int m, j, i;
  double Qmin, Qmax;
  double tmin, tmax;
  double fmin, fmax;
  double f, t, dfx, dQx, a, x, y, u, Q, mx;
  char filename[100];
  FILE *out;
  FILE *test;

  tmin = range[0][0];
  tmax = range[0][1];
  fmin = range[1][0];
  fmax = range[1][1];
  Qmin = range[2][0];
  Qmax = range[2][1];

  dfx = (fmax-fmin)/(double)(nf);  // spacing of grid in frequency
  dQx = (Qmax-Qmin)/(double)(nQ);  // spacing of grid in frequency

  x = 0.0;
  y = 1.0e60;
  for(m = 0; m < nQ; m++)  // loop over Q
  {
    for(j = 0; j < nf; j++)
    {
      for(i = 0; i < nt; i++)
      {
        a = tfden[m][j][i];
        if(a > x) x = a;
        if(a < y) y = a;
      }
    }
  }

  mx = x;
  a = ceil(-log10(x));
  y = x*pow(10.0, a);
  u = floor(y);

  if(y-u < 0.5) x = (u+0.5)*pow(10.0, -a);
  if(y-u >= 0.5) x = (u+1.0)*pow(10.0, -a);

  for(m = 0; m < nQ; m++)  // loop over Q
  {
    Q = Qmin + ((double)(m)+0.5)*dQx;

    sprintf(filename,"waveforms/TF_map.dat");
    out = fopen(filename,"w");
    for(j = 0; j < nf; j++)
    {
      f = fmin + dfx*((double)(j)+0.5);
      for(i = 0; i < nt; i++)
      {
        t = tmin + dt*((double)(i)+0.5);
        if(j==0 && i==0)
        {
          fprintf(out, "%f %f %e\n", t, f, mx);    // this forces all the maps to use the same color scale
        }
        else
        {
          fprintf(out, "%f %f %e\n", t, f, tfden[m][j][i]);
        }
      }
      fprintf(out,"\n");
    }
    fclose(out);


    sprintf(filename,"TF_script.gnu");
    test = fopen(filename,"w");
    fprintf(test,"set term png enhanced truecolor crop font Helvetica 18  size 1200,800\n");
    if(Q < 10.0)
    {
      fprintf(test,"set output 'waveforms/TF_0%.1f_%i.png'\n", Q,ifo);
    }
    else
    {
      fprintf(test,"set output 'waveforms/TF_%.1f_%i.png'\n", Q,ifo);
    }
    fprintf(test,"set pm3d map corners2color c1\n");
    fprintf(test,"set xrange [%f:%f]\n", tmin, tmax);
    fprintf(test,"set yrange [%f:%f]\n", fmin, fmax);
    // fprintf(test,"set zrange [%e:%e]\n", 0.0, x);
    fprintf(test,"set title 'Q = %.1f'\n", Q);
    fprintf(test,"set ylabel 'f (Hz)'\n");
    fprintf(test,"set xlabel 't (s)'\n");
    fprintf(test,"set palette defined ( 0 '#000090',\
            1 '#000fff',\
            2 '#0090ff',\
            3 '#0fffee',\
            4 '#90ff70',\
            5 '#ffee00',\
            6 '#ff7000',\
            7 '#ee0000',\
            8 '#7f0000')\n");
    fprintf(test,"splot 'waveforms/TF_map.dat' using 1:2:3 notitle");
    fprintf(test,"\n");
    fclose(test);

    system("gnuplot TF_script.gnu");

  }

}

void TFprop_signal(struct Data *data, double **range, struct TimeFrequencyMap *tf, struct Network *net)
{
    int n = tf->N;
    int nt = tf->nt;
    int nf = tf->nf;
    int nQ = tf->nQ;

    double ****tfden = tf->pdf;
    double ****tfsnr = tf->snr;
    double **tfmax   = tf->max;

    double Tobs = data->Tobs;

    int i, ii, j, m, ioff;
    double dt,dV;
    double norm;
    double tsnr;

    dt = Tobs/(double)n;


    double tmin, tmax;
    double fmin, fmax;
    double Qmin, Qmax;
    tmin = range[0][0];
    tmax = range[0][1];
    fmin = range[1][0];
    fmax = range[1][1];
    Qmin = range[2][0];
    Qmax = range[2][1];

    dV = (double)(nt*nf*nQ)/( (tmax-tmin) * (fmax-fmin) * (Qmax-Qmin) );

    // Here we combine the TF maps from two detectors allowing for the time delay tdelay = t1 -t2
    // It is assumed that the signal model times are referenced to detector 1
    // This code should be generalized to handle N detectors

    int ifo;

    norm=0.0;
    for(m = 0; m < nQ; m++)  // loop over Q
    {

        //    x = 0.0;
        for(ifo=1; ifo<data->NI; ifo++)
        {
            ioff = (int)((net->dtimes[ifo]-net->dtimes[0])/dt);
            for(j = 0; j < nf; j++)
            {
                for(i = 0; i < nt; i++)
                {
                    ii = i - ioff;
                    if(ii < 0 || ii > nt)
                    {
                        tsnr = tfsnr[0][m][j][i];  // leave out second detector if mapped out of time window (small edge effect)
                    }
                    else
                    {
                        tsnr = tfsnr[0][m][j][i] + tfsnr[ifo][m][j][ii];
                    }
                    if(data->logClusterProposalFlag)
                        tfden[data->NI][m][j][i] = tsnr/2.;
                    else
                        tfden[data->NI][m][j][i] = exp(tsnr/2.0)-1.0;
                    norm += tfden[data->NI][m][j][i];
                }
            }
        }
    }

    tfmax[data->NI][0] = 0.0;
    for(m = 0; m < nQ; m++)  // loop over Q
    {
        for(j = 0; j < nf; j++)
        {
            for(i = 0; i < nt; i++)
            {
                tfden[data->NI][m][j][i] /= norm;
                tfden[data->NI][m][j][i] *= dV;
                if(tfden[data->NI][m][j][i] > tfmax[data->NI][0]) tfmax[data->NI][0] = tfden[data->NI][m][j][i];
            }
        }
    }
    for(m = 1; m < nQ; m++) tfmax[data->NI][m] = tfmax[data->NI][0];
    
    
}

/* ********************************************************************************** */
/*                                                                                    */
/*                       Custom extrinsic proposal distributions                      */
/*                                                                                    */
/* ********************************************************************************** */


void sky_ring_proposal(double *x, double *y, struct Data *data, gsl_rng *seed)
{
  int i,j,l;
  int NI = data->NI;

  double n[3];
  double k[3];

  double newRA, newDec;

  double gmst = XLALGreenwichMeanSiderealTime(&data->epoch);

  //remap gmst back to [0:2pi]
  double intpart;
  double decpart;
  gmst /= LAL_TWOPI;
  intpart = (int)( gmst );
  decpart = gmst - (double)intpart;
  gmst = decpart*LAL_TWOPI;

  double ra       = x[0];
  double sindelta = x[1];
  double dec      = asin(sindelta);

  //first copy parameters to new array
  for(i=0; i<7; i++) y[i]=x[i];

  /*
   line-of-sight vector
   */
  k[0] = cos(gmst-ra)*cos(dec);
  k[1] =-sin(gmst-ra)*cos(dec);
  k[2] = sin(dec);

  double IFO1[3];
  double IFO2[3];
  double IFOX[3];


  /*
   Store location for each detector
   */
  double **IFO = double_matrix(NI-1,2);

  int ifo;
  for(ifo=0; ifo<NI; ifo++)
  {
    memcpy(IFOX, data->detector[ifo]->location, 3*sizeof(double));
    for(i=0; i<3; i++) IFO[ifo][i] = IFOX[i];
  }

  /*
   Randomly select two detectors from the network
   -this assumes there are no co-located detectors
   */
  i=j=0;
  while(i==j)
  {
    i=(int)floor(uniform_draw(seed)*(double)NI);
    j=(int)floor(uniform_draw(seed)*(double)NI);
  }

  for(l=0; l<3; l++)
  {
    IFO1[l] = IFO[i][l];
    IFO2[l] = IFO[j][l];
  }

  /*
   detector axis
   */
  double normalize;

  normalize=0.0;
  for(i=0; i<3; i++)
  {
    n[i]  = IFO1[i]-IFO2[i];
    normalize += n[i]*n[i];
  }
  normalize = 1./sqrt(normalize);
  for(i=0; i<3; i++) n[i] *= normalize;

  /*
   rotation angle
   */
  double omega    = LAL_TWOPI*uniform_draw(seed);
  double cosomega = cos(omega);
  double sinomega = sin(omega);
  double c1momega = 1.0 - cosomega;


  /*
   rotate k' = Rk
   */
  double kp[3];
  kp[0] = (c1momega*n[0]*n[0] + cosomega)     *k[0] + (c1momega*n[0]*n[1] - sinomega*n[2])*k[1] + (c1momega*n[0]*n[2] + sinomega*n[1])*k[2];
  kp[1] = (c1momega*n[0]*n[1] + sinomega*n[2])*k[0] + (c1momega*n[1]*n[1] + cosomega)     *k[1] + (c1momega*n[1]*n[2] - sinomega*n[0])*k[2];
  kp[2] = (c1momega*n[0]*n[2] - sinomega*n[1])*k[0] + (c1momega*n[1]*n[2] + sinomega*n[0])*k[1] + (c1momega*n[2]*n[2] + cosomega)     *k[2];

  /*
   convert k' back to ra' and dec'
   */
  newDec   = asin(kp[2]);
  newRA    = atan2(kp[1],kp[0]) + gmst;

  if(newRA <0.0)
    newRA+=LAL_TWOPI;
  else if (newRA >= LAL_TWOPI)
    newRA -= LAL_TWOPI;

  y[0] = newRA;
  y[1] = sin(newDec);

  free_double_matrix(IFO,NI-1);
}



/* ********************************************************************************** */
/*                                                                                    */
/*            Proposal for extrinsic parameters to hold waveforms fixed               */
/*                                                                                    */
/* ********************************************************************************** */

/*
 The skymap functions involve extracting the "primitive" antenna patters f+ and fx from
 the full antenna patterns F+ and Fx, which LIGO defines as including the psi dependence.

 TODO: Note that the sky mapping functions only gives the phase phi in the range [0,Pi].
 The full range is covered by mapping phi -> phi + pi and psi -> psi + pi/2.
 I don't see any reason for using the expanded range.

 The skydensity() code computes the Jacobian. The Hasting's ratio should be multiplied by
 the quantity

 Jxy = skydensity(paramsx, paramsy);

 The new parameters paramsy are found from the call

 skymap(paramsx, paramsy);

 This assumes that the new sky location has already been selected.
 */

static void uvwz(double *u, double *v, double *w, double *z, double *params)
{
  double psi, ecc, Amp, phi, ps2;
  double cphi, sphi, c2p, s2p;

  psi = params[2];
  ecc = params[3];
  phi = params[4];
  Amp = params[5];
  ps2 = 2.0*psi;

  cphi = cos(phi);
  sphi = sin(phi);
  c2p  = cos(ps2);
  s2p  = sin(ps2);

  *u = Amp*(cphi*c2p+ecc*sphi*s2p);
  *v = Amp*(cphi*s2p-ecc*sphi*c2p);
  *w = Amp*(sphi*c2p-ecc*cphi*s2p);
  *z = Amp*(sphi*s2p+ecc*cphi*c2p);
}

static void uvwz_sol(double *uy, double *vy, double *wy, double *zy, double ux, double vx, double wx, double zx, \
                     double fp1x, double fp1y, double fc1x, double fc1y, double fp2x, double fp2y, double fc2x, double fc2y)
{

  double den;

  den = 1./(fc1y*fp2y-fc2y*fp1y);

  double c1 = fc1y*fc2x - fc1x*fc2y;
  double c2 = fc1y*fp2x - fc2y*fp1x;
  double c3 = fc1x*fp2y - fc2x*fp1y;
  double c4 = fp2y*fp1x - fp1y*fp2x;

  *uy = (vx*c1 + ux*c2)*den;

  *vy = (vx*c3 + ux*c4)*den;

  *wy = (zx*c1 + wx*c2)*den;

  *zy = (zx*c3 + wx*c4)*den;

}


static void exsolve(double *phi, double *psi, double *Amp, double *eps, double uy, double vy, double wy, double zy)
{

  double q, rad;
  double x, p, ecc;
  int flag1, flag2, flag3;

  double c1 = uy*wy+vy*zy;
  double c2 = uy*uy+vy*vy;
  double c3 = wy*wy+zy*zy;
  double c4 = uy*zy-vy*wy;

  q = 2.0*c1/(c2 - c3);

  rad = sqrt(1.0+q*q);

  x = atan2(2.0*c1, (c2 - c3));
  if(x < 0.0) x += LAL_TWOPI;

  double x_on_2 = 0.5*x;

  *phi = 0.5*x;

  flag1 = flag2 = flag3 = 0;
  if(cos(x) > 0.0)      flag1 = 1;
  if(cos(x_on_2) > 0.0) flag2 = 1;
  if(sin(x_on_2) > 0.0) flag3 = 1;

  p = ((rad+1.0)*c2+(rad-1.0)*c3+2.0*q*c1)/(2.0*rad);


  if (flag1) ecc = c4/p;
  else       ecc = p/c4;

  *eps = ecc;

  if(flag1 && flag2)
  {
    x = atan2((1.0+rad)*vy+q*zy,(1.0+rad)*uy+q*wy);
  }
  else if (flag1 && !flag2)
  {
    x = atan2(-((1.0+rad)*vy+q*zy),-((1.0+rad)*uy+q*wy));
  }
  else if (!flag1 && flag3)
  {
    x = atan2( ecc*((1.0+rad)*uy+q*wy), -ecc*((1.0+rad)*vy+q*zy));
  }
  else if (!flag1 && !flag3)
  {
    x = atan2(-ecc*((1.0+rad)*uy+q*wy), ecc*((1.0+rad)*vy+q*zy));
  }

  if(x < 0.0) x += LAL_TWOPI;


  *psi = 0.5*x;

  if(flag1)  *Amp = sqrt(p);
  else       *Amp = sqrt(p)/fabs(ecc);

}

// finds the extrinsic parameters at the new sky location that preserve the waveforms
static void update_orientation_parameters(double *paramsx, double *paramsy, struct Data *data)
{
  int NI = data->NI;
  double alpha, sindelta, psi, psi0;
  double *fyplus, *fycross;
  double *fxplus, *fxcross;
  double ux, vx, wx, zx;
  double uy, vy, wy, zy;
  double newPhi, newPsi, newAmp, newEps;
  int i;

  double x, y;

  fyplus  = malloc(NI*sizeof(double));
  fycross = malloc(NI*sizeof(double));
  fxplus  = malloc(NI*sizeof(double));
  fxcross = malloc(NI*sizeof(double));

  // Get detector frame orientation for current location
  alpha    = paramsx[0];
  sindelta = paramsx[1];
  psi      = 0.0;

  for(i=0; i< NI; i++)
  {
    XLALComputeDetAMResponse(&x, &y, (const REAL4(*)[3])data->detector[i]->response, alpha, asin(sindelta), psi, XLALGreenwichMeanSiderealTime(&data->epoch));
    psi0 = 0.5*atan2(y,x);
    XLALComputeDetAMResponse(&x, &y, (const REAL4(*)[3])data->detector[i]->response, alpha, asin(sindelta), psi0, XLALGreenwichMeanSiderealTime(&data->epoch));
    fxplus[i]  = x*cos(2.0*psi0);
    fxcross[i] = fxplus[i]*tan(2.0*psi0);
  }

  // Get detector frame orientation for proposed location
  alpha    = paramsy[0];
  sindelta = paramsy[1];
  psi      = 0.0;

  for(i=0; i< NI; i++)
  {
    XLALComputeDetAMResponse(&x, &y, (const REAL4(*)[3])data->detector[i]->response, alpha, asin(sindelta), psi, XLALGreenwichMeanSiderealTime(&data->epoch));
    psi0 = 0.5*atan2(y,x);
    XLALComputeDetAMResponse(&x, &y, (const REAL4(*)[3])data->detector[i]->response, alpha, asin(sindelta), psi0, XLALGreenwichMeanSiderealTime(&data->epoch));
    fyplus[i]  = x*cos(2.0*psi0);
    fycross[i] = fyplus[i]*tan(2.0*psi0);

  }

  // Solve for extrinsic parameters that preserve h
  uvwz(&ux, &vx, &wx, &zx, paramsx);

  uvwz_sol(&uy, &vy, &wy, &zy, ux, vx, wx, zx, fxplus[0], fyplus[0], fxcross[0], fycross[0], fxplus[1], fyplus[1], fxcross[1], fycross[1]);

  exsolve(&newPhi, &newPsi, &newAmp, &newEps, uy, vy, wy, zy);

  if(!data->extrinsicAmplitudeFlag) newAmp=1.0;

  paramsy[2] = newPsi;
  paramsy[3] = newEps;
  paramsy[4] = newPhi;
  paramsy[5] = newAmp;

  free(fyplus);
  free(fycross);
  free(fxplus);
  free(fxcross);

}

void network_orientation_proposal(double *paramsx, double *paramsy, struct Data *data, double *logJ)
{
  int i, j;
  double detJ;
  double ep = 1.0e-6;
  int NJ = 3;
  if(data->extrinsicAmplitudeFlag) NJ = 4;

  double *paramsxp = malloc(NE*sizeof(double));
  double *paramsyp = malloc(NE*sizeof(double));

  // At this stage all we have for the new location y is the sky location. So first we find what the rest
  // of the parameters (amplitude, polarization, ellipticity, phase) need to be to keep the signal the same

  update_orientation_parameters(paramsx, paramsy, data);

  // The sky locations are chosen to be on the equal time delay sky ring. Presumably the desnity factors for that
  // mapping have been taken care of. What we need to know here is how the volume of a small volume
  // dAx /\ dpsix /\ dphix /\ depsilonx onto the image volume dAy /\ dpsiy /\ dphiy /\ depsilony. This is
  // given by the Jacobian.

  // numerically compute the Jacobian by perturbing updated extrinsic parameters
  double **J = malloc(NJ*sizeof(double*));
  for(i=0; i<NJ; i++) J[i] = malloc(NJ*sizeof(double));

  for(i=0; i<NJ; i++)  // step through the parameters at x
  {
    for(j=0; j<NE; j++) paramsxp[j] = paramsx[j];
    for(j=0; j<2; j++) paramsyp[j] = paramsy[j];

    //perturb an extrinsic
    paramsxp[i+2] += ep;

    //find new extrinsic parameters
    update_orientation_parameters(paramsxp, paramsyp, data);

    // can get thrown to the alternative solution of phi, psi. Map these back
    if( fabs(paramsyp[2]+LAL_PI_2-paramsy[2]) < fabs(paramsyp[2]-paramsy[2]) ) paramsyp[2] += LAL_PI_2; //LAL_PI_2 = PI/2
    if( fabs(paramsyp[2]-LAL_PI_2-paramsy[2]) < fabs(paramsyp[2]-paramsy[2]) ) paramsyp[2] -= LAL_PI_2;
    if( fabs(paramsyp[4]+LAL_PI-paramsy[4])   < fabs(paramsyp[4]-paramsy[4]) ) paramsyp[4] += LAL_PI;
    if( fabs(paramsyp[4]-LAL_PI-paramsy[4])   < fabs(paramsyp[4]-paramsy[4]) ) paramsyp[4] -= LAL_PI;
    
    //Derivative of extrinsic parameters are Jacobian elements
    for(j=0; j<NJ; j++) J[i][j] = (paramsyp[j+2]-paramsy[j+2])/ep;
  }

  detJ = matrix_jacobian(J, NJ);

  for(i=0; i<NJ; i++)free(J[i]);
  free(J);
  free(paramsyp);
  free(paramsxp);

  *logJ += log(detJ);
}


/* ********************************************************************************** */
/*                                                                                    */
/*                          Stochastic background proposals                           */
/*                                                                                    */
/* ********************************************************************************** */

void stochastic_background_proposal(struct Background *bkg_x, struct Background *bkg_y, gsl_rng *seed, int *test)
{

  double logAmin = -115;
  double logAmax = -80;

  double kmin = -6;
  double kmax =  6;

  //uniform
  if(uniform_draw(seed)<0.1)
  {
    bkg_y->logamp = logAmin + uniform_draw(seed)*(logAmax-logAmin);
    bkg_y->index  = kmin + uniform_draw(seed)*(kmax-kmin);
  }
  //gaussian
  else
  {
    bkg_y->logamp = bkg_x->logamp + gaussian_draw(seed)*(0.1);
    bkg_y->index  = bkg_x->index  + gaussian_draw(seed)*(0.1);

    if(bkg_y->logamp < logAmin || bkg_y->logamp > logAmax) *test = 1;
    if(bkg_y->index  < kmin    || bkg_y->index  > kmax   ) *test = 1;

  }
}


