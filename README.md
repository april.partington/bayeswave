# BayesWave

Welcome to the BayesWave software repository.  These pages provide instructions for:
 * [Installation](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/INSTALLATION.md)
 * [Running Analyses](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/RUNNING.md)

See the [BayesWave wiki](https://wiki.ligo.org/Bursts/BayesWave) for further info

## Quick Start
See
[here](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/QUICKSTART.md)
for quick start instructions to installing and running a simple GW150914
analysis

## Available Branches
There are two main branches (with corresponding singularity containers) which are well supported:
 * Master: the main branch with latest features, active development and little guarentee of 100% functionality.
 * Stable: functionally unchanged from O2.  Should be considered the *reviewed, production-level* code.

Older SVN branches and tags are also available.  Functionality is not
guarenteed, they have not been updated to work with the latest infrastructure
and are preserved purely for posterity.

## Contributing
Procedures for developing and contributing to this repository match those for
[lalsuite](https://git.ligo.org/lscsoft/lalsuite).  The contribution guide for
lalsuite can be found
[here](https://git.ligo.org/lscsoft/lalsuite/blob/master/CONTRIBUTING.md)

Documentation is most conveniently provided via markdown (.md) files in the
repository itself.  The page you are now reading is the `README.md` at the root
of the master branch.  All other documentation should live in `doc`.

**TLDR**
 * We use the fork/merge development model
 * Development projects should take place on feature branches (*NOT* adhoc
   sub-directories!)

This eliminates conflicts, ensures the master branch is always useable (no one
breaks the build etc) and means different features/branches are easily
found, used and contributed to.

So: 
 1. create your own fork of this repository
 1. create a branch to develop some feature on the fork
 1. make and commit changes
 1. push that branch upstream
 1. when development is complete, merge that branch's changes back into master

Full details of how to do this can be found in the [lalsuite contribution
guide](https://git.ligo.org/lscsoft/lalsuite/blob/master/CONTRIBUTING.md)

This is, first and foremost, a *source* repository.  To reduce bloat and
simplify development, data products and analysis-specific scripts/configuration
files should really be hosted in separate repositories or archives which
document those analyses.

## Repository Structure
This repository has been cloned from the original svn repository at [
https://svn.ligo.caltech.edu/svn/bayeswave](
https://svn.ligo.caltech.edu/svn/bayeswave).  Commit history, branches and tags
have been preserved and non-source directories have been excised from the
history.  Some details of the svn->git transition can be found
[here](https://wiki.ligo.org/Bursts/BayesWave/BayesWaveGit).

The git master branch looks like:
* `src`: The main source directory.  Includes all C code, header files and build tools
* `test`: Test scripts to check successful compilation and linking.
* `utils`: Python and bash utility scripts.
* `postprocess`: Python and bash post-processing (e.g., plots, webpages) scripts
* `BayesWavePipe`: A standalone python module for HTCondor workflow generation

Note that different branches in the original svn do not share a uniform
structure; if you cannot find the directory or file you're looking for, check
you're on the correct branch.



