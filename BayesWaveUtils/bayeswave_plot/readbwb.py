
import numpy as np
import glob
import sys

# ---------------
# Script to read 
# BWB parameters
# --------------


# -- Read the run file

class BwbParams:

    def __init__(self):

        # -- Initialize variables
        ifoNames = []
        self.bayesline = False
        self.ra = None
        self.dec = None

        
        # -- Determine name of job
        bayeswaverunfile = glob.glob('*bayeswave.run')
        if not len(bayeswaverunfile):
            sys.exit("\n bayeswave.run not found! \n")
        bayeswaverunfile = bayeswaverunfile[0]        
        jobName = bayeswaverunfile.split('_')
        del jobName[-1]
        if not len(jobName) == 0:
            self.jobname = '_'.join(jobName)+'_'
        else:
            self.jobname = ''

        # -- Read the evidence file
        try:
            evfile = open('evidence.dat')
            for line in evfile:
                spl = line.split()
                if spl[0] == 'glitch':
                    self.glitch = float(spl[1])
                    self.glitch_err = float(spl[2])
                if spl[0] == 'signal':
                    self.signal = float(spl[1])
                    self.signal_err = float(spl[2])
                if spl[0] == 'noise':
                    self.noise = float(spl[1])
                    self.noise_err = float(spl[2])
        except: 
            print "WARNING: Could not find file evidence.dat"


        # -- Read run file
        runfile = open(bayeswaverunfile, 'r')

        # -- Parse command line arguments
        lines = runfile.readlines()
        cmdline = lines[7].split(' ')

        # This is my (Tyson's) horrendously hacky way to find the injection parameters.
        # Can someon who knows Python fix this?
        # Update: Fixed. The script now picks up the injection sky location from the xml file
        # Sudarshan Ghonge
        #injline = lines[26].split(' ')
        #print injline[2]
        #if injline[2]=='Signal':
        #    print "Found Signal Injection data!"
        #    self.ra = float(lines[27].split()[1])
        #    sin_dec = float(lines[27].split()[2])
        #    self.dec = np.arcsin(sin_dec)

        runfile.close()

        for index, arg in enumerate(cmdline):
            # Determine the IFOs involved
            if arg=='--ifo':
                ifoNames.append(cmdline[index+1])
            # Determine the trigger time
            elif arg=='--trigtime':
                self.gps = float(cmdline[index+1])
            # Determine if the job was glitchOnly, noiseOnly, or signalOnly
            elif arg=='--glitchOnly':
                print '\nThis run was executed with the --glitchOnly flag\n'
                self.restrictModel = 'glitch'
            elif arg=='--noiseOnly':
                print '\nThis run was executed with the --noiseOnly flag\n'
                self.restrictModel = 'noise'
            elif arg=='--signalOnly':
                print '\nThis run was executed with the --signalOnly flag\n'
                self.restrictModel = 'signal'
            elif arg=='--inj':
                self.mdc = True
                self.xml = cmdline[index+1]
            elif arg=='--MDC-channel':
                self.mdc_channel = cmdline[index+1]
            elif arg=='--MDC-prefactor':
                self.scale = cmdline[index+1]
            elif arg=='--MDC-cache':
                self.mdc_cache = cmdline[index+1]
                self.mdc = True
            elif arg=='--srate':
                self.srate = cmdline[index+1]
            elif arg=='--seglen':
                self.seglen = cmdline[index+1]
            elif arg=='--bayesLine':
                self.bayesline = True
            elif arg=='--event':
                self.event = cmdline[index+1]
        
        self.numifo = len(ifoNames)
        self.ifoNames = ifoNames

        self.flowList = []
        chanList = []
        for ifo in ifoNames:
            for index, arg in enumerate(cmdline):
                if arg=='--{0}-flow'.format(ifo):
                    self.flowList.append(cmdline[index+1])
                if arg=='--{0}-channel'.format(ifo):
                    chanList.append(cmdline[index+1])


